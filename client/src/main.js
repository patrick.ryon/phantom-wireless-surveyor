import { createApp } from 'vue'
import App from './App.vue'
import router from './router/router'
import VueKonva from 'vue-konva';
import 'bootstrap/dist/css/bootstrap.css'

const app = createApp(App)

app.use(router)
app.use(VueKonva);

app.mount('#app')
