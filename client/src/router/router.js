import { createRouter, createWebHistory } from 'vue-router';
import Projects from '../components/Projects.vue';
import Project from '../components/Project.vue';
import Ping from '../components/Ping.vue';

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Projects',
      component: Projects,
    },
    {
      path: '/project/:project',
      name: 'Project',
      component: Project,
    },
    {
      path: '/ping',
      name: 'ping',
      component: Ping
    },
  ]
});

export default router