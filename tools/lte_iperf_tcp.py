import argparse
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import yaml

from datetime import datetime
from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print(f'WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'lte_iperf_tcp'


def main(args):
    logger.debug(f"project directory: {args.p}")

    project_data = json_data.read_json(f"{args.p}/project.json")
    floorplan_file = os.path.join(args.p, project_data["floor_plan"])
    logger.debug(f"floorplan file: {floorplan_file}")

    # Load the floorplan image
    floorplan_img = mpimg.imread(floorplan_file)

    # Load SQLite file
    data_file = os.path.join(args.p, "data.sqlite")

    carriers = sqlite_data.get_lte_carriers(data_file, 'lte')
    # Process tests by type
    tests = [
        ["bps", "Rate (Mbps)", "rate"],
        ["retries", "Retries", "retries"],
        ["rtt", "RTT (ms)", "rtt"]
    ]
    test_types = ["f_l", "r_l", "f_r", "r_r", "f_s", "r_s", "u", "d"]
    for test in tests:

        logger.debug(f"processing test: {test[0]} ({test[1]})")

        err, heatmap_points, summary_points = sqlite_data.get_lte_iperf(
            data_file, test[0], carriers, "tcp")

        if not err:
            for test_type in test_types:
                logger.debug(test_type)
                if test_type == "f_r":
                    test_name = f"remote-upload-{test[2]}"
                    create_heatmap(args, floorplan_img, test, heatmap_points,
                                   test_type, test_name, carriers, "upload",
                                   "tcp")
                if test_type == "r_r":
                    test_name = f"remote-download-{test[2]}"
                    create_heatmap(args, floorplan_img, test, heatmap_points,
                                   test_type, test_name, carriers, "download",
                                   "tcp")
                if test_type == "u":
                    test_name = f"summary-upload-{test[2]}"
                    create_chart(args, test, summary_points["u"], test_type,
                                 test_name, carriers, "upload", "tcp")
                if test_type == "d":
                    test_name = f"summary-download-{test[2]}"
                    create_chart(args, test, summary_points["d"], test_type,
                                 test_name, carriers, "download", "tcp")


def create_heatmap(args, floorplan_img, test, data_points, test_type,
                   test_name, carriers, direction, protcol):
    # Dimensions of the working floorplan defined by image dimensions
    floorplan_width = floorplan_img.shape[1]
    floorplan_height = floorplan_img.shape[0]

    logger.debug(f'{test_name}')

    for carrier in carriers:
        logger.debug(data_points[carrier][test_type])
        # Extract x, y, and z values from data points
        x, y, z = zip(*data_points[carrier][test_type])

        # Define the interpolate grid using floorplan dimensions
        xi, yi = np.meshgrid(
            np.linspace(0, floorplan_width, floorplan_width),
            np.linspace(0, floorplan_height, floorplan_height))
        #xi, yi = xi.flatten(), yi.flatten()

        # Create the interpolant using RBF
        rbf = Rbf(x, y, z, function='cubic')

        # Interpolate the data
        zi = rbf(xi, yi)

        # Set values in zi to 0 where they are 0
        zi[zi == 0] = 0
        if test == "rtt":
            zi[zi == -1] = 500
            min_threshold = 0  # desired minimum threshold

            # Find indices where zi is above or equal to the minimum threshold
            below_threshold_indices = zi < min_threshold
            logger.info(zi)
            logger.info(f'below_threshold_indices: {below_threshold_indices}')

            # Apply the minimum threshold to values below the threshold
            zi[below_threshold_indices] = 0

        # Set the minimum and maximum values for the colormap
        vmin = 0  # Set your desired minimum value here
        vmax = 100  # Set your desired maximum value here
        if test[2] == "retries":
            vmin = 0  # Set your desired minimum value here
            vmax = 5000  # Set your desired maximum value here
        if test[2] == "rtt":
            vmin = 0  # Set your desired minimum value here
            vmax = 500  # Set your desired maximum value here

        # Set values in zi to the specified color
        cmap_colors = [
            (0.5, 0.5, 0.5),  # Gray
            (0, 1, 0),  # Green
            (1, 1, 0),  # Yellow
            (1, 0, 0)]  # Red
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', cmap_colors, N=256)

        # Create a figure
        fig, ax = plt.subplots(figsize=(10, 8))

        # Display the floorplan image with 'origin' set to 'upper'
        ax.imshow(floorplan_img, origin='upper')

        # Define the custom colormap with specified color transitions
        colors = [  # default coverage colormap
            ((0)/100, 'green'),
            ((5)/100, 'green'),
            ((10)/100, 'yellow'),
            ((30)/100, 'yellow'),
            ((50)/100, 'red'),
            ((100)/100, 'red'),
            ]
        if test[2] == "rate":
            colors = [  # rate coverage colormap
            ((0)/100, 'gray'),
            ((3)/100, 'red'),
            ((6)/100, 'yellow'),
            ((10)/100, 'yellow'),
            ((20)/100, 'green'),
            ((100)/100, 'green'),
            ]
        if test[2] == "retries":
            colors = [  # retries coverage colormap
            ((0)/5000, 'green'),
            ((900)/5000, 'green'),
            ((1200)/5000, 'yellow'),
            ((3000)/5000, 'yellow'),
            ((4000)/5000, 'red'),
            ((5000)/5000, 'gray'),
            ]
        if test[2] == "rtt":
            colors = [  # rtt coverage colormap
            ((0)/500, 'green'),
            ((200)/500, 'green'),
            ((250)/500, 'yellow'),
            ((300)/500, 'yellow'),
            ((350)/500, 'red'),
            ((500)/500, 'gray'),
            ]
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', colors)

        # Create heatmap overlay using interpolated data with colormap
        heatmap = ax.imshow(
            zi.reshape(floorplan_height, floorplan_width),
            cmap=cmap,
            alpha=0.5,
            extent=[0, floorplan_width, floorplan_height, 0],
            vmin=vmin, vmax=vmax)

        # Remove axis labels and tick marks
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlabel("")
        ax.set_ylabel("")

        # Add red dots for test points
        ax.scatter(x, y, c='red', marker='o', s=50)

        if args.l:  # Add labels below the dots
            for i in range(len(x)):
                ax.text(x[i], y[i] + 18,
                        f'({x[i]}, {y[i]})',
                        ha='center', va='top',
                        fontsize=8)

        # Add a colorbar
        cbar = plt.colorbar(heatmap, orientation='horizontal')
        cbar.set_label(test[1])

        # Add titles and labels
        ax.set_title(f'{protcol.upper()} {direction.title()} {test[1]}')

        # Save to file
        plt.tight_layout()
        plt.savefig(f"{args.t}-lte-{carrier}-{protcol}-{test_name}.jpg")
        plt.close()


def create_chart(args, test, data_points, test_type, test_name, carriers, direction,
                 protcol):

    logger.debug(f'test: {test}')
    logger.debug(f'data_points: {data_points}')

    # Extract timestamps and data columns
    timestamps = []
    for data_point in data_points:
        ts, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)

    numeric_data = [values for _, *values in data_points]
    logger.debug(f'numeric_data: {numeric_data}')

    # Create a list of column labels
    column_labels = []
    for carrier in carriers:
        column_labels.append(f'{carrier} {direction.title()} Local')
        column_labels.append(f'{carrier} {direction.title()} Remote')
        column_labels.append(f'{carrier} {direction.title()} Control')

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))
    logger.debug(f'numeric_data: {numeric_data}')

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        logger.debug(column_labels[i])
        if "remote" in column_labels[i].lower():
            logger.debug("plotting remote series")
            ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Times')
    ax.set_title(f'{protcol.upper()} {direction.title()} {test[1]}')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{args.t}-lte-{protcol}-{test_name}.jpg")
    plt.close()


if __name__ == '__main__':
    # setup global logging
    global logger

    # Setup CLI arugments
    parser = argparse.ArgumentParser(prog='lte_iperf_tcp.py')
    parser.add_argument('-t', help='project title')
    parser.add_argument('-p', help='project folder')
    parser.add_argument('-l', action='store_true', help='label points')
    parser.add_argument('-d', action='store_true', help='debug to console')
    args = parser.parse_args()

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level'],
        args.d,
    )

    main(args)
