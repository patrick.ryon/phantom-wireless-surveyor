import argparse
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import yaml

from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print(f'WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'wifi_signal'


def main(args):
    logger.debug(f"project directory: {args.p}")
    logger.debug(f"ssid: {args.s}")

    project_data = json_data.read_json(f"{args.p}/project.json")
    floorplan_file = os.path.join(args.p, project_data["floor_plan"])
    logger.debug(f"floorplan file: {floorplan_file}")

    # Load the floorplan image
    floorplan_img = mpimg.imread(floorplan_file)

    # Dimensions of the working floorplan defined by image dimensions
    floorplan_width = floorplan_img.shape[1]
    floorplan_height = floorplan_img.shape[0]

    data_file = os.path.join(args.p, "data.sqlite")

    for band in ["2.4", "5", "6"]:
        logger.debug(f"processing band: {band}")

        err, data_points = sqlite_data.get_wifi_signal(
            data_file, args.s, band)

        if not err:
            for level in data_points:
                logger.debug(level)
                # Extract x, y, and z values from data points
                x, y, z = zip(*data_points[level])

                # Define the interpolate grid using floorplan dimensions
                xi, yi = np.meshgrid(
                    np.linspace(0, floorplan_width, floorplan_width),
                    np.linspace(0, floorplan_height, floorplan_height))
                xi, yi = xi.flatten(), yi.flatten()

                logger.debug(xi)
                logger.debug(yi)

                # Create the interpolant using RBF
                rbf = Rbf(x, y, z, function='cubic')

                # Interpolate the data
                zi = rbf(xi, yi)

                # Set values in zi to -110 where they are 0
                zi[zi == 0] = -110

                # Set the minimum and maximum values for the colormap
                vmin = -110  # Set your desired minimum value here
                vmax = -30  # Set your desired maximum value here

                # Set values in zi to the specified color
                cmap_colors = [
                    (0.5, 0.5, 0.5),  # Gray
                    (0, 1, 0),  # Green
                    (1, 1, 0),  # Yellow
                    (1, 0, 0)]  # Red
                cmap = LinearSegmentedColormap.from_list(
                    'custom_colormap', cmap_colors, N=256)

                # Create a figure
                fig, ax = plt.subplots(figsize=(10, 8))

                # Display the floorplan image with 'origin' set to 'upper'
                ax.imshow(floorplan_img, origin='upper')

                # Define the custom colormap with specified color transitions
                colors = [  # primary coverage colormap
                    ((-110+110)/80, 'gray'),
                    ((-70+110)/80, 'gray'),
                    ((-67+110)/80, 'red'),
                    ((-65+110)/80, 'yellow'),
                    ((-58+110)/80, 'green'),
                    ((-52+110)/80, 'green'),
                    ((-48+110)/80, 'green'),
                    ((-30+110)/80, 'green'),]
                if level == "secondary":
                    colors = [  # secondary coverage colormap
                        ((-110+110)/80, 'gray'),
                        ((-75+110)/80, 'gray'),
                        ((-72+110)/80, 'red'),
                        ((-68+110)/80, 'yellow'),
                        ((-62+110)/80, 'green'),
                        ((-58+110)/80, 'green'),
                        ((-52+110)/80, 'green'),
                        ((-30+110)/80, 'green'),]
                cmap = LinearSegmentedColormap.from_list(
                    'custom_colormap', colors)

                # Create heatmap overlay using interpolated data with colormap
                heatmap = ax.imshow(
                    zi.reshape(floorplan_height, floorplan_width),
                    cmap=cmap,
                    alpha=0.5,
                    extent=[0, floorplan_width, floorplan_height, 0],
                    vmin=vmin, vmax=vmax)

                # Remove axis labels and tick marks
                ax.set_xticks([])
                ax.set_yticks([])
                ax.set_xlabel("")
                ax.set_ylabel("")

                # Add red dots for test points
                ax.scatter(x, y, c='red', marker='o', s=50)

                if args.l:  # Add labels below the dots
                    for i in range(len(x)):
                        ax.text(x[i], y[i] + 18,
                                f'({x[i]}, {y[i]})',
                                ha='center', va='top',
                                fontsize=8)

                # Add a colorbar
                cbar = plt.colorbar(heatmap, orientation='horizontal')
                cbar.set_label('Signal strength in dBm')

                # Add titles and labels
                ax.set_title(f'{band}GHz {level} signal strength'.title())

                # Save to file
                plt.tight_layout()
                plt.savefig(f"{args.t}-wifi-{band}ghz-{level}"
                            "-signal-strength.jpg")


if __name__ == '__main__':
    # setup global logging
    global logger

    # Setup CLI arugments
    parser = argparse.ArgumentParser(prog='wifi_5ghz_signal.py')
    parser.add_argument('-t', help='project title')
    parser.add_argument('-p', help='project folder')
    parser.add_argument('-s', help='ssid')
    parser.add_argument('-l', action='store_true', help='label points')
    parser.add_argument('-d', action='store_true', help='debug to console')
    args = parser.parse_args()

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level'],
        args.d,
    )

    main(args)
