import matplotlib.pyplot as plt
import numpy as np

# Sample data in the form of a list of dicts
wifi_data = [{'bssid': '00:9e:1e:e6:b3:fa', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:af:50', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:54', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:52', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e0', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:56', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:51', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:57', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:53', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e2', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:af:55', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e4', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e6', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e7', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e1', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b8:e3', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:4d:24', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:4d:26', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:4d:21', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:4d:27', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:4d:23', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:4d:25', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:b3:f6', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f1', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f7', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f4', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f2', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f0', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f3', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b3:f5', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b8:ef', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:eb', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:ed', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:e9', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:ee', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:e8', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:ec', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b8:ea', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:4d:2f', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:2b', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:2e', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:2d', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:29', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:28', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:2c', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:4d:2a', 'band': 5.0, 'width': 20, 'channel': 48}, {'bssid': '00:9e:1e:e6:b3:ff', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:b3:fb', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:b3:fd', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:b3:f9', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:b3:fe', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:b3:f8', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:b3:fc', 'band': 5.0, 'width': 20, 'channel': 64}, {'bssid': '00:9e:1e:e6:af:5f', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:5b', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:5d', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:59', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:5e', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:58', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:5c', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:af:5a', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:4d:20', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:c1', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:4d:22', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:6a:52', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b8:e5', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b6', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b7', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b1:45', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b3', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b1:43', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b0', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b4', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b2', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b5', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:b1', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b1:47', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b1:44', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b1:46', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:65:bf', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:65:bb', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:b1:4f', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:6a:56', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:6a:55', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:6a:51', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:6a:57', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:65:ba', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:65:bd', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:65:b9', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:6a:5e', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:b1:40', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:98:c6', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:c5', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:c7', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:c3', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:6a:50', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:6a:53', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b1:49', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:b1:4e', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:b1:48', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:b1:4c', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:b1:4a', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:b1:4b', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:6a:5c', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:6a:5a', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:6a:5f', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:6a:5b', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:6a:5d', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:6a:59', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:98:cd', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:98:ca', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:6a:54', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '00:9e:1e:e6:b1:4d', 'band': 5.0, 'width': 20, 'channel': 52}, {'bssid': '00:9e:1e:e6:65:be', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:65:b8', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '00:9e:1e:e6:65:bc', 'band': 5.0, 'width': 20, 'channel': 36}, {'bssid': '98:5d:46:22:cc:0c', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:6a:58', 'band': 5.0, 'width': 20, 'channel': 149}, {'bssid': '00:9e:1e:e6:98:cf', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:98:cb', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:98:c9', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:b1:42', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:b1:41', 'band': 2.4, 'width': 20, 'channel': 1}, {'bssid': '00:9e:1e:e6:98:c4', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:c0', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:c2', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '00:9e:1e:e6:98:ce', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:98:c8', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': '00:9e:1e:e6:98:cc', 'band': 5.0, 'width': 20, 'channel': 157}, {'bssid': 'fc:2d:5e:33:63:55', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '02:04:f3:1e:ff:ac', 'band': 2.4, 'width': 20, 'channel': 6}, {'bssid': '16:56:fe:c7:99:c0', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '12:56:fe:c7:99:c0', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '1a:56:fe:c7:99:c0', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '06:56:fe:c7:99:c0', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '02:56:fe:c7:99:c0', 'band': 2.4, 'width': 20, 'channel': 11}, {'bssid': '0a:56:fe:c7:99:c0', 'band': 2.4, 'width': 20, 'channel': 11}]

# Data for 2.4GHz and 5GHz bands
channels_2_4ghz = np.arange(1, 13)
utilization_2_4ghz = np.zeros(len(channels_2_4ghz))

channels_5ghz = np.array(
    [36, 40, 44, 48,
     52, 56, 60, 64,
     100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144,
     149, 153, 157, 161, 165])
utilization_5ghz = np.zeros(len(channels_5ghz))

# DFS channel range in 5GHz band (example range, adjust based on your specific DFS range)
unii1_s = 34
unii1_e = 50
unii2a_s = 50
unii2a_e = 66
unii2b_s = 66
unii2b_e = 98
unii2c_s = 98
unii2c_e = 147
unii3_s = 147
unii3_e = 167

# Populate 5GHz and 2.4GHz utilization data based on the provided Wi-Fi data
for entry in wifi_data:
    if entry['band'] == 5.0:
        channel_index = np.searchsorted(channels_5ghz, entry['channel'])
        channel_range = 1
        if entry['width'] == 160:
            channel_range = 8
        if entry['width'] == 80:
            channel_range = 4
        if entry['width'] == 40:
            channel_range = 2
        for i in range(channel_range):
            utilization_5ghz[channel_index + i] += 1
    elif entry['band'] == 2.4:
        channel_index = entry['channel'] - 1
        utilization_2_4ghz[channel_index] += 1

# Plotting the Wi-Fi spectrum/channel utilization chart for 2.4GHz
plt.figure(figsize=(12, 6))

# Plot for 2.4GHz
plt.bar(channels_2_4ghz, utilization_2_4ghz, width=0.4, color='blue', alpha=0.7, label='2.4GHz')

# Customize the chart
plt.title('Wi-Fi Channel Utilization - 2.4GHz')
plt.xlabel('Wi-Fi Channels')
plt.ylabel('Count of Networks')
plt.xticks(channels_2_4ghz)
plt.legend()

# Save to file
plt.tight_layout()
plt.savefig(f"wifi_channels_2_4ghz.jpg")

# Plotting the Wi-Fi spectrum/channel utilization chart for 5GHz
plt.figure(figsize=(12, 6))

# Highlight 5GHz channel ranges with vertical lines
line_height = plt.ylim()[1]  # Set line height to the top of the plot
plt.axvspan(unii1_s, unii1_e, color='lightblue', alpha=0.3, label='UNII-1 Range')
plt.axvspan(unii2a_s, unii2a_e, color='lightgreen', alpha=0.3, label='UNII-2a Range')
plt.axvspan(unii2b_s, unii2b_e, color='gray', alpha=0.3, label='UNII-2b Range (Not used)')
plt.axvspan(unii2c_s, unii2c_e, color='lightcoral', alpha=0.3, label='UNII-2c Range')
plt.axvspan(unii3_s, unii3_e, color='lemonchiffon', alpha=0.3, label='UNII-3 Range')

# Plot for 5GHz
plt.bar(channels_5ghz - 2, utilization_5ghz, width=4, color='green', alpha=0.7, label='5GHz', align='edge')

# Customize the chart
plt.title('Wi-Fi Channel Utilization - 5GHz')
plt.xlabel('Wi-Fi Channels')
plt.ylabel('Count of Networks')
plt.xticks(channels_5ghz)
#plt.legend()
plt.legend(loc='upper left', bbox_to_anchor=(0.265, 1.0))

# Save to file
plt.tight_layout()
plt.savefig(f"wifi_channels_5ghz.jpg")
