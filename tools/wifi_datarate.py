import argparse
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import yaml

from datetime import datetime
from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print(f'WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'wifi_datarate'


def main(args):
    logger.debug(f"project directory: {args.p}")

    project_data = json_data.read_json(f"{args.p}/project.json")
    floorplan_file = os.path.join(args.p, project_data["floor_plan"])
    logger.debug(f"floorplan file: {floorplan_file}")

    # Load the floorplan image
    floorplan_img = mpimg.imread(floorplan_file)

    # Load SQLite file
    data_file = os.path.join(args.p, "data.sqlite")
    
    err, data_points = sqlite_data.get_wifi_datarate(data_file)

    if not err:
        # Process tests by direction
        for test_type in ["datarate_tx", "datarate_rx", "datarate_summary"]:
            logger.debug(test_type)
            if test_type == "datarate_tx":
                test_name = f"Wi-Fi Tx Datarate"
                create_heatmap(args, floorplan_img, data_points, test_type,
                               test_name)
            if test_type == "datarate_rx":
                test_name = f"Wi-Fi Rx Datarate"
                create_heatmap(args, floorplan_img, data_points, test_type,
                               test_name)
            if test_type == "datarate_summary":
                test_name = f"Wi-Fi Datarate Summary"
                create_chart(args, data_points, test_type, test_name)


def create_heatmap(args, floorplan_img, data_points, test_type, test_name):
    # Dimensions of the working floorplan defined by image dimensions
    floorplan_width = floorplan_img.shape[1]
    floorplan_height = floorplan_img.shape[0]

    logger.debug(f'{test_name}')
    logger.debug(data_points)
    # Extract x, y, and z values from data points
    x, y, ts, tx, rx = zip(*data_points)
    if test_type == "datarate_tx":
        z = tx
    else:
        z = rx

    # Define the interpolate grid using floorplan dimensions
    xi, yi = np.meshgrid(
        np.linspace(0, floorplan_width, floorplan_width),
        np.linspace(0, floorplan_height, floorplan_height))
    #xi, yi = xi.flatten(), yi.flatten()

    # Create the interpolant using RBF
    rbf = Rbf(x, y, z, function='cubic')

    # Interpolate the data
    zi = rbf(xi, yi)
    logger.info('zi:')
    logger.info(zi)

    # Set values in zi to 0 where they are 0
    zi[zi == 0] = 0

    # Set the minimum and maximum values for the colormap
    vmin = 0  # Set your desired minimum value here
    vmax = 1000  # Set your desired maximum value here

    # Set values in zi to the specified color
    cmap_colors = [
        (0.5, 0.5, 0.5),  # Gray
        (0, 1, 0),  # Green
        (1, 1, 0),  # Yellow
        (1, 0, 0)]  # Red
    cmap = LinearSegmentedColormap.from_list(
        'custom_colormap', cmap_colors, N=256)

    # Create a figure
    fig, ax = plt.subplots(figsize=(10, 8))

    # Display the floorplan image with 'origin' set to 'upper'
    ax.imshow(floorplan_img, origin='upper')

    # Define the custom colormap with specified color transitions
    colors = [  # retries coverage colormap
        ((0)/1000, 'gray'),
        ((20)/1000, 'gray'),
        ((25)/1000, 'red'),
        ((50)/1000, 'yellow'),
        ((100)/1000, 'green'),
        ((1000)/1000, 'green'),
    ]
    cmap = LinearSegmentedColormap.from_list('custom_colormap', colors)

    # Create heatmap overlay using interpolated data with colormap
    heatmap = ax.imshow(
        zi.reshape(floorplan_height, floorplan_width),
        cmap=cmap,
        alpha=0.5,
        extent=[0, floorplan_width, floorplan_height, 0],
        vmin=vmin, vmax=vmax)

    # Remove axis labels and tick marks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel("")
    ax.set_ylabel("")

    # Add red dots for test points
    ax.scatter(x, y, c='red', marker='o', s=50)

    if args.l:  # Add labels below the dots
        for i in range(len(x)):
            ax.text(x[i], y[i] + 18,
                    f'({x[i]}, {y[i]})',
                    ha='center', va='top',
                    fontsize=8)

    # Add a colorbar
    cbar = plt.colorbar(heatmap, orientation='horizontal')
    
    cbar.set_label('Rate (Mbps)')

    # Add titles and labels
    ax.set_title(f'{test_name}')

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{args.t}-wifi-{test_type.replace('_','-')}.jpg")


def create_chart(args, data_points, test_type, test_name):

    # Extract timestamps and data columns
    timestamps = []
    numeric_data = []
    for data_point in data_points:
        _, _, ts, tx, rx = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)
        numeric_data.append([tx, rx])


    #numeric_data = [values for _, _, _, *values in data_points]
    logger.debug(f'numeric_data: {numeric_data}')

    # Create a list of column labels
    column_labels = [
        f'Tx',
        f'Rx'
    ]

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Times')
    ax.set_title(f'{test_name}')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{args.t}-wifi-{test_type.replace('_','-')}.jpg")


if __name__ == '__main__':
    # setup global logging
    global logger

    # Setup CLI arugments
    parser = argparse.ArgumentParser(prog='wifi_datarate.py')
    parser.add_argument('-t', help='project title')
    parser.add_argument('-p', help='project folder')
    parser.add_argument('-l', action='store_true', help='label points')
    parser.add_argument('-d', action='store_true', help='debug to console')
    args = parser.parse_args()

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level'],
        args.d,
    )

    main(args)
