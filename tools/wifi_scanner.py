import argparse
import os
import sys
import trio
import yaml

import lib.iwconfig as iwconfig
import lib.iwscan as iwscan
import lib.sqlite_data as sqlite_data

# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print(f'WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'wifi_scanner'


def main(args):
    if args.r:
        # run report
        logger.debug('run report flag found')
        run_report()
    else:
        # Create project directory
        if args.p:
            try:
                os.makedirs(args.p, exist_ok=True)
            except Exception as e:
                logger.error(e)
                return False
            if args.t:
                passed = True
                if passed:
                    passed = sqlite_data.create_connection(f"{args.p}/{args.t}.sqlite")
                if passed:
                    passed = sqlite_data.create_scan_tbl(f"{args.p}/{args.t}.sqlite")

        trio.run(run_scanner, args)


def run_report(args):
    logger.debug('generate report')


async def run_scanner(args):
    logger.debug('running scanner')
    logger.debug(args.i)
    while True:
        logger.debug('running scan loop')
        await run_wifi_scan(args.i)
        await trio.sleep(args.l)


async def run_wifi_scan(wintf):
    err, data = await iwscan.scan(wintf)
    if not err:
        wifi_scan_data = iwscan.parse(data)
        logger.debug("wifi scan data:")
        logger.debug(wifi_scan_data)
        counter = 0
        for bssid in wifi_scan_data:
            if wifi_scan_data[bssid]['ssid'] == args.s:
                logger.debug(bssid)
                counter += 1
        if counter == 0:
            logger.error(f'SSID {args.s} not found.')
        else:
            logger.info(f'{counter} BSSIDs found for SSID {args.s}')
            if args.p and args.t:
                sqlite_data.write_wifi_scan(
                    f'{args.p}/{args.t}.sqlite',
                    wifi_scan_data
                )


if __name__ == '__main__':
    # setup global logging
    global logger

    # Setup CLI arugments
    parser = argparse.ArgumentParser(prog='wifi_dscanner.py')
    parser.add_argument('-i', help='wifi interface')
    parser.add_argument('-l', help='loop interval in sec', default=30, type=int)
    parser.add_argument('-t', help='scanner title')
    parser.add_argument('-p', help='scanner folder')
    parser.add_argument('-f', help='frequency')
    parser.add_argument('-s', help='ssid')
    parser.add_argument('-r', help='generate report')
    parser.add_argument('-d', help='debug')
    args = parser.parse_args()

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level'],
        args.d,
    )

    main(args)
