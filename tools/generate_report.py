import argparse
import os
import sys
import yaml

import lib.json_data as json_data

from docxtpl import DocxTemplate, InlineImage
from docx.shared import Inches


# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print('WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
    context_file = open(os.path.join(
        conf_basedir, 'config', 'context.yaml'), 'r')
    context_conf = yaml.safe_load(context_file)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'generate_report'


def load_images(doc, title, folder):
    project_data = json_data.read_json(f"{args.p}/project.json")
    img_return = {}

    floorplan_file = os.path.join(args.p, project_data["floor_plan"])
    logger.debug(f"floorplan file: {floorplan_file}")
    if os.path.isfile(floorplan_file):
        img_return["floorplan"] = InlineImage(
            doc,
            image_descriptor=floorplan_file,
            width=Inches(6.5))
    else:
        logger.warning(f'{floorplan_file} not found')
        img_return["floorplan"] = InlineImage(
            doc,
            image_descriptor="placeholder.png",
            width=Inches(6.5))

    img_types = [
        "wifi-2.4ghz-primary-signal-strength",  # wifi signal
        "wifi-5ghz-primary-signal-strength",
        "wifi-2.4ghz-secondary-signal-strength",
        "wifi-5ghz-secondary-signal-strength",
        "wifi-datarate-summary",  # wifi datarate
        "wifi-datarate-tx",
        "wifi-datarate-rx",
        "wifi-udp-summary-upload-rate",   # wifi udp upload
        "wifi-udp-remote-upload-rate",
        "wifi-udp-local-upload-rate",
        "wifi-udp-summary-upload-jitter",
        "wifi-udp-remote-upload-jitter",
        "wifi-udp-local-upload-jitter",
        "wifi-udp-summary-upload-lost_perc",
        "wifi-udp-remote-upload-lost_perc",
        "wifi-udp-local-upload-lost_perc",
        "wifi-udp-summary-download-rate",   # wifi udp download
        "wifi-udp-remote-download-rate",
        "wifi-udp-local-download-rate",
        "wifi-udp-summary-download-jitter",
        "wifi-udp-remote-download-jitter",
        "wifi-udp-local-download-jitter",
        "wifi-udp-summary-download-lost_perc",
        "wifi-udp-remote-download-lost_perc",
        "wifi-udp-local-download-lost_perc",
        "wifi-tcp-summary-upload-rate",  # wifi tcp upload
        "wifi-tcp-remote-upload-rate",
        "wifi-tcp-local-upload-rate",
        "wifi-tcp-summary-upload-retries",
        "wifi-tcp-remote-upload-retries",
        "wifi-tcp-local-upload-retries",
        "wifi-tcp-summary-upload-rtt",
        "wifi-tcp-remote-upload-rtt",
        "wifi-tcp-local-upload-rtt",
        "wifi-tcp-summary-download-rate",  # wifi tcp download
        "wifi-tcp-remote-download-rate",
        "wifi-tcp-local-download-rate",
        "wifi-tcp-summary-download-retries",
        "wifi-tcp-remote-download-retries",
        "wifi-tcp-local-download-retries",
        "lte-summary-rsrp",  # lte signal
        "lte-summary-rsrq",
        "lte-summary-sinr",
        "lte-AT&T-rsrp",
        "lte-T-Mobile-rsrp",
        "lte-Verizon-rsrp",
        "lte-AT&T-rsrq",
        "lte-T-Mobile-rsrq",
        "lte-Verizon-rsrq",
        "lte-AT&T-sinr",
        "lte-T-Mobile-sinr",
        "lte-Verizon-sinr",
        "lte-udp-summary-upload-rate",  # lte udp upload
        "lte-AT&T-udp-remote-upload-rate",
        "lte-T-Mobile-udp-remote-upload-rate",
        "lte-Verizon-udp-remote-upload-rate",
        "lte-udp-summary-upload-jitter",
        "lte-AT&T-udp-remote-upload-jitter",
        "lte-T-Mobile-udp-remote-upload-jitter",
        "lte-Verizon-udp-remote-upload-jitter",
        "lte-udp-summary-upload-lost_perc",
        "lte-AT&T-udp-remote-upload-lost_perc",
        "lte-T-Mobile-udp-remote-upload-lost_perc",
        "lte-Verizon-udp-remote-upload-lost_perc",
        "lte-udp-summary-download-rate",  # lte udp download
        "lte-AT&T-udp-remote-download-rate",
        "lte-T-Mobile-udp-remote-download-rate",
        "lte-Verizon-udp-remote-download-rate",
        "lte-udp-summary-download-jitter",
        "lte-AT&T-udp-remote-download-jitter",
        "lte-T-Mobile-udp-remote-download-jitter",
        "lte-Verizon-udp-remote-download-jitter",
        "lte-udp-summary-download-lost_perc",
        "lte-AT&T-udp-remote-download-lost_perc",
        "lte-T-Mobile-udp-remote-download-lost_perc",
        "lte-Verizon-udp-remote-download-lost_perc",
        "lte-tcp-summary-upload-rate",  # lte tcp upload
        "lte-AT&T-tcp-remote-upload-rate",
        "lte-T-Mobile-tcp-remote-upload-rate",
        "lte-Verizon-tcp-remote-upload-rate",
        "lte-tcp-summary-upload-retries",
        "lte-AT&T-tcp-remote-upload-retries",
        "lte-T-Mobile-tcp-remote-upload-retries",
        "lte-Verizon-tcp-remote-upload-retries",
        "lte-tcp-summary-download-rate",  # lte udp upload
        "lte-AT&T-tcp-remote-download-rate",
        "lte-T-Mobile-tcp-remote-download-rate",
        "lte-Verizon-tcp-remote-download-rate",
        "lte-tcp-summary-download-retries",
        "lte-AT&T-tcp-remote-download-retries",
        "lte-T-Mobile-tcp-remote-download-retries",
        "lte-Verizon-tcp-remote-download-retries",
        "lte-tcp-summary-upload-rtt",
        "lte-AT&T-tcp-remote-upload-rtt",
        "lte-T-Mobile-tcp-remote-upload-rtt",
        "lte-Verizon-tcp-remote-upload-rtt",
    ]


    for img_type in img_types:
        if os.path.isfile(f"{title}-{img_type}.jpg"):
            # Modify the variable representation
            modified_img_type = img_type.replace("&", "_and_")
            img_return[modified_img_type] = InlineImage(
                doc,
                image_descriptor=f"{title}-{img_type}.jpg",
                width=Inches(6.5))
        else:
            logger.warning(f'{title}-{img_type}.jpg not found')
            img_return[img_type] = InlineImage(
                doc,
                image_descriptor="placeholder.png",
                width=Inches(6.5))

    return img_return


def main(args):
    logger.debug(args)
    doc = DocxTemplate(args.i)

    images = load_images(doc, args.t, args.p)
    logger.debug(images)
    context_conf["images"] = {}

    logger.debug('context before images:')
    logger.debug(context_conf)
    for image in images:
        logger.debug(image)
        context_conf["images"][image] = images[image]

    logger.debug('context after images:')
    logger.debug(context_conf)

    doc.render(context_conf)
    doc.save(args.o)


if __name__ == '__main__':
    # setup global logging
    global logger

    # Setup CLI arugments
    parser = argparse.ArgumentParser(prog='generate_report.py')
    parser.add_argument('-t', help='project title', required=True)
    parser.add_argument('-p', help='project folder', required=True)
    parser.add_argument('-i', help='input template document', required=True)
    parser.add_argument('-o', help='output generated document', required=True)
    parser.add_argument('-d', action='store_true', help='debug to console')
    args = parser.parse_args()

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level'],
        args.d,
    )

    main(args)
