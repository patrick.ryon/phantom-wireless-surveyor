import argparse
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import yaml

from datetime import datetime
from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print(f'WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'lte_rsrq'


def main(args):
    data_file = os.path.join(args.p, "data.sqlite")

    carriers = sqlite_data.get_lte_carriers(data_file, 'lte')

    err, dp_by_carrier, dp_summary = sqlite_data.get_lte_rsrq(
        data_file, carriers)
    
    if not err:
        create_heatmaps(args, dp_by_carrier, carriers)
        create_chart(args, dp_summary, carriers)


def create_chart(args, data_points, carriers):

    # Extract timestamps and data columns
    timestamps = []
    for data_point in data_points:
        ts, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)

    numeric_data = [values for _, *values in data_points]

    # Create a list of column labels
    column_labels = carriers

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Times')
    ax.set_title(f'RSRQ Values over Time')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{args.t}-lte-summary-rsrq.jpg")


def create_heatmaps(args, data_points, carriers):
    logger.debug(f"project directory: {args.p}")

    project_data = json_data.read_json(f"{args.p}/project.json")
    floorplan_file = os.path.join(args.p, project_data["floor_plan"])
    logger.debug(f"floorplan file: {floorplan_file}")

    # Load the floorplan image
    floorplan_img = mpimg.imread(floorplan_file)

    # Dimensions of the working floorplan defined by image dimensions
    floorplan_width = floorplan_img.shape[1]
    floorplan_height = floorplan_img.shape[0]

    for carrier in carriers:
        # Extract x, y, and z values from data points
        x, y, z = zip(*data_points[carrier])

        # Define the interpolate grid using floorplan dimensions
        xi, yi = np.meshgrid(
            np.linspace(0, floorplan_width, floorplan_width),
            np.linspace(0, floorplan_height, floorplan_height))
        xi, yi = xi.flatten(), yi.flatten()

        logger.debug(xi)
        logger.debug(yi)

        # Create the interpolant using RBF
        rbf = Rbf(x, y, z, function='cubic')

        # Interpolate the data
        zi = rbf(xi, yi)

        # Set values in zi to -30 where they are 0
        zi[zi == 0] = -30

        # Set the minimum and maximum values for the colormap
        vmin = -30  # Set your desired minimum value here
        vmax = 0  # Set your desired maximum value here

        # Set values in zi to the specified color
        cmap_colors = [
            (0.5, 0.5, 0.5),  # Gray
            (0, 1, 0),  # Green
            (1, 1, 0),  # Yellow
            (1, 0, 0)]  # Red
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', cmap_colors, N=256)

        # Create a figure
        fig, ax = plt.subplots(figsize=(10, 8))

        # Display the floorplan image with 'origin' set to 'upper'
        ax.imshow(floorplan_img, origin='upper')

        # Define the custom colormap with specified color transitions
        colors = [  # primary coverage colormap
            ((-30+30)/30, 'gray'),  # 0
            ((-21+30)/30, 'gray'),
            ((-19+30)/30, 'red'), 
            ((-17+30)/30, 'yellow'),
            ((-15+30)/30, 'yellow'),
            ((-13+30)/30, 'green'),
            ((0+30)/30, 'green'),]  # 1
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', colors)

        # Create heatmap overlay using interpolated data with colormap
        heatmap = ax.imshow(
            zi.reshape(floorplan_height, floorplan_width),
            cmap=cmap,
            alpha=0.5,
            extent=[0, floorplan_width, floorplan_height, 0],
            vmin=vmin, vmax=vmax)

        # Remove axis labels and tick marks
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlabel("")
        ax.set_ylabel("")

        # Add red dots for test points
        ax.scatter(x, y, c='red', marker='o', s=50)

        if args.l:  # Add labels below the dots
            for i in range(len(x)):
                ax.text(x[i], y[i] + 18,
                        f'({x[i]}, {y[i]})',
                        ha='center', va='top',
                        fontsize=8)

        # Add a colorbar
        cbar = plt.colorbar(heatmap, orientation='horizontal')
        cbar.set_label('RSRQ Value (dB)')

        # Add titles and labels
        ax.set_title(f'{carrier} RSRQ')

        # Save to file
        plt.tight_layout()
        plt.savefig(f"{args.t}-lte-{carrier}"
                    "-rsrq.jpg")


if __name__ == '__main__':
    # setup global logging
    global logger

    # Setup CLI arugments
    parser = argparse.ArgumentParser(prog='lte_rsrq.py')
    parser.add_argument('-t', help='project title')
    parser.add_argument('-p', help='project folder')
    parser.add_argument('-l', action='store_true', help='label points')
    parser.add_argument('-d', action='store_true', help='debug to console')
    args = parser.parse_args()

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level'],
        args.d,
    )

    main(args)
