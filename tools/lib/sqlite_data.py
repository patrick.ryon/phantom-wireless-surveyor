import copy
import logging
import sqlite3
import time

from sqlite3 import Error

logger = logging.getLogger("lib_sqlite_data")


# create a database connection to the SQLite database specified by db_file
def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        logger.error(e)

    return conn


# update timestamp on point when tested
def point_tested(db_file, id):
    conn = create_connection(db_file)
    statement = "SELECT datetime('now');"
    cur = conn.cursor()
    cur.execute(statement)
    timestamp = cur.fetchone()
    timestamp = timestamp[0]
    statement = ''' UPDATE points
                    SET tested = ? ,
                        tested_at = ?,
                        type = ?
                    WHERE id = ? '''
    task = (1, timestamp, "test", id)
    try:
        cur = conn.cursor()
        cur.execute(statement, task)
        conn.commit()

        return True
    except Error as e:
        logger.error("error in point_tested()")
        logger.error(e)

    return False


# create wifi table in db_file
def create_scan_tbl(db_file):
    conn = create_connection(db_file)
    statement = """ CREATE TABLE IF NOT EXISTS scan(
                    scan_time TEXT not NULL,
                    bssid TEXT not NULL,
                    essid TEXT,
                    freq INTEGER,
                    band TEXT,
                    channel INTEGER,
                    clients INTEGER,
                    utilization INTEGER,
                    width INTEGER,
                    signal_str REAL,
                    signal_qual REAL,
                    datarates TEXT
                ); """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        conn.commit()
        return True
    except Error as e:
        logger.error("error in create_wifi_tbl()")
        logger.error(e)
    return False


# write wifi scan info to wifi table
def write_wifi_scan(db_file, data):
    has_errors = False
    conn = create_connection(db_file)

    statement = "SELECT datetime('now');"
    cur = conn.cursor()
    cur.execute(statement)
    timestamp = cur.fetchone()
    timestamp = timestamp[0]

    for bssid in data:
        logger.debug(bssid)
        logger.debug(data[bssid])
        quality = float(data[bssid]["signal"]) + 110

        statement = """ INSERT INTO scan(
            scan_time,
            bssid,
            essid,
            freq,
            band,
            channel,
            clients,
            utilization,
            width,
            signal_str,
            signal_qual,
            datarates
            )
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) """
        entry = (
            timestamp,
            bssid,
            data[bssid]['ssid'],
            data[bssid]['freq'],
            data[bssid]['band'],
            data[bssid]['chan'],
            data[bssid]['clients'],
            data[bssid]['util'],
            data[bssid]['width'],
            data[bssid]['signal'],
            quality,
            data[bssid]['rates']
            )
        try:
            cur = conn.cursor()
            cur.execute(statement, entry)
            conn.commit()
        except Error as e:
            logger.error("error in write_wifi_scan()")
            logger.error(e)
            has_errors = True
    conn.close()
    return has_errors


def get_wifi_signal(db_file, ssid, band):
    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`
        FROM `points`
        ;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()

        # build points dict with min assumed signal at all points
        points_dict = {}
        for point in points:
            points_dict[point[0]] = {
                "x": point[1],
                "y": point[2],
                0: {
                    "signal_str": -110
                },
                1: {
                    "signal_str": -110
                },
            }
        logger.debug(points_dict)

        # query for signal at points
        statement = f"""
            SELECT `point_id`, `signal_str`
            FROM `wifi`
            WHERE `essid` LIKE '{ssid}'
            AND `band` LIKE '{band}'
            ORDER BY `point_id`, `signal_str`;
            """

        try:
            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()

            # fill in points_dict with actual values
            level = 0
            last_id = ''
            for row in rows:
                logger.debug(row)
                if last_id != row[0]:  # new id
                    points_dict[row[0]][0]["signal_str"] = row[1]
                    level = 1
                if last_id == row[0] and level == 1:  # second id
                    points_dict[row[0]][1]["signal_str"] = row[1]
                    level = 0
                last_id = row[0]
            logger.debug(points_dict)

            pri = []
            sec = []

            # convert points_dict to tupple for matlab
            for point in points_dict:
                logger.debug(point)
                pri.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point][0]["signal_str"])
                )
                sec.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point][1]["signal_str"])
                )

            return False, {"primary": pri, "secondary": sec}

        # query of wifi table failed
        except Error as e:
            logger.error(e)

    # query of points table failed
    except Error as e:
        logger.error(e)

    return True, {}


def get_wifi_datarate(db_file):
    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`, `tested_at`
        FROM `points`
        ORDER BY `tested_at`
        ;
    """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, []

    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "datarate_tx": 0,
            "datarate_rx": 0
        }
    logger.debug(points_dict)

    # query for signal at points
    statement = f"""
        SELECT `point_id`, `tx_datarate`, `rx_datarate`
        FROM `wifi`
        WHERE `connected` LIKE '1'
        ORDER BY `point_id`;
    """

    try:
        cur = conn.cursor()
        cur.execute(statement)
        rows = cur.fetchall()
    # query of wifi table failed
    except Error as e:
        logger.error(e)
        return True, []

    # fill in points_dict with actual values
    for row in rows:
        logger.debug(row)
        points_dict[row[0]]["datarate_tx"] = float(row[1])/1000
        points_dict[row[0]]["datarate_rx"] = float(row[2])/1000
    logger.debug(points_dict)

    dp = []

    # convert points_dict to tupple for matlab
    for point in points_dict:
        logger.debug(point)
        dp.append((
            points_dict[point]["x"],
            points_dict[point]["y"],
            points_dict[point]["time"],
            points_dict[point]["datarate_tx"],
            points_dict[point]["datarate_rx"])
        )

    return False, dp


def get_wifi_iperf(db_file, test, protocol):
    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`, `tested_at`
        FROM `points`
        ;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()

        # build points dict with min assumed signal at all points
        zero_val = 0
        if test == 'retries': zero_val = 5000
        if test == 'rtt': zero_val = 500
        if test == 'jitter': zero_val = 200
        if test == 'lost_perc': zero_val = 100

        points_dict = {}
        for point in points:
            points_dict[point[0]] = {
                "x": point[1],
                "y": point[2],
                "time": point[3],
                "f": {
                    "l": zero_val,
                    "r": zero_val,
                    "s": zero_val,
                },
                "r": {
                    "l": zero_val,
                    "r": zero_val,
                    "s": zero_val,
                },
            }
        logger.debug(points_dict)
        cur.close()

        # query for signal at points
        statement = f"""
            SELECT `point_id`,
                CASE
	                WHEN `f_bps` <= 0 THEN {zero_val}
		            ELSE `f_{test}`
	            END as f_{test},
	            CASE
	                WHEN `r_bps` <= 0 THEN {zero_val}
		            ELSE `r_{test}`
	            END as r_{test},
	            `type`,
	            `server`
            FROM `perf`
            WHERE `protocol` LIKE '{protocol}'
            ORDER BY `point_id`
            ;
            """

        try:
            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()

            f_l = []  # tuple for local upload heatmap
            r_l = []  # tuple for local download heatmap
            f_r = []  # tuple for remote upload heatmap
            r_r = []  # tuple for remote download heatmap
            u = []  # tuple for upload chart
            d = []  # tuple for download chart

            # fill in points_dict with actual values
            for row in rows:
                logger.debug(row)
                if test == "bps":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 0
                    else:
                        val_f = float(row[1])/1000000
                    logger.debug(row[2])
                    if float(row[2]) == -1:
                        val_r = 0
                    else:
                        val_r = float(row[2])/1000000
                    if row[3] == "wifi":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"] = float(val_f)
                            points_dict[row[0]]["r"]["l"] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"] = float(val_f)
                            points_dict[row[0]]["r"]["r"] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"] = float(val_f)
                        points_dict[row[0]]["r"]["s"] = float(val_r)
                elif test == "rtt":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 500
                    else:
                        val_f = float(row[1])/1000
                    if float(row[2]) == -1:
                        val_r = 500
                    else:
                        val_r = float(row[2])/1000
                    if row[3] == "wifi":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"] = float(val_f)
                            points_dict[row[0]]["r"]["l"] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"] = float(val_f)
                            points_dict[row[0]]["r"]["r"] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"] = float(val_f)
                        points_dict[row[0]]["r"]["s"] = float(val_r)
                else:
                    if row[3] == "wifi":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"] = float(row[1])
                            points_dict[row[0]]["r"]["l"] = float(row[2])
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"] = float(row[1])
                            points_dict[row[0]]["r"]["r"] = float(row[2])
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"] = float(row[1])
                        points_dict[row[0]]["r"]["s"] = float(row[2])
            logger.debug(points_dict)

            # convert points_dict to tupple for matlab
            for point in points_dict:
                logger.debug(point)
                logger.debug(points_dict[point])
                f_l.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["f"]["l"],)
                )
                r_l.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["r"]["l"],)
                )
                f_r.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["f"]["r"],)
                )
                r_r.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["r"]["r"],)
                )
                u.append((
                    points_dict[point]["time"],
                    points_dict[point]["f"]["l"],
                    points_dict[point]["f"]["r"],
                    points_dict[point]["f"]["s"],
                ))
                d.append((
                    points_dict[point]["time"],
                    points_dict[point]["r"]["l"],
                    points_dict[point]["r"]["r"],
                    points_dict[point]["r"]["s"],
                ))

            return False, {
                "f_l": f_l,
                "r_l": r_l,
                "f_r": f_r,
                "r_r": r_r,
                "u": u,
                "d": d}

        # query of perf table failed
        except Error as e:
            logger.error(e)

    # query of points table failed
    except Error as e:
        logger.error(e)

    return True, {}


def get_lte_carriers(db_file, tbl):
    carriers = []
    conn = create_connection(db_file)
    # query for points
    statement = f"""
        SELECT DISTINCT `carrier` FROM `{tbl}`;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        res = cur.fetchall()
        logger.debug(res)
        for row in res:
            carriers.append(row[0])
        logger.debug(carriers)
        # query of points table failed
    except Error as e:
        logger.error(e)
    return carriers


def get_lte_rsrp(db_file, carriers):
    # build filler dict for carrier rsrp values
    default_list = {}
    for carrier in carriers:
        default_list[carrier] = -120

    conn = create_connection(db_file)

    try:
        # query for points
        statement = """
            SELECT `id`, `x`, `y`, `tested_at`
            FROM `points`
            ORDER BY `tested_at`
            ;
            """

        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, {}, []
    
    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "rsrp": default_list.copy()
        }
        logger.debug(points_dict[point[0]])
    logger.debug(points_dict)

    logger.debug(f'carriers: {carriers}')
    for carrier in carriers:
        # query for signal at points
        logger.debug(f'carrier: {carrier}')
        try:
            statement = f"""
                SELECT `point_id`, `rsrp`
                FROM `lte`
                WHERE `carrier` LIKE '{carrier}'
                ORDER BY `point_id`, `rsrp`;
            """

            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()
        # query of lte table failed
        except Error as e:
            logger.error(e)
            return True, {}, []

        points_dict_copy = copy.deepcopy(points_dict)

        # fill in points_dict with actual values
        logger.debug(f'sql rows: {rows}')
        for row in rows:
            logger.debug(f'row in rows: {row}')
            logger.debug(f'row id: {row[0]}')
            points_dict[row[0]]["rsrp"][carrier] = row[1]
        
        # convert points_dict to tupple for matlab for heatmaps
        # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
        rsrp_list_by_carrier = {}
        for carrier in carriers:
            rsrp_list_by_carrier[carrier] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                rsrp_list_by_carrier[carrier].append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["rsrp"][carrier])
                )
        logger.debug(rsrp_list_by_carrier)

        # convert points_dict to tupple for matlab for summary chart
        # [(time1,rsrp:carrier1,rsrp:carrier2,rsrp:carrier3)...]
        rsrp_list_summary = []
        for point in points_dict:
            logger.debug(points_dict[point])
            temp_tup = (points_dict[point]["time"],)
            for carrier in points_dict[point]["rsrp"]:
                logger.debug(f'{carrier}: {points_dict[point]["rsrp"][carrier]}')
                temp_tup = temp_tup + (points_dict[point]["rsrp"][carrier],)
                logger.debug(temp_tup)
            rsrp_list_summary.append(temp_tup)
        logger.debug(rsrp_list_summary)

    return False, rsrp_list_by_carrier, rsrp_list_summary


def get_lte_rsrq(db_file, carriers):
    # build filler dict for carrier rsrq values
    default_list = {}
    for carrier in carriers:
        default_list[carrier] = -120

    conn = create_connection(db_file)

    try:
        # query for points
        statement = """
            SELECT `id`, `x`, `y`, `tested_at`
            FROM `points`
            ORDER BY `tested_at`
            ;
            """

        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, {}, []
    
    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "rsrq": default_list.copy()
        }
        logger.debug(points_dict[point[0]])
    logger.debug(points_dict)

    logger.debug(f'carriers: {carriers}')
    for carrier in carriers:
        # query for signal at points
        logger.debug(f'carrier: {carrier}')
        try:
            statement = f"""
                SELECT `point_id`, `rsrq`
                FROM `lte`
                WHERE `carrier` LIKE '{carrier}'
                ORDER BY `point_id`, `rsrq`;
            """

            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()
        # query of lte table failed
        except Error as e:
            logger.error(e)
            return True, {}, []

        points_dict_copy = copy.deepcopy(points_dict)

        # fill in points_dict with actual values
        logger.debug(f'sql rows: {rows}')
        for row in rows:
            logger.debug(f'row in rows: {row}')
            logger.debug(f'row id: {row[0]}')
            points_dict[row[0]]["rsrq"][carrier] = row[1]
        
        # convert points_dict to tupple for matlab for heatmaps
        # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
        rsrq_list_by_carrier = {}
        for carrier in carriers:
            rsrq_list_by_carrier[carrier] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                rsrq_list_by_carrier[carrier].append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["rsrq"][carrier])
                )
        logger.debug(rsrq_list_by_carrier)

        # convert points_dict to tupple for matlab for summary chart
        # [(time1,rsrq:carrier1,rsrq:carrier2,rsrq:carrier3)...]
        rsrq_list_summary = []
        for point in points_dict:
            logger.debug(points_dict[point])
            temp_tup = (points_dict[point]["time"],)
            for carrier in points_dict[point]["rsrq"]:
                logger.debug(f'{carrier}: {points_dict[point]["rsrq"][carrier]}')
                temp_tup = temp_tup + (points_dict[point]["rsrq"][carrier],)
                logger.debug(temp_tup)
            rsrq_list_summary.append(temp_tup)
        logger.debug(rsrq_list_summary)

    return False, rsrq_list_by_carrier, rsrq_list_summary


def get_lte_sinr(db_file, carriers):
    # build filler dict for carrier sinr values
    default_list = {}
    for carrier in carriers:
        default_list[carrier] = -120

    conn = create_connection(db_file)

    try:
        # query for points
        statement = """
            SELECT `id`, `x`, `y`, `tested_at`
            FROM `points`
            ORDER BY `tested_at`
            ;
            """

        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, {}, []
    
    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "sinr": default_list.copy()
        }
        logger.debug(points_dict[point[0]])
    logger.debug(points_dict)

    logger.debug(f'carriers: {carriers}')
    for carrier in carriers:
        # query for signal at points
        logger.debug(f'carrier: {carrier}')
        try:
            statement = f"""
                SELECT `point_id`, `sinr`
                FROM `lte`
                WHERE `carrier` LIKE '{carrier}'
                ORDER BY `point_id`, `sinr`;
            """

            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()
        # query of lte table failed
        except Error as e:
            logger.error(e)
            return True, {}, []

        # fill in points_dict with actual values
        logger.debug(f'sql rows: {rows}')
        for row in rows:
            logger.debug(f'row in rows: {row}')
            logger.debug(f'row id: {row[0]}')
            points_dict[row[0]]["sinr"][carrier] = row[1]
        
        # convert points_dict to tupple for matlab for heatmaps
        # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
        sinr_list_by_carrier = {}
        for carrier in carriers:
            sinr_list_by_carrier[carrier] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                sinr_list_by_carrier[carrier].append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["sinr"][carrier])
                )
        logger.debug(sinr_list_by_carrier)

        # convert points_dict to tupple for matlab for summary chart
        # [(time1,sinr:carrier1,sinr:carrier2,sinr:carrier3)...]
        sinr_list_summary = []
        for point in points_dict:
            logger.debug(points_dict[point])
            temp_tup = (points_dict[point]["time"],)
            for carrier in points_dict[point]["sinr"]:
                logger.debug(f'{carrier}: {points_dict[point]["sinr"][carrier]}')
                temp_tup = temp_tup + (points_dict[point]["sinr"][carrier],)
                logger.debug(temp_tup)
            sinr_list_summary.append(temp_tup)
        logger.debug(sinr_list_summary)

    return False, sinr_list_by_carrier, sinr_list_summary


def get_lte_iperf(db_file, test, carriers, protocol):
    # build points dict with min assumed signal at all points
    zero_val = 0
    if test == 'retries': zero_val = 5000
    if test == 'rtt': zero_val = 500
    if test == 'jitter': zero_val = 200
    if test == 'lost_perc': zero_val = 100

    # build filler dict for carrier iperf values
    default_list = {}
    logger.debug(carriers)
    for carrier in carriers:
        default_list[carrier] = zero_val
    logger.debug(default_list)

    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`, `tested_at`
        FROM `points`
        ;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()

        points_dict = {}

        for point in points:
            points_dict[point[0]] = {
                "x": point[1],
                "y": point[2],
                "time": point[3],
                "f": {
                    "l": default_list.copy(),
                    "r": default_list.copy(),
                    "s": default_list.copy(),
                },
                "r": {
                    "l": default_list.copy(),
                    "r": default_list.copy(),
                    "s": default_list.copy(),
                },
            }
        logger.debug(points_dict)
        cur.close()

        if test == 'rtt': zero_val = 500000

        # query for signal at points
        statement = f"""
            SELECT `point_id`,
                CASE
	                WHEN `f_bps` <= 0 THEN {zero_val}
		            ELSE `f_{test}`
	            END as f_{test},
	            CASE
	                WHEN `r_bps` <= 0 THEN {zero_val}
		            ELSE `r_{test}`
	            END as r_{test},
	            `type`,
	            `server`,
                `carrier`
            FROM `perf`
            WHERE `protocol` LIKE '{protocol}'
            ORDER BY `point_id`
            ;
            """

        try:
            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()

            # fill in points_dict with actual values
            for row in rows:
                logger.debug(row)
                if test == "bps":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 0
                    else:
                        val_f = float(row[1])/1000000
                    logger.debug(row[2])
                    if float(row[2]) == -1:
                        val_r = 0
                    else:
                        val_r = float(row[2])/1000000
                    if row[3] == "lte":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["l"][row[5]] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["r"][row[5]] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"][row[5]] = float(val_f)
                        points_dict[row[0]]["r"]["s"][row[5]] = float(val_r)
                elif test == "rtt":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 500
                    else:
                        val_f = float(row[1])/1000
                    if float(row[2]) == -1:
                        val_r = 500
                    else:
                        val_r = float(row[2])/1000
                    if row[3] == "lte":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["l"][row[5]] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["r"][row[5]] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"][row[5]] = float(val_f)
                        points_dict[row[0]]["r"]["s"][row[5]] = float(val_r)
                else:
                    logger.debug(row[1])
                    if row[3] == "lte":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"][row[5]] = float(row[1])
                            points_dict[row[0]]["r"]["l"][row[5]] = float(row[2])
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"][row[5]] = float(row[1])
                            points_dict[row[0]]["r"]["r"][row[5]] = float(row[2])
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"][row[5]] = float(row[1])
                        points_dict[row[0]]["r"]["s"][row[5]] = float(row[2])
            logger.debug(points_dict)

            # convert points_dict to tupple for matlab for heatmaps
            # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
            iperf_list_by_carrier = {}
            for carrier in carriers:
                iperf_list_by_carrier[carrier] = {
                    "f_l": [],
                    "f_r": [],
                    "f_s": [],
                    "r_l": [],
                    "r_r": [],
                    "r_s": []
                }
                for point in points_dict:
                    logger.debug(points_dict[point])
                    iperf_list_by_carrier[carrier]["f_l"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["f"]["l"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["r_l"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["r"]["l"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["f_r"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["f"]["r"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["r_r"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["r"]["r"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["f_s"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["f"]["s"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["r_s"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["r"]["s"][carrier])
                    )
            logger.debug(iperf_list_by_carrier)

            # convert points_dict to tupple for matlab for summary chart
            # [(time1,v1:carrier1,v1:carrier2,v1:carrier3)...]
            iperf_list_summary = {}
            iperf_list_summary['u'] = []
            iperf_list_summary['d'] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                u_temp_tup = (points_dict[point]["time"],)
                d_temp_tup = (points_dict[point]["time"],)
                for carrier in carriers:
                    logger.debug(f'{carrier}: {points_dict[point]["f"]["l"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["f"]["r"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["f"]["s"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["r"]["l"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["r"]["r"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["r"]["s"][carrier]}')
                    u_temp_tup = u_temp_tup + (
                        points_dict[point]["f"]["l"][carrier],
                        points_dict[point]["f"]["r"][carrier],
                        points_dict[point]["f"]["s"][carrier],
                    )
                    d_temp_tup = d_temp_tup + (
                        points_dict[point]["r"]["l"][carrier],
                        points_dict[point]["r"]["r"][carrier],
                        points_dict[point]["r"]["s"][carrier],
                    )
                    logger.debug(u_temp_tup)
                    logger.debug(d_temp_tup)
                iperf_list_summary['u'].append(u_temp_tup)
                iperf_list_summary['d'].append(u_temp_tup)
            logger.debug(iperf_list_summary['u'])
            logger.debug(iperf_list_summary['d'])

            return False, iperf_list_by_carrier, iperf_list_summary

        # query of perf table failed
        except Error as e:
            logger.error(e)

    # query of points table failed
    except Error as e:
        logger.error(e)

    return True, {}, {}
