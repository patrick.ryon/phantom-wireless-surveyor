import logging

from subprocess import Popen, PIPE

logger = logging.getLogger("lib_shell")


def shell_exec(cmd):
    try:
        process = Popen(cmd.split(), stdout=PIPE, stderr=PIPE)
    except Exception as e:
        logger.warning(e)
        return None, None
    (output, err) = process.communicate()
    output = output.decode("utf-8")
    if err:
        err = err.decode("utf-8")
        logger.warning(err)
    exit_code = process.wait()
    logger.debug(exit_code)
    return exit_code, output, err
