import json
import logging
import lib.shell as shell

logger = logging.getLogger("lib_iperf")


def check_iperf():
    logger.debug('checking iperf3')
    try:
        exit_code, output, err = shell.shell_exec("iperf3 --version")
        if (not err and exit_code == 0):
            logger.debug('iperf3 check passed')
            return True
        logger.error(err)
    except Exception as e:
        logging.error(e)
    return False


def run_tcp_client_f(server_ip, server_port):
    if check_iperf():
        cmd = (f"iperf3 -P15 -p {server_port} -t3 -c {server_ip} -J "
               "--connect-timeout 5000")

        logger.debug(f"iperf command: {cmd}")

        exit_code, output, err = shell.shell_exec(cmd)
        logger.debug(exit_code)
        if not err:
            data = json.loads(output)
            logger.debug('iperf result: %s', data["end"]["sum_received"])
            logger.debug('iperf result: %s', data["end"]["sum_sent"])
            return True, {
                "protocol": "tcp",
                "seconds": data["end"]["sum_received"]["seconds"],
                "bytes": data["end"]["sum_received"]["bytes"],
                "bps": data["end"]["sum_received"]["bits_per_second"],
                "retries": data["end"]["sum_sent"]["retransmits"],
            }

    return False, {}


def run_tcp_client_r(server_ip, server_port):
    if check_iperf():
        cmd = (f"iperf3 -R -P15 -p {server_port} -t3 -c {server_ip} -J "
               "--connect-timeout 5000")

        logger.debug(f"iperf command: {cmd}")

        exit_code, output, err = shell.shell_exec(cmd)
        logger.debug(exit_code)
        if not err:
            data = json.loads(output)
            logger.debug('iperf result: %s', data["end"]["sum_received"])
            logger.debug('iperf result: %s', data["end"]["sum_sent"])
            return True, {
                "protocol": "tcp",
                "seconds": data["end"]["sum_received"]["seconds"],
                "bytes": data["end"]["sum_received"]["bytes"],
                "bps": data["end"]["sum_received"]["bits_per_second"],
                "retries": data["end"]["sum_sent"]["retransmits"],
            }

    return False, {}


def run_udp_client_f(server_ip, server_port):
    if check_iperf():
        cmd = (f"iperf3 -P15 -p {server_port} -t3 -c {server_ip} -u -J "
               "--connect-timeout 5000")

        logger.debug(f"iperf command: {cmd}")

        exit_code, output, err = shell.shell_exec(cmd)
        logger.debug(exit_code)
        if not err:
            data = json.loads(output)
            logger.debug('iperf result: %s', data["end"]["sum"])
            return True, {
                "direction": "forward",
                "protocol": "udp",
                "seconds": data["end"]["sum"]["seconds"],
                "bytes": data["end"]["sum"]["bytes"],
                "bps": data["end"]["sum"]["bits_per_second"],
                "jitter_ms": data["end"]["sum"]["jitter_ms"],
                "lost_packets": data["end"]["sum"]["lost_packets"],
                "packets": data["end"]["sum"]["packets"],
                "lost_percent": data["end"]["sum"]["lost_percent"],
            }

    return False, {}


def run_udp_client_r(server_ip, server_port):
    if check_iperf():
        cmd = (f"iperf3 -R -P15 -p {server_port} -t3 -c {server_ip} -u -J "
               "--connect-timeout 5000")

        logger.debug(f"iperf command: {cmd}")

        exit_code, output, err = shell.shell_exec(cmd)
        logger.debug(exit_code)
        if not err:
            data = json.loads(output)
            logger.debug('iperf result: %s', data["end"]["sum"])
            return True, {
                "direction": "reverse",
                "protocol": "udp",
                "seconds": data["end"]["sum"]["seconds"],
                "bytes": data["end"]["sum"]["bytes"],
                "bps": data["end"]["sum"]["bits_per_second"],
                "jitter_ms": data["end"]["sum"]["jitter_ms"],
                "lost_packets": data["end"]["sum"]["lost_packets"],
                "packets": data["end"]["sum"]["packets"],
                "lost_percent": data["end"]["sum"]["lost_percent"],
            }

    return False, {}
