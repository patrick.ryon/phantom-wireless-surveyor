import os
import sys
import time
import yaml
import lib.iperf as iperf

from flask import Flask, jsonify, request
from flask_cors import CORS

# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__),
        'config',
        'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print('WARNING - Invalid basedir, trying {} instead.'.format(
            os.path.join(os.path.dirname(__file__))))
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir,
            'config',
            'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir,
            'config',
            'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'remote'

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})


# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')


# Define a route for handling tcp iperf tests
@app.route('/tcp/<server_ip>/<server_port>', methods=['OPTIONS', 'GET'])
def test_tcp(server_ip, server_port):
    response_object = {'status': 'success'}
    # CORS pre-check
    if request.method == 'OPTIONS':
        return jsonify(response_object)

    # run tcp check
    if request.method == 'GET':
        tcp = {}
        forward = iperf.run_tcp_client_f(server_ip, server_port)
        if forward[0]:
            tcp["forward"] = forward[1]
            logger.info('pausing for 2sec before next test')
            time.sleep(2)
            reverse = iperf.run_tcp_client_r(server_ip, server_port)
            if reverse[0]:
                tcp["reverse"] = reverse[1]
                logger.info('pausing for 2sec before next test')
                time.sleep(2)
                return jsonify(tcp)

    return 'bad request!', 400


# Define a route for handling udp iperf tests
@app.route('/udp/<server_ip>/<server_port>', methods=['OPTIONS', 'GET'])
def test_udp(server_ip, server_port):
    response_object = {'status': 'success'}
    # CORS pre-check
    if request.method == 'OPTIONS':
        return jsonify(response_object)

    # run udp check
    if request.method == 'GET':
        udp = {}
        forward = iperf.run_udp_client_f(server_ip, server_port)
        if forward[0]:
            udp["forward"] = forward[1]
            logger.info('pausing for 2sec before next test')
            time.sleep(2)
            reverse = iperf.run_udp_client_r(server_ip, server_port)
            if reverse[0]:
                udp["reverse"] = reverse[1]
                logger.info('pausing for 2sec before next test')
                time.sleep(2)
                return jsonify(udp)

    return 'bad request!', 400


if __name__ == '__main__' or __name__ == 'app':
    # setup global logging
    global logger

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level']
    )

    app.run()
