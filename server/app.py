import os
import shutil
import sys
import time
import yaml
import lib.em7455 as em7455
import lib.json_data as json_data
import lib.mmcli as mmcli
import lib.nmcli as nmcli
import lib.sqlite_data as sqlite_data
import lib.tests as tests

from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS
from PIL import Image


# Get Config
try:
    conf_file = open(os.path.join(
        os.path.dirname(__file__), 'config', 'base.yaml'), 'r')
    base_conf = yaml.safe_load(conf_file)

    if os.path.isdir(base_conf['basedir']):
        conf_basedir = base_conf['basedir']
    elif os.path.isdir(os.path.join(os.path.dirname(__file__))):
        print(f'WARNING - Invalid basedir, trying '
              f'{os.path.join(os.path.dirname(__file__))} instead.')
        conf_basedir = os.path.join(os.path.dirname(__file__))
    else:
        print('ERROR - Unable to start, no valid basedir found.')
        sys.exit(1)

    if base_conf['stage'].lower() == "production":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'production.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    elif base_conf['stage'].lower() == "development":
        conf_file = open(os.path.join(
            conf_basedir, 'config', 'development.yaml'), 'r')
        stage_conf = yaml.safe_load(conf_file)
    else:
        print('ERROR - Unable to start, invalid application stage.')
        sys.exit(1)
except Exception as e:
    print(e)
    sys.exit(1)

sys.path.append(conf_basedir)

import lib.log as log  # noqa: E402

# Setup logging
logger_name = 'server'

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# Initialize a shared status variable
test_status = {}


# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')


# Define a route for creating a new project
@app.route('/project', methods=['POST'])
def create_project():
    project_name = request.form['title']

    # Check if the 'projectImage' field is in request.files
    if 'floorplan' not in request.files:
        return jsonify({'message': 'No file part'}), 400

    file = request.files['floorplan']

    # Check if a file was uploaded
    if file.filename == '':
        return jsonify({'message': 'No selected file'}), 400

    # Specify the directory where you want to save the uploaded images
    project_dir = os.path.join('projects', project_name)
    try:
        os.makedirs(project_dir, exist_ok=True)
    except Exception as e:
        print(e)
        return jsonify({'message': 'Project directory not created'}), 400

    # Save the uploaded image to the directory
    try:
        file.save(os.path.join(project_dir, file.filename))
        json_data.write_json(f"{project_dir}/project.json",
                             {"floor_plan": file.filename})
    except Exception as e:
        print(e)
        return jsonify({'message': 'Floorplan not uploaded'}), 400

    passed = True
    if passed:
        passed = sqlite_data.create_connection(f"{project_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_points_tbl(f"{project_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_perf_tbl(f"{project_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_lte_tbl(f"{project_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_wifi_tbl(f"{project_dir}/data.sqlite")
    if passed:
        return jsonify({
            'message': 'project created successfully',
            'project_name': project_name
        })

    return 'bad request!', 400


# Define a route for deleting a project
@app.route('/project/<project_name>', methods=['DELETE'])
def delete_project(project_name):
    response_object = {'status': 'success'}
    project_dir = './projects'

    dirpath = os.path.join(project_dir, project_name)
    if os.path.exists(dirpath) and os.path.isdir(dirpath):
        shutil.rmtree(dirpath)
        response_object['message'] = 'project removed'

        return jsonify(response_object)
    return 'bad request!', 400


# Define a route for handle a point requests
@app.route('/point/<project_name>/<point_id>',
           methods=['OPTIONS', 'DELETE', 'POST', 'PUT'])
def handle_point(project_name, point_id):
    response_object = {'status': 'success'}
    # CORS pre-check
    if request.method == 'OPTIONS':
        return jsonify(response_object)

    project_dir = './projects'
    project_dir = os.path.join(project_dir, project_name)
    if os.path.exists(project_dir) and os.path.isdir(project_dir):
        # delete point
        if request.method == 'DELETE':
            if (sqlite_data.delete_point(f"{project_dir}/data.sqlite",
                                         point_id)):
                response_object['message'] = (f'point {point_id} '
                                              f'removed from {project_name}')
                return jsonify(response_object)

        # create point
        if request.method == 'POST':
            if (sqlite_data.create_point(
                    f"{project_dir}/data.sqlite",
                    point_id,
                    request.form["x"],
                    request.form["y"])):
                response_object['message'] = (
                    f'point {point_id} '
                    f'created in {project_name}')
                return jsonify(response_object)

        # update point
        if request.method == 'PUT':
            if (sqlite_data.move_point(
                    f"{project_dir}/data.sqlite",
                    point_id,
                    request.form["x"],
                    request.form["y"])):
                response_object['message'] = (
                    f'point {point_id} moved to '
                    f'{request.form["x"]},'
                    f'{request.form["y"]}')

                return jsonify(response_object)

    return 'bad request!', 400


# Define a route for getting a list of projects on server
@app.route('/projects', methods=['GET'])
def list_projects():
    project_dir = './projects'
    project_list = []

    for name in os.listdir(project_dir):
        if os.path.isdir(os.path.join(project_dir, name)):
            project_list.append(name)
    return jsonify(project_list)


# Define a route for handling a point tests
@app.route('/test_point/<project_name>/<point_id>', methods=['GET'])
def handle_test(project_name, point_id):
    response_object = {'status': 'success'}
    test_status[(project_name, point_id)] = 'Testing in progress...'

    project_dir = './projects'
    project_dir = os.path.join(project_dir, project_name)
    if os.path.exists(project_dir) and os.path.isdir(project_dir):
        response_object['message'] = f'testing ${point_id}'

        connections = nmcli.get_connections()
        logger.debug(f'connections: {connections}')

        modems = mmcli.get_modems()
        logger.debug(f'modems: {modems}')

        logger.debug(f'interfaces: {stage_conf["interfaces"]}')
        for interface in stage_conf["interfaces"]:
            carrier = ''
            # save int name for iperf tests
            interface = stage_conf["interfaces"][interface]

            logger.debug(f'interface: {interface}')

            # wifi specific tests
            if interface["type"] == 'wifi':
                # get connected wifi info
                res, wifi_info_data = tests.test_wifi_info(interface["name"])
                if res:
                    logger.debug(wifi_info_data)
                    carrier = wifi_info_data["ssid"]

                if interface["tests"]["scan"]:
                    # get wifi scan
                    res, wifi_scan_data = tests.test_wifi_scan(
                        interface["name"])
                    if res:
                        logger.debug("wifi scan params:")
                        logger.debug(wifi_info_data["bssid"])
                        logger.debug(wifi_scan_data)
                        sqlite_data.write_wifi_scan(
                            f"{project_dir}/data.sqlite",
                            point_id,
                            wifi_scan_data,
                            wifi_info_data["bssid"])

            # gsm/lte specific tests
            if interface["type"] == "gsm" or interface["type"] == "lte":
                interface["type"] = "lte"
                carrier = interface["nm_profile"]
                retries = 0
                max_retries = 3  # Maximum number of connection retries

                # Try to connect nm gsm profile up to max_retries times
                while retries < max_retries:
                    if not connections[interface["nm_profile"]]["connected"]:
                        logger.debug('not connected, trying to connect...')
                        logger.debug(modems[str(interface["imei"])])
                        logger.debug(
                            nmcli.connect_modem(
                                interface["nm_profile"],
                                modems[str(interface["imei"])]["port"]
                            )
                        )
                        retries += 1

                        # Pause for connection to stablize
                        time.sleep(2)

                        # Refresh the connections list
                        connections = nmcli.get_connections()
                        logger.debug(f'connections: {connections}')
                    else:
                        # The connection was successful, break out of the loop
                        break

                logger.debug(connections[interface["nm_profile"]]["connected"])
                if connections[interface["nm_profile"]]["connected"]:
                    # Save int name for iperf tests
                    interface["name"] = modems[str(interface["imei"])]["int"]
                    logger.debug('run gsm/lte tests')
                    status, lte_data = em7455.get_status(
                        f'/dev/{modems[str(interface["imei"])]["tty"]}'
                    ) 
                    if status:
                        lte_data = em7455.parse_status(lte_data)
                        logger.debug(lte_data)
                        sqlite_data.write_lte_scan(
                            f"{project_dir}/data.sqlite",
                            point_id,
                            interface["nm_profile"],
                            lte_data)
                else:
                    logger.debug('fill with -1 type results')
                    sqlite_data.write_lte_scan(
                            f"{project_dir}/data.sqlite",
                            point_id,
                            interface["nm_profile"],
                            {
                                'rssi': -101,
                                'rsrp': -121,
                                'rsrq': -31,
                                'sinr': -1,
                            })

            logger.debug(interface["tests"]["local"])
            if interface["tests"]["local"]:
                # Test tcp on wifi to local iperf
                tcp_local = tests.test_tcp_local(interface["name"])
                logger.debug(tcp_local[0])
                if tcp_local[0]:
                    logger.debug(tcp_local[1])
                    result = sqlite_data.write_tcp_test(
                        f"{project_dir}/data.sqlite",
                        point_id,
                        interface["type"],
                        carrier,
                        "local",
                        tcp_local[1])
                    logger.debug(result)

                # test udp on wifi to local iperf
                udp_local = tests.test_udp_local(interface["name"])
                logger.debug(udp_local[0])
                if udp_local[0]:
                    logger.debug(udp_local[1])
                    result = sqlite_data.write_udp_test(
                        f"{project_dir}/data.sqlite",
                        point_id,
                        interface["type"],
                        carrier,
                        "local",
                        udp_local[1])
                    logger.debug(result)

            logger.debug(interface["tests"]["remote"])
            if interface["tests"]["remote"]:
                # test tcp on wifi to remote iperf
                tcp_remote = tests.test_tcp_remote(interface["name"])
                logger.debug(tcp_remote[0])
                if tcp_remote[0]:
                    logger.debug(tcp_remote[1])
                    result = sqlite_data.write_tcp_test(
                        f"{project_dir}/data.sqlite",
                        point_id,
                        interface["type"],
                        carrier,
                        "remote",
                        tcp_remote[1])
                    logger.debug(result)

                # test udp on wifi to remote iperf
                udp_remote = tests.test_udp_remote(interface["name"])
                logger.debug(udp_remote[0])
                if udp_remote[0]:
                    logger.debug(udp_remote[1])
                    result = sqlite_data.write_udp_test(
                        f"{project_dir}/data.sqlite",
                        point_id,
                        interface["type"],
                        carrier,
                        "remote",
                        udp_remote[1])
                    logger.debug(result)

            logger.debug(interface["tests"]["control"])
            if interface["tests"]["control"]:
                # test tcp on control station to control iperf api
                tcp_control = tests.test_tcp_control()
                logger.info(tcp_control)
                logger.debug(tcp_remote[0])
                if tcp_control[0]:
                    logger.debug(tcp_control[1])
                    result = sqlite_data.write_tcp_test(
                        f"{project_dir}/data.sqlite",
                        point_id,
                        "stationary",
                        carrier,
                        "remote",
                        tcp_control[1])
                    logger.debug(result)

                # test udp to control iperf api
                udp_control = tests.test_udp_control()
                logger.info(udp_control)
                logger.debug(udp_remote[0])
                if udp_control[0]:
                    logger.debug(udp_control[1])
                    result = sqlite_data.write_udp_test(
                        f"{project_dir}/data.sqlite",
                        point_id,
                        "stationary",
                        carrier,
                        "remote",
                        udp_control[1])
                    logger.debug(result)

        # Update tested at timestamp in sqlite
        sqlite_data.point_tested(f"{project_dir}/data.sqlite", point_id)
        # Update webhook status
        test_status[(project_name, point_id)] = 'Test completed successfully'

        return jsonify(response_object)

    test_status[(project_name, point_id)] = 'Test failed'
    return 'bad request!', 400


# Define a route for listing all points in a project
@app.route('/points/<project_name>', methods=['OPTIONS', 'GET'])
def get_points(project_name):
    response_object = {'status': 'success'}
    if request.method == 'OPTIONS':
        return jsonify(response_object)

    if request.method == 'GET':

        project_dir = './projects'
        project_dir = os.path.join(project_dir, project_name)
        if os.path.exists(project_dir) and os.path.isdir(project_dir):
            point_list = sqlite_data.get_points(f"{project_dir}/data.sqlite")
            if point_list[0]:
                return jsonify(point_list[1])

    return 'bad request!', 400


# Define a route for getting floorplan image's size,
@app.route('/floorplan/size/<project_name>')
def get_floorplan_size(project_name):
    response_object = {'status': 'success'}
    if request.method == 'OPTIONS':
        return jsonify(response_object)

    if request.method == 'GET':
        data = json_data.read_json(f"./projects/{project_name}/project.json")

        dir_path = os.path.dirname(os.path.realpath(__name__))
        file_path = os.path.join(dir_path, "projects",
                                 project_name, data["floor_plan"])

        width, height = Image.open(file_path).size

        return jsonify({
            'x': width,
            'y': height
        })

    return 'bad request!', 400


# Define a route for serving a project's floorplan image
@app.route('/floorplan/<project_name>')
def send_floorplan(project_name):
    data = json_data.read_json(f"./projects/{project_name}/project.json")

    dir_path = os.path.dirname(os.path.realpath(__name__))
    dir_path = os.path.join(dir_path, "projects", project_name)

    print(data)

    return send_from_directory(dir_path, data["floor_plan"])


# Define a route to serve as the webhook for test status updates
@app.route('/webhook/test_status/<project_name>/<point_id>',
           methods=['OPTION', 'GET'])
def test_status_webhook(project_name, point_id):
    response_object = {'status': 'success'}
    if request.method == 'OPTIONS':
        return jsonify(response_object)

    if request.method == 'GET':
        # Retrieve the status from the shared variable
        status = test_status.get((project_name, point_id), 'Test not started')
        response_object = {'status': status}

        return jsonify(response_object)

    return 'bad request!', 400


if __name__ == '__main__' or __name__ == 'app':
    # setup global logging
    global logger

    logger = log.setup_custom_logger(
        logger_name,
        (conf_basedir + stage_conf['logging']['path']),
        stage_conf['logging']['console_level'],
        stage_conf['logging']['file_level']
    )

    app.run()
