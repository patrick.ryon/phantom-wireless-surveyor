mmcli -m 1
      Bearer   |                paths: /org/freedesktop/ModemManager1/Bearer/1
mmcli -m 1 --enable
sudo ip link set wwan1 up
mmcli --bearer 0
  IPv4 configuration |         method: static
                     |        address: 10.199.14.50
                     |         prefix: 30
                     |        gateway: 10.199.14.49
                     |            dns: 172.26.38.1
                     |            mtu: 1430


sudo ip addr add 10.199.14.50/32 dev wwan1
sudo ip link set dev wwan1 arp off
sudo ip link set dev wwan1 mtu 1430
sudo ip route add default dev wwan1 metric 201

sudo bash -c 'echo "nameserver 10.177.0.34" >> /etc/resolv.conf'

sudo ip route add 192.168.2.0/24 via 192.168.2.254 dev eth0
sudo ip route add default via 192.168.1.254 dev wwan0 metric 700

sudo nmcli connection up "Verizon" ifname cdc-wdm0