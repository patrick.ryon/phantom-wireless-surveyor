'''Application entry point'''
import logging
import os
import platform

from flask import Flask
from flask_cors import CORS

logger = logging.getLogger("app__init__")

base_conf = {}
stage_conf = {}


def _initialize_blueprints(app) -> None:
    '''Register Flask blueprints'''
    from app.routes.floorplan import floorplan
    app.register_blueprint(floorplan)

    from app.routes.ping import ping
    app.register_blueprint(ping)

    from app.routes.point import point
    app.register_blueprint(point)

    from app.routes.points import points
    app.register_blueprint(points)

    from app.routes.project import project
    app.register_blueprint(project)

    from app.routes.projects import projects
    app.register_blueprint(projects)

    from app.routes.report import report
    app.register_blueprint(report)

    from app.routes.test import test
    app.register_blueprint(test)

    from app.routes.webhook import webhook
    app.register_blueprint(webhook)


def create_app(base, stage) -> Flask:
    '''Create an app by initializing components'''
    app = Flask(__name__)
    app.config.from_object(__name__)

    # enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    global base_conf
    base_conf = base
    global stage_conf
    stage_conf = stage

    if ('Linux' in platform.system()) and ('WSL' not in platform.release()):
        logger.info('Linux OS detected, attempting to disable USB auto suspend.')
        try:
            auto_sus = os.open('/sys/module/usbcore/parameters/autosuspend', os.O_RDWR)
            os.write(auto_sus, "-1".encode())
            os.close(auto_sus)
            logger.info('USB auto suspend successfully disabled.')
        except Exception as e:
            logger.warning(f'Disabling USB auto suspend failed.  Error is: {e}')
    else:
        logger.warning('Non-Linux OPS detected.  Survey features unavailable.')

    _initialize_blueprints(app)

    # Run App
    return app
