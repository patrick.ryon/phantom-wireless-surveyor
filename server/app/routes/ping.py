'''Ping related routes'''
from flask import Blueprint, jsonify

ping = Blueprint('ping', __name__)


# sanity check route
@ping.route('/ping', methods=['GET'])
def ping_pong():
    '''Repsonse to ping'''
    status_code = 200
    return jsonify('pong!'), status_code
