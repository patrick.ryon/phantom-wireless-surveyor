'''Points related routes'''
import os

from flask import Blueprint, jsonify, request
from app import models
from app import base_conf

points = Blueprint('points', __name__)


# Define a route for listing all points in a project
@points.route('/points/<project_name>', methods=['OPTIONS', 'GET'])
def get_points(project_name):
    '''List projects'''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    project_dir = f'{base_conf["basedir"]}/projects'
    project_dir = os.path.join(project_dir, project_name)
    if request.method == 'GET':
        repsonse, status = models.points.list_points(project_dir)
        return jsonify(repsonse), status

    return ({'message': 'Bad request'}, 400)
