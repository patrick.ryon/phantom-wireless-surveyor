'''Webhook related routes'''
from flask import Blueprint, jsonify, request
from app import models

webhook = Blueprint('webhook', __name__)


# Define a route to serve as the webhook for test status updates
@webhook.route('/webhook/test/status/<project_name>/<point_id>',
           methods=['OPTION', 'GET'])
def test_status_webhook(project_name, point_id):
    '''Webhook for test_status'''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    if request.method == 'GET':
        # Retrieve the status from the shared variable
        repsonse, status = models.webhook.test_status_webhook(
            project_name, point_id)

    return jsonify(repsonse), status


# Define a route to serve as the webhook for report status updates
@webhook.route('/webhook/report/status/<project_name>',
           methods=['OPTION', 'GET'])
def report_status_webhook(project_name):
    '''Webhook for report_status'''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    if request.method == 'GET':
        # Retrieve the status from the shared variable
        repsonse, status = models.webhook.report_status_webhook(
            project_name)

    return jsonify(repsonse), status
