'''Floorplan related routes'''
from flask import abort, Blueprint, jsonify, request, send_from_directory
from app import models

floorplan = Blueprint('floorplan', __name__)


# Define a route for getting floorplan image's size,
@floorplan.route('/floorplan/size/<project_name>', methods=['OPTION', 'GET'])
def floorplan_size(project_name):
    '''Get floorplan size'''
    # CORS pre-check
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    repsonse, status = models.floorplan.floorplan_size(project_name)
    return jsonify(repsonse), status


# Define a route for serving a project's floorplan image
@floorplan.route('/floorplan/<project_name>')
def floorplan_image(project_name):
    '''Get floorplan image'''
    err, path, file = models.floorplan.floorplan_image(project_name)

    if err:
        abort(404)
    return send_from_directory(path, file)
