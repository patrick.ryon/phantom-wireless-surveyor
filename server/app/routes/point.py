'''Point related routes'''
import os

from flask import Blueprint, jsonify, request
from app import models
from app import base_conf

point = Blueprint('point', __name__)


# Define a route for creating a new project
@point.route('/project', methods=['POST'])
def create_project():
    '''Create project'''
    repsonse, status = models.project.create_project(request)
    return jsonify(repsonse), status


# Define a route for handling a point requests
@point.route('/point/<proj_name>/<point_id>',
             methods=['OPTIONS', 'DELETE', 'POST', 'PUT'])
def handle_point(proj_name, point_id):
    ''' CORS pre-check '''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)
    if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
        ''' delete point '''
        if request.method == 'DELETE':
            repsonse, status = models.point.delete_point(
                proj_dir, proj_name, point_id)
            return jsonify(repsonse), status

        ''' create point '''
        if request.method == 'POST':
            repsonse, status = models.point.create_point(
                proj_dir, proj_name, point_id, request)
            return jsonify(repsonse), status

        ''' mod point '''
        if request.method == 'PUT':
            repsonse, status = models.point.mod_point(
                proj_dir, point_id, request)
            return jsonify(repsonse), status

    return ({'message': 'Bad request'}, 400)
