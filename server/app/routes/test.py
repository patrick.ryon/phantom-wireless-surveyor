'''Test related routes'''
import trio

from flask import Blueprint, jsonify, request
from app import models

test = Blueprint('test', __name__)


# Define a route for handling a point tests
@test.route('/test/point/<project_name>/<point_id>',
            methods=['OPTIONS', 'GET'])
def test_point(project_name, point_id):
    '''Run tests at a point'''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200
    
    if request.method == 'GET':
        repsonse, status = trio.run(models.test.run_tests_async,
        project_name, point_id)
    return jsonify(repsonse), status
