'''Project related routes'''
import os

from flask import Blueprint, jsonify, request
from app import models
from app import base_conf

project = Blueprint('project', __name__)


# Define a route for creating a new project
@project.route('/project', methods=['POST'])
def create_project():
    '''Create project'''
    repsonse, status = models.project.create_project(request)
    return jsonify(repsonse), status
    

# Define a route for deleting a project
@project.route('/project/<proj_name>', methods=['DELETE'])
def delete_project(proj_name):
    '''Delete project'''
    repsonse, status = models.project.delete_project(proj_name)
    return jsonify(repsonse), status


# Define a route for project's properties
@project.route('/project/properties/<proj_name>', methods=['OPTIONS', 'GET', 'PUT'])
def update_properties(proj_name):
    ''' CORS Pre-check '''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)
    if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
        '''Get project properties'''
        if request.method == 'GET':
            repsonse, status = models.project.get_properties(proj_name)
            return jsonify(repsonse), status

        ''' Update project properties '''
        if request.method == 'PUT':
            repsonse, status = models.project.update_properties(proj_name, request.get_json(force=True))
            return jsonify(repsonse), status

    return ({'message': 'Bad request'}, 400)
