'''Projects related routes'''
from app import models
from flask import Blueprint, jsonify

projects = Blueprint('projects', __name__)


# Define a route for getting a list of projects on server
@projects.route('/projects', methods=['GET'])
def list_projects():
    '''List projects'''
    repsonse, status = models.projects.list_projects()
    return jsonify(repsonse), status
