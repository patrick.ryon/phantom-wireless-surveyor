'''Report related routes'''
import os

from flask import Blueprint, jsonify, request
from app import models
from app import base_conf

report = Blueprint('report', __name__)


# Define a route for creating a report
@report.route('/report/<proj_name>', methods=['OPTIONS', 'POST'])
def create_report(proj_name):
    ''' CORS Pre-check '''
    if request.method == 'OPTIONS':
        return jsonify({'status': 'success'}), 200

    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)
    if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
        ''' Create Report '''
        if request.method == 'POST':
            repsonse, status = models.report.create_report(proj_name)
            return jsonify(repsonse), status

    return ({'message': 'Bad request'}, 400)
