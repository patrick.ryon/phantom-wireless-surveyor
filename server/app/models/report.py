'''Report related operations'''
import logging
import json
import matplotlib.image as mpimg
import os
import shutil

import app.models.webhook
import lib.report_doc as report_doc
import lib.report_lte as report_lte
import lib.report_lte_perf as report_lte_perf
import lib.report_wifi as report_wifi
import lib.report_wifi_perf as report_wifi_perf
import lib.sqlite_data as sqlite_data
import lib.yaml_data as yaml_data

from app import base_conf

logger = logging.getLogger("models_report")


def create_report(proj_name, labels=False):
    '''Create report'''

    app.models.webhook.report_status[(proj_name)] = ({
        'status': 'Report generation starting...',
        'report_dir': '',
    })

    # Specify the directory where you want to save the uploaded images
    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)
    report_dir = os.path.join(proj_dir, "report")
    try:
        projs_dir = f'{base_conf["basedir"]}/projects'
        proj_dir = os.path.join(projs_dir, proj_name)
        
        if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
            err, proj_data = yaml_data.read_yaml('project.yaml', proj_dir)
            if err:
                logger.error(f'error reading project data: {proj_dir}/project.yaml')
                app.models.webhook.report_status[(proj_name)] = ({
                    'status': 'Report generation failed',
                    'report_dir': '',
                })
                return ({'message': 'Error reading project data'}, 400)
    except Exception as e:
        logger.error(e)
        app.models.webhook.report_status[(proj_name)] = ({
            'status': 'Report generation failed',
            'report_dir': '',
        })
        return ({'message': 'Bad request'}, 400)

    # Check report directory
    report_dir = os.path.join(proj_dir, "report")
    if os.path.exists(report_dir) and os.path.isdir(report_dir):
        logger.debug(f'report directory {report_dir} exists')
    else:
        try:
            os.makedirs(report_dir, exist_ok=True)
            logger.debug(f'report directory {report_dir} created')
        except Exception as e:
            logger.error(e)
            app.models.webhook.report_status[(proj_name)] = ({
                'status': 'Report generation failed',
                'report_dir': '',
            })
            return ({'message': 'Project report directory not created'}, 400)
    app.models.webhook.report_status[(proj_name)] = ({
        'status': 'Report generation in progress...',
        'report_dir': report_dir,
    })
    
    # Load the database file
    data_file = os.path.join(proj_dir, "data.sqlite")
    logger.debug(f"database file: {data_file}")

    floor_file = os.path.join(proj_dir, proj_data["floor_plan"])
    logger.debug(f"floorplan file: {floor_file}")

    # Load the floorplan image
    floor_img = mpimg.imread(floor_file)

    # Get lte.gsm carriers
    carriers = sqlite_data.get_lte_carriers(data_file, 'lte')

    # Generate images
    report_lte.rsrp_data(proj_name, report_dir, data_file, carriers, floor_img,
                         labels)
    report_lte.rsrq_data(proj_name, report_dir, data_file, carriers, floor_img, 
                         labels)
    report_lte.sinr_data(proj_name, report_dir, data_file, carriers, floor_img,
                         labels)

    report_lte_perf.lte_tcp_iperf(proj_name, report_dir, data_file, carriers,
                                  floor_img, labels)
    report_lte_perf.lte_udp_iperf(proj_name, report_dir, data_file, carriers,
                                  floor_img, labels)

    report_wifi.signal_data(proj_name, report_dir, data_file, floor_img,
                            proj_data['wifi']['ssid'], labels)
    report_wifi.datarate_data(proj_name, report_dir, data_file, floor_img,
                              proj_data['wifi']['ssid'], labels)
    report_wifi.channel_data(proj_name, report_dir, data_file,
                              proj_data['wifi']['ssid'], labels)

    report_wifi_perf.wifi_tcp_iperf(proj_name, report_dir, data_file,
                                    floor_img, labels)
    report_wifi_perf.wifi_udp_iperf(proj_name, report_dir, data_file,
                                    floor_img, labels)

    # Generate document from template
    report_doc.generate_report(proj_name, proj_dir, report_dir, floor_file)

    app.models.webhook.report_status[(proj_name)] = ({
        'status': 'Report generated successfully',
        'report_dir': report_dir,
    })
    return ({
        'message': 'Project report created successfully',
        'project_name': proj_name
    }, 200)
