'''Projects related operations'''
import logging
import lib.sqlite_data as sqlite_data

logger = logging.getLogger("models_points")


def list_points(project_dir):
    err, point_list = sqlite_data.get_points(f"{project_dir}/data.sqlite")
    if not err:
        return ({'message': 'Points retrieved',
                 'list': point_list}, 200)
    return ({'message': 'Bad request'}, 400)
