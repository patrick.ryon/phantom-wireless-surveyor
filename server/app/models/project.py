'''Project related operations'''
import logging
import json
import os
import shutil

import lib.yaml_data as yaml_data
import lib.sqlite_data as sqlite_data

from app import base_conf

logger = logging.getLogger("models_project")


def create_project(request):
    '''Create project'''
    proj_name = request.form['title']

    # Check if the 'projectImage' field is in request.files
    if 'floorplan' not in request.files:
        return ({'message': 'No file part'}, 400)

    fp_file = request.files['floorplan']

    # Check if a file was uploaded
    if fp_file.filename == '':
        return ({'message': 'No selected file'}, 400)

    # Specify the directory where you want to save the uploaded images
    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)
    report_dir = os.path.join(proj_dir, "report")

    # Create project directory
    try:
        os.makedirs(proj_dir, exist_ok=True)
    except Exception as e:
        logger.error(e)
        return ({'message': 'Project directory not created'}, 400)

    # create report directory
    try:
        os.makedirs(report_dir, exist_ok=True)
    except Exception as e:
        logger.error(e)
        return ({'message': 'Project report directory not created'}, 400)

    # Save the uploaded image to the directory
    try:
        fp_file.save(os.path.join(proj_dir, fp_file.filename))
    except Exception as e:
        logger.error(e)
        return ({'message': 'Floorplan not uploaded'}, 400)

    # Save yaml project data file
    err = yaml_data.write_yaml(
        {
            'floor_plan': fp_file.filename,
            'survey_date': '',
            'customer_name': '',
            'site': {
                'name': '',
                'address1': '',
                'address2': '',
                'city': '',
                'state': '',
                'zip': '',
                'country': '',
            },
            'contact1': {
                'name': '',
                'title': '',
                'email': '',
                'phone': '',
            },
            'contact2': {
                'name': '',
                'title': '',
                'email': '',
                'phone': '',
            },
            'surveyor1': {
                'name': '',
                'title': '',
                'email': '',
                'phone': '',
            },
            'surveyor2': {
                'name': '',
                'title': '',
                'email': '',
                'phone': '',
            },
            'surveyor3': {
                'name': '',
                'title': '',
                'email': '',
                'phone': '',
            },
            'wifi': {
                'ssid': '',
                'auth': '',
                'mac': '',
            },
            'lte': {
                'carrier1': '',
                'carrier2': '',
                'carrier3': '',
            }
        }, 'project.yaml', proj_dir)
    if err:
        logger.error('project yaml file not created')
        return ({'message': 'Project yaml file not created'}, 400)

    passed = True
    if passed:
        passed = sqlite_data.create_connection(f"{proj_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_points_tbl(f"{proj_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_perf_tbl(f"{proj_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_lte_tbl(f"{proj_dir}/data.sqlite")
    if passed:
        passed = sqlite_data.create_wifi_tbl(f"{proj_dir}/data.sqlite")
    if passed:
        return ({
            'message': 'Project created successfully',
            'project_name': proj_name
        }, 200)

    return ({'message': 'Bad request'}, 400)


def delete_project(proj_name):
    '''Delete project'''
    try:
        projs_dir = f'{base_conf["basedir"]}/projects'
        proj_dir = os.path.join(projs_dir, proj_name)
        if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
            shutil.rmtree(proj_dir)
            return ({'message': 'Project removed'}, 200)
    except Exception as e:
        logger.error(e)
    return ({'message': 'Bad request'}, 400)


def get_properties(proj_name):
    '''Get Properties'''
    try:
        projs_dir = f'{base_conf["basedir"]}/projects'
        proj_dir = os.path.join(projs_dir, proj_name)
        if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
            err, proj_data = yaml_data.read_yaml('project.yaml', proj_dir)
            if not err:
                return (proj_data, 200)
            else:
                logger.error(f'error reading project data: {proj_dir}/project.yaml')
    except Exception as e:
        logger.error(e)
    return ({'message': 'Bad request'}, 400)


def update_properties(proj_name, new_data):
    '''Update Properties'''
    logger.debug(new_data)
    logger.debug(type(new_data))
    logger.debug(new_data['customer_name'])

    try:
        projs_dir = f'{base_conf["basedir"]}/projects'
        proj_dir = os.path.join(projs_dir, proj_name)
        if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
            err, proj_data = yaml_data.read_yaml('project.yaml', proj_dir)
            if not err:
                # Save yaml project data file
                err = yaml_data.write_yaml(
                    {
                        'floor_plan': proj_data['floor_plan'],
                        'survey_date': new_data['survey_date'],
                        'customer_name': new_data['customer_name'],
                        'site': {
                            'name': new_data['site']['name'],
                            'address1': new_data['site']['address1'],
                            'address2': new_data['site']['address2'],
                            'city': new_data['site']['city'],
                            'state': new_data['site']['state'],
                            'zip': new_data['site']['zip'],
                            'country': new_data['site']['country'],
                        },
                        'contact1': {
                            'name': new_data['contact1']['name'],
                            'title': new_data['contact1']['title'],
                            'email': new_data['contact1']['email'],
                            'phone': new_data['contact1']['phone'],
                        },
                        'contact2': {
                            'name': new_data['contact2']['name'],
                            'title': new_data['contact2']['title'],
                            'email': new_data['contact2']['email'],
                            'phone': new_data['contact2']['phone'],
                        },
                        'surveyor1': {
                            'name': new_data['surveyor1']['name'],
                            'title': new_data['surveyor1']['title'],
                            'email': new_data['surveyor1']['email'],
                            'phone': new_data['surveyor1']['phone'],
                        },
                        'surveyor2': {
                            'name': new_data['surveyor2']['name'],
                            'title': new_data['surveyor2']['title'],
                            'email': new_data['surveyor2']['email'],
                            'phone': new_data['surveyor2']['phone'],
                        },
                        'surveyor3': {
                            'name': new_data['surveyor3']['name'],
                            'title': new_data['surveyor3']['title'],
                            'email': new_data['surveyor3']['email'],
                            'phone': new_data['surveyor3']['phone'],
                        },
                        'wifi': {
                            'ssid': new_data['wifi']['ssid'],
                            'auth': new_data['wifi']['auth'],
                            'mac': new_data['wifi']['mac'],
                        },
                        'lte': {
                            'carrier1': new_data['lte']['carrier1'],
                            'carrier2': new_data['lte']['carrier2'],
                            'carrier3': new_data['lte']['carrier3'],
                        }
                    }, 'project.yaml', proj_dir)
                return ({'message': 'Properties updated'}, 200)
                if err:
                    logger.error('project yaml file not updated')
            else:
                logger.error(f'error reading project data: {proj_dir}/project.yaml')
    except Exception as e:
        logger.error(e)
    return ({'message': 'Bad request'}, 400)
