import logging
import lib.sqlite_data as sqlite_data

logger = logging.getLogger("models_point")


def delete_point(proj_dir, proj_name, point_id):
    if (sqlite_data.delete_point(
          f"{proj_dir}/data.sqlite",
          point_id)):
        return ({'message': f'point {point_id} removed from {proj_name}'},
                200)
    return ({'message': 'Bad request'}, 400)


def create_point(proj_dir, proj_name, point_id, request):
    if (sqlite_data.create_point(
            f"{proj_dir}/data.sqlite",
            point_id,
            request.form["x"],
            request.form["y"])):
        return ({'message': f'point {point_id} created in {proj_name}'},
                200)
    return ({'message': 'Bad request'}, 400)


def mod_point(proj_dir, point_id, request):
    if request.form["action"] == "move":
        if (sqlite_data.move_point(
              f"{proj_dir}/data.sqlite",
              point_id,
              request.form["x"],
              request.form["y"])):
            return ({'message': (f'point {point_id} moved to '
                     f'{request.form["x"]}, {request.form["y"]}')},
                    200)
    if request.form["action"] == "type" and request.form["type"] == "ap":
        if (sqlite_data.point_is_ap(
              f"{proj_dir}/data.sqlite",
              point_id)):
            return ({'message': (f'point {point_id} changed to '
                     f'{request.form["type"]}')},
                    200)
    return ({'message': 'Bad request'}, 400)
