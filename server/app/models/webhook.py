'''Webhook related operations'''
import logging

logger = logging.getLogger("models_webhook")

# Initialize shared status variables
test_status = {}
report_status = {}


def test_status_webhook(project_name, point_id):
    status = test_status.get((project_name, point_id), 'Test not started')
    return {'status': status}, 200


def report_status_webhook(project_name):
    status = report_status.get((project_name), {
        'status': 'Test not started',
        'report_dir': '',
    })
    return status, 200