'''Projects related operations'''
import logging
import os

from app import base_conf

logger = logging.getLogger("models_projects")


def list_projects():
    '''List projects'''
    try:
        project_dir = f'{base_conf["basedir"]}/projects'
        project_list = []

        for name in sorted(os.listdir(project_dir)):
            if os.path.isdir(os.path.join(project_dir, name)):
                project_list.append(name)
        return ({
            'message': 'Projects retrieved',
            'list': project_list}, 200)
    except Exception as e:
        logger.error(e)
    return ({'message': 'Bad request'}, 400)
