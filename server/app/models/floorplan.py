'''Project related operations'''
import logging
import os
import lib.yaml_data as yaml_data

from PIL import Image

from app import base_conf

logger = logging.getLogger("models_floorplan")


def floorplan_size(proj_name):
    ''' Get floorplan size '''
    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)
    err, proj_data = yaml_data.read_yaml('project.yaml', proj_dir)

    if not err:
        try:
            fp_file = os.path.join(proj_dir, proj_data["floor_plan"])

            width, height = Image.open(fp_file).size

            return ({
                'x': width,
                'y': height
            }, 200)
        except Exception as e:
            logger.error(f'error opening image: {fp_file}')
            logger.error(f'error: {e}')
    else:
        logger.error(f'error reading project data: {proj_dir}/project.yaml')

    return {'message': 'Bad request'}, 400


def floorplan_image(proj_name):
    ''' Get floorplan image '''
    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)

    logger.debug(f'proj_name : {proj_name}')
    err, proj_data = yaml_data.read_yaml('project.yaml', proj_dir)

    if not err:
        return False, proj_dir, proj_data["floor_plan"]
    else:
        logger.error(f'error reading project data: {proj_dir}/project.yaml')

    return True, "", ""
