'''Shared code'''
import logging
import app.models.floorplan
import app.models.point
import app.models.points
import app.models.project
import app.models.projects
import app.models.report
import app.models.test
import app.models.webhook

logger = logging.getLogger("models__init__")
