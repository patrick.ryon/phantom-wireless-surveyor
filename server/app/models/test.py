'''Test related operations'''
import logging
import os
import trio
import app.models.webhook
import lib.em7455 as em7455
import lib.mmcli as mmcli
import lib.nmcli as nmcli
import lib.sqlite_data as sqlite_data
import lib.tests as tests

from app import base_conf
from app import stage_conf

logger = logging.getLogger("models_test")

connected_bssids = []

async def run_tests_async(proj_name, point_id):
    logger.debug(stage_conf)

    app.models.webhook.test_status[(proj_name, point_id)] = (
        'Testing in progress...')

    projs_dir = f'{base_conf["basedir"]}/projects'
    proj_dir = os.path.join(projs_dir, proj_name)

    if os.path.exists(proj_dir) and os.path.isdir(proj_dir):
        logger.debug(f'testing {point_id}')

        # Create a dictionary of locks, one per interface
        interface_locks = {}

        connections = await nmcli.get_connections()
        logger.debug(f'connections: {connections}')

        modems = mmcli.get_modems()
        logger.debug(f'modems: {modems}')

        logger.debug(f'interfaces: {stage_conf["interfaces"]}')

        async with trio.open_nursery() as nursery:
            for i in stage_conf["interfaces"]:
                interface = stage_conf["interfaces"][i]
                logger.debug(interface)
                if "name" not in interface and len(modems) > 0:
                    interface["name"] = modems[str(interface["imei"])]["int"]
                    
                carrier = ''

                # Run wifi scans
                if interface["type"] == 'wifi':
                    carrier = 'wifi'
                    nursery.start_soon(
                        run_wifi_scan,
                        interface,
                        proj_dir,
                        point_id)

                # Run lte/gsm scans
                if interface["type"] in {"gsm", "lte"}:
                    carrier = interface["nm_profile"]
                    retries = 0
                    max_retries = 3  # Maximum number of connection retries

                    while retries < max_retries:
                        logger.debug(f'connections[]: {connections}')
                        logger.debug(f'interface["nm_profile"]: {interface["nm_profile"]}')
                        if not (
                            connections[interface["nm_profile"]]["connected"]
                        ):
                            logger.debug('not connected, trying to connect...')
                            if str(interface["imei"]) in modems:
                                logger.debug(modems[str(interface["imei"])])
                                logger.debug(
                                    await nmcli.connect_modem(
                                        interface["nm_profile"],
                                        modems[str(interface["imei"])]["port"]
                                    )
                                )
                            retries += 1

                            # Pause for connection to stabilize
                            await trio.sleep(2)

                            # Refresh the connections list
                            connections = await nmcli.get_connections()
                            logger.debug(f'connections: {connections}')
                        else:
                            # The connection successful, break loop
                            break
                    if retries == max_retries:
                        logger.warning(f'unable to connect {carrier} after {retries} tries')
                    else:
                        logger.info(f'connected {carrier} after {retries} tries')


                    logger.debug(
                        connections[interface["nm_profile"]]["connected"])
                    if connections[interface["nm_profile"]]["connected"]:
                        nursery.start_soon(
                            run_lte_scan,
                            interface,
                            proj_dir,
                            point_id,
                            modems)
                    else:
                        logger.debug('fill with -1 type results')
                        await sqlite_data.write_lte_scan_async(
                            f"{proj_dir}/data.sqlite",
                            point_id,
                            interface["nm_profile"],
                            {
                                'rssi': -101,
                                'rsrp': -121,
                                'rsrq': -31,
                                'sinr': -1,
                            })
                        logger.debug('returned')

                logger.debug(f'interface {interface}')
                if "name" in interface:
                    # Ensure there's a lock for this interface
                    if interface["name"] not in interface_locks:
                        logger.debug(f'added lock to {interface["name"]}')
                        interface_locks[interface["name"]] = trio.Lock()

                    # Run local tests
                    logger.debug(interface["tests"]["local"]["enabled"])
                    if interface["tests"]["local"]["enabled"]:
                        logger.debug(f'running local test on {interface["name"]}')
                        async with interface_locks[interface["name"]]:
                            nursery.start_soon(
                                run_local_iperf,
                                interface,
                                proj_dir,
                                point_id,
                                carrier
                            )

                    # Run remote tests
                    logger.debug(interface["tests"]["remote"]["enabled"])
                    if interface["tests"]["remote"]["enabled"]:
                        logger.debug(f'running remote test on {interface["name"]}')
                        nursery.start_soon(
                            run_remote_iperf,
                            interface,
                            proj_dir,
                            point_id,
                            carrier
                        )

                    # Run control tests
                    logger.debug(interface["tests"]["control"]["enabled"])
                    if interface["tests"]["control"]["enabled"]:
                        logger.debug(f'running control test on {interface["name"]}')
                        async with interface_locks[interface["name"]]:
                            nursery.start_soon(
                                run_control_iperf,
                                interface,
                                proj_dir,
                                point_id,
                                carrier
                            )


        # Update tested at timestamp in sqlite
        logger.debug('exiting loop, writing tested')
        sqlite_data.point_tested(f"{proj_dir}/data.sqlite", point_id)
        # Add connected data
        for bssid in connected_bssids:
            sqlite_data.update_wifi_connected(
                f"{proj_dir}/data.sqlite", point_id, bssid)
        # Update webhook status
        app.models.webhook.test_status[(proj_name, point_id)] = (
            'Test completed successfully')
        
        return ({'status': 'success'}, 200)

    app.models.webhook.test_status[(proj_name, point_id)] = (
        'Test failed')
    return ({'message': 'Bad request'}, 400)


async def run_wifi_scan(interface, proj_dir, point_id):
    res, wifi_connected_data = await tests.test_wifi_connected(interface["name"])
    if res:
        logger.debug("wifi connected data:")
        logger.debug(wifi_connected_data)
        connected_bssids.append(wifi_connected_data)

    logger.debug(interface["tests"]["scan"]["enabled"])
    if interface["tests"]["scan"]["enabled"]:
        scanned, wifi_scan_data = await tests.test_wifi_scan(interface["name"])
        if scanned:
            logger.debug("wifi scan data:")
            logger.debug(wifi_scan_data)
            await sqlite_data.write_wifi_scan_async(
                f"{proj_dir}/data.sqlite",
                point_id,
                wifi_scan_data)


async def run_lte_scan(interface, proj_dir, point_id, modems):
    logger.debug(interface["tests"]["scan"]["enabled"])
    if interface["tests"]["scan"]["enabled"]:
        retries = 0
        max_retries = 5  # Maximum number of connection retries

        while retries < max_retries:
            status, lte_data = em7455.get_status(
                f'/dev/{modems[str(interface["imei"])]["tty"]}'
            )
            if status:
                break
            else:
                retries += 1

                # Pause for modem serial availability
                await trio.sleep(3)
        if retries == max_retries:
            logger.warning(f'unable to connect /dev/{modems[str(interface["imei"])]["tty"]} after {retries} tries')
        else:
            logger.info(f'connected /dev/{modems[str(interface["imei"])]["tty"]} after {retries} tries')
        
        lte_status, lte_data = em7455.parse_status(lte_data)
        logger.debug(lte_status)
        logger.debug(lte_data)
        await sqlite_data.write_lte_scan_async(
            f"{proj_dir}/data.sqlite",
            point_id,
            interface["nm_profile"],
            lte_data)


async def run_local_iperf(interface, proj_dir, point_id, carrier):
    # Test tcp on wifi to local iperf
    tcp_local = await tests.test_tcp_local(interface)
    logger.debug(tcp_local)
    result = sqlite_data.write_tcp_test(
        f"{proj_dir}/data.sqlite",
        point_id,
        interface["type"],
        carrier,
        "local",
        tcp_local[1])
    logger.debug(result)

    # Test udp on wifi to local iperf
    udp_local = await tests.test_udp_local(interface)
    logger.debug(udp_local)
    result = sqlite_data.write_udp_test(
        f"{proj_dir}/data.sqlite",
        point_id,
        interface["type"],
        carrier,
        "local",
        udp_local[1])
    logger.debug(result)


async def run_remote_iperf(interface, proj_dir, point_id, carrier):
    # Test tcp on wifi to remote iperf
    if "name" in interface:
        tcp_remote = await tests.test_tcp_remote(interface)
        logger.debug(tcp_remote)
        result = sqlite_data.write_tcp_test(
            f"{proj_dir}/data.sqlite",
            point_id,
            interface["type"],
            carrier,
            "remote",
            tcp_remote[1])
        logger.debug(result)

    # test udp on wifi to remote iperf
    if "name" in interface:
        udp_remote = await tests.test_udp_remote(interface)
        logger.debug(udp_remote)
        result = sqlite_data.write_udp_test(
            f"{proj_dir}/data.sqlite",
            point_id,
            interface["type"],
            carrier,
            "remote",
            udp_remote[1])
        logger.debug(result)


async def run_control_iperf(interface, proj_dir, point_id, carrier):
    # Test tcp on control station to control iperf api
    tcp_control = tests.test_tcp_control(interface)
    logger.debug(tcp_control)
    result = sqlite_data.write_tcp_test(
        f"{proj_dir}/data.sqlite",
        point_id,
        "stationary",
        carrier,
        "remote",
        tcp_control[1])
    logger.debug(result)

    # Test udp to control iperf api
    udp_control = tests.test_udp_control(interface)
    logger.info(udp_control)
    result = sqlite_data.write_udp_test(
        f"{proj_dir}/data.sqlite",
        point_id,
        "stationary",
        carrier,
        "remote",
        udp_control[1])
    logger.debug(result)


async def assign_name(interface, modems):
    logger.debug(modems)
    logger.debug(interface)

    name = modems[str(interface["imei"])]["int"]

    return name
