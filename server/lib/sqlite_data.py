import copy
import logging
import sqlite3
import trio

from sqlite3 import Error

logger = logging.getLogger("lib_sqlite_data")


# create a database connection to the SQLite database specified by db_file
def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        logger.error(e)

    return conn


# create a table from the create_table_sql statement
def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
        return True
    except Error as e:
        logger.error("error in create_table()")
        logger.error(e)
    return False


# create points table in db_file
def create_points_tbl(db_file):
    conn = create_connection(db_file)
    statement = """ CREATE TABLE IF NOT EXISTS points(
                    id TEXT NOT NULL,
                    x INTEGER NOT NULL,
                    y INTEGER NOT NULL,
                    tested INTEGER NOT NULL,
                    tested_at TEXT,
                    type TEXT
                ); """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        conn.commit()
        return True
    except Error as e:
        logger.error("error in create_connection()")
        logger.error(e)
    return False


# create perf table in db_file
def create_perf_tbl(db_file):
    conn = create_connection(db_file)
    statement = """ CREATE TABLE IF NOT EXISTS perf(
                    point_id TEXT NOT NULL,
                    type TEXT not NULL,
                    carrier TEXT not NULL,
                    server TEXT not NULL,
                    protocol TEXT not NULL,
                    f_sec REAL,
                    r_sec REAL,
                    f_bytes REAL,
                    r_bytes REAL,
                    f_bps REAL,
                    r_bps REAL,
                    f_retries INTEGER,
                    r_retries INTEGER,
                    f_jitter REAL,
                    r_jitter REAL,
                    f_packets INTEGER,
                    r_packets INTEGER,
                    f_lost_pack INTEGER,
                    r_lost_pack INTEGER,
                    f_lost_perc REAL,
                    r_lost_perc REAL,
                    f_ooo_pack INTEGER,
                    r_ooo_pack INTEGER,
                    f_ooo_perc REAL,
                    r_ooo_perc REAL,
                    f_rtt REAL,
                    r_rtt REAL
                ); """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        conn.commit()
        return True
    except Error as e:
        logger.error("error in create_perf_tbl()")
        logger.error(e)
    return False


# create lte table in db_file
def create_lte_tbl(db_file):
    conn = create_connection(db_file)
    statement = """ CREATE TABLE IF NOT EXISTS lte(
                    point_id TEXT NOT NULL,
                    carrier TEXT not NULL,
                    rssi REAL,
                    rsrp REAL,
                    rsrq REAL,
                    sinr REAL
                ); """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        conn.commit()
        return True
    except Error as e:
        logger.error("error in create_lte_tbl()")
        logger.error(e)
    return False


# create wifi table in db_file
def create_wifi_tbl(db_file):
    conn = create_connection(db_file)
    statement = """ CREATE TABLE IF NOT EXISTS wifi(
                    point_id TEXT NOT NULL,
                    bssid TEXT not NULL,
                    essid TEXT,
                    connected INTEGER,
                    freq INTEGER,
                    band TEXT,
                    channel INTEGER,
                    clients INTEGER,
                    utilization INTEGER,
                    width INTEGER,
                    signal_str REAL,
                    signal_qual REAL,
                    datarate_tx REAL,
                    datarate_rx REAL
                ); """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        conn.commit()
        return True
    except Error as e:
        logger.error("error in create_wifi_tbl()")
        logger.error(e)
    return False


# create new point in points table
def create_point(db_file, id, x, y):
    conn = create_connection(db_file)
    statement = """ INSERT INTO points(id, x, y, tested)
                    VALUES(?, ?, ?, ?) """
    point = (id, x, y, 0)
    try:
        cur = conn.cursor()
        cur.execute(statement, point)
        conn.commit()
        return True, cur.lastrowid
    except Error as e:
        logger.error("error in create_point()")
        logger.error(e)
    return False, -1


# delete point from points table
def delete_point(db_file, id):
    errors = False
    conn = create_connection(db_file)

    statement = 'DELETE FROM points WHERE id=?'
    try:
        cur = conn.cursor()
        cur.execute(statement, (id,))
        conn.commit()
    except Error as e:
        logger.error(e)
        errors = True

    statement = 'DELETE FROM perf WHERE point_id=?;'
    try:
        cur = conn.cursor()
        cur.execute(statement, (id,))
        conn.commit()
    except Error as e:
        logger.error(e)
        errors = True

    statement = 'DELETE FROM wifi WHERE point_id=?;'
    try:
        cur = conn.cursor()
        cur.execute(statement, (id,))
        conn.commit()
    except Error as e:
        logger.error(e)
        errors = True

    statement = 'DELETE FROM lte WHERE point_id=?;'
    try:
        cur = conn.cursor()
        cur.execute(statement, (id,))
        conn.commit()
    except Error as e:
        logger.error("error in delete_point()")
        logger.error(e)
        errors = True

    if errors:
        return False
    return True


# change point x, y coords in points table
def move_point(db_file, id, x, y):
    conn = create_connection(db_file)
    statement = ''' UPDATE points
                    SET x = ? ,
                        y = ?
                    WHERE id = ? '''
    try:
        cur = conn.cursor()
        cur.execute(statement, (x, y, id,))
        conn.commit()
        return True
    except Error as e:
        logger.error("error in move_point()")
        logger.error(e)
        return False


# update timestamp on point when tested
def point_tested(db_file, id):
    conn = create_connection(db_file)
    statement = "SELECT datetime('now');"
    cur = conn.cursor()
    cur.execute(statement)
    timestamp = cur.fetchone()
    timestamp = timestamp[0]
    statement = ''' UPDATE points
                    SET tested = ? ,
                        tested_at = ?,
                        type = ?
                    WHERE id = ? '''
    task = (1, timestamp, "test", id)
    try:
        cur = conn.cursor()
        cur.execute(statement, task)
        conn.commit()

        return True
    except Error as e:
        logger.error("error in point_tested()")
        logger.error(e)

    return False


# update point to AP
def point_is_ap(db_file, id):
    conn = create_connection(db_file)
    statement = ''' UPDATE points
                    SET type = ?
                    WHERE id = ? '''
    task = ("ap", id)
    try:
        cur = conn.cursor()
        cur.execute(statement, task)
        conn.commit()

        return True
    except Error as e:
        logger.error("error in point_is_ap()")
        logger.error(e)

    return False


# write iperf tcp data to perf table
def write_tcp_test(db_file, id, type, carrier, server, data):
    conn = create_connection(db_file)

    statement = ('DELETE FROM perf WHERE point_id=?'
                 ' AND type=? AND carrier=? AND server=? AND protocol=?')
    try:
        cur = conn.cursor()
        cur.execute(statement, (id, type, carrier, server, "tcp",))
        conn.commit()
    except Error as e:
        logger.info(e)

    statement = """ INSERT INTO perf(
        point_id,
        type,
        carrier,
        server,
        protocol,
        f_sec,
        r_sec,
        f_bytes,
        r_bytes,
        f_bps,
        r_bps,
        f_retries,
        r_retries,
        f_rtt,
        r_rtt)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) """
    point = (
        id,
        type,
        carrier,
        server,
        "tcp",
        data["forward"]["seconds"],
        data["reverse"]["seconds"],
        data["forward"]["bytes"],
        data["reverse"]["bytes"],
        data["forward"]["bps"],
        data["reverse"]["bps"],
        data["forward"]["retries"],
        data["reverse"]["retries"],
        data["forward"]["rtt"],
        data["reverse"]["rtt"])
    try:
        cur = conn.cursor()
        cur.execute(statement, point)
        conn.commit()
        return True, cur.lastrowid
    except Error as e:
        logger.error("error in write_tcp_test()")
        logger.error(e)
    return False, -1


# write iperf udp data to perf table
def write_udp_test(db_file, id, type, carrier, server, data):
    logger.debug(data)
    conn = create_connection(db_file)

    statement = ('DELETE FROM perf WHERE point_id=?'
                 ' AND type=? AND carrier=? AND server=? AND protocol=?')
    try:
        cur = conn.cursor()
        cur.execute(statement, (id, type, carrier, server, "udp",))
        conn.commit()
    except Error as e:
        logger.info(e)

    statement = """ INSERT INTO perf(
        point_id,
        type,
        carrier,
        server,
        protocol,
        f_sec,
        r_sec,
        f_bytes,
        r_bytes,
        f_bps,
        r_bps,
        f_jitter,
        r_jitter,
        f_packets,
        r_packets,
        f_lost_pack,
        r_lost_pack,
        f_lost_perc,
        r_lost_perc,
        f_ooo_pack,
        r_ooo_pack,
        f_ooo_perc,
        r_ooo_perc)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) """
    point = (
        id,
        type,
        carrier,
        server,
        "udp",
        data["forward"]["seconds"],
        data["reverse"]["seconds"],
        data["forward"]["bytes"],
        data["reverse"]["bytes"],
        data["forward"]["bps"],
        data["reverse"]["bps"],
        data["forward"]["jitter_ms"],
        data["reverse"]["jitter_ms"],
        data["forward"]["packets"],
        data["reverse"]["packets"],
        data["forward"]["lost_packets"],
        data["reverse"]["lost_packets"],
        data["forward"]["lost_percent"],
        data["reverse"]["lost_percent"],
        data["forward"]["ooo_packets"],
        data["reverse"]["ooo_packets"],
        data["forward"]["ooo_percent"],
        data["reverse"]["ooo_percent"])
    try:
        cur = conn.cursor()
        cur.execute(statement, point)
        conn.commit()
        return True, cur.lastrowid
    except Error as e:
        logger.error("error in write_udp_test()")
        logger.error(e)
    return False, -1


# async wrapper for write_lte_scan
async def write_lte_scan_async(db_file, id, carrier, data):
    logger.debug('threading in write_lte_scan wrapper')
    has_errors = False
    try:
        result = await trio.to_thread.run_sync(write_lte_scan, db_file, id, carrier, data)
        has_errors = result
    except Error as e:
        logger.error("error in write_lte_scan()")
        logger.error(e)
        has_errors = True
    return has_errors


# write gsm/iperf scan info to lte table
def write_lte_scan(db_file, id, carrier, data):
    has_errors = False
    conn = create_connection(db_file)

    logger.debug('trying to delete old lte data')
    statement = ('DELETE FROM lte WHERE point_id=?'
                 'AND carrier=?')

    try:
        cur = conn.cursor()
        cur.execute(statement, (id, carrier,))
        conn.commit()
    except Error as e:
        logger.info(e)
    
    logger.debug('trying to insert new lte data')
    statement = """ INSERT INTO lte(
            point_id,
            carrier,
            rssi,
            rsrp,
            rsrq,
            sinr)
            VALUES(?, ?, ?, ?, ?, ?) """
    entry = (
        id,
        carrier,
        data["rssi"],
        data["rsrp"],
        data["rsrq"],
        data["sinr"]
    )
    try:
        cur = conn.cursor()
        cur.execute(statement, entry)
        conn.commit()
    except Error as e:
        logger.error("error in write_lte_scan()")
        logger.error(e)
        has_errors = True
    conn.close()
    return has_errors


# async wrapper for write_wifi_scan
async def write_wifi_scan_async(db_file, id, data):
    logger.debug('threading in write_wifi_scan wrapper')
    has_errors = False
    try:
        result = await trio.to_thread.run_sync(write_wifi_scan, db_file, id, data)
        has_errors = result
    except Error as e:
        logger.error("error in write_wifi_scan()")
        logger.error(e)
        has_errors = True
    return has_errors


# write wifi scan info to wifi table
def write_wifi_scan(db_file, id, data):
    conn = create_connection(db_file)

    statement = ('DELETE FROM wifi WHERE point_id=?')

    try:
        cur = conn.cursor()
        cur.execute(statement, (id,))
        conn.commit()
    except Error as e:
        logger.info(e)

    has_errors = False
    for bssid in data:
        logger.debug(bssid)
        logger.debug(data[bssid])
        connected_bssid = 0
        datarate_tx = 0
        datarate_rx = 0
        quality = float(data[bssid]["signal"]) + 110

        statement = """ INSERT INTO wifi(
            point_id,
            bssid,
            essid,
            connected,
            freq,
            band,
            channel,
            clients,
            utilization,
            width,
            signal_str,
            signal_qual,
            datarate_tx,
            datarate_rx)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) """
        entry = (
            id,
            bssid,
            data[bssid]["ssid"],
            connected_bssid,
            data[bssid]["freq"],
            data[bssid]["band"],
            data[bssid]["chan"],
            data[bssid]["clients"],
            data[bssid]["util"],
            data[bssid]["width"],
            data[bssid]["signal"],
            quality,
            datarate_tx,
            datarate_rx
            )
        try:
            cur = conn.cursor()
            cur.execute(statement, entry)
            conn.commit()
        except Error as e:
            logger.error("error in write_wifi_scan()")
            logger.error(e)
            has_errors = True
    conn.close()
    return has_errors


# add connected data to wifi scan data
def update_wifi_connected(db_file, id, data):
    conn = create_connection(db_file)
    statement = ''' UPDATE wifi
                    SET connected = ? ,
                        datarate_tx = ?,
                        datarate_rx = ?
                    WHERE point_id = ?
                    AND bssid = ? '''
    try:
        cur = conn.cursor()
        cur.execute(statement,
            (1, data["datarate_tx"], data["datarate_rx"], id, data["bssid"]))
        conn.commit()
        return True
    except Error as e:
        logger.error("error in update_wifi_connected()")
        logger.error(e)
        return False


# return list of points from points table
def get_points(db_file):
    conn = create_connection(db_file)
    conn.row_factory = lambda c, r: dict([
        (col[0], r[idx]) for idx, col in enumerate(c.description)
    ])
    statement = 'SELECT * FROM points'

    try:
        cur = conn.cursor()
        cur.execute(statement)

        rows = cur.fetchall()

        return False, rows
    except Error as e:
        logger.error("error in get_points()")
        logger.error(e)

    return True, []


def get_wifi_signal(db_file, ssid, band):
    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`
        FROM `points`
        WHERE `type` LIKE 'test'
        ;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()

        # build points dict with min assumed signal at all points
        points_dict = {}
        for point in points:
            points_dict[point[0]] = {
                "x": point[1],
                "y": point[2],
                0: {
                    "signal_str": -110
                },
                1: {
                    "signal_str": -110
                },
            }
        logger.debug(points_dict)

        # query for signal at points
        statement = f"""
            SELECT `point_id`, `signal_str`
            FROM `wifi`
            WHERE `essid` LIKE '{ssid}'
            AND `band` LIKE '{band}'
            ORDER BY `point_id`, `signal_str` DESC;
            """

        try:
            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()

            # fill in points_dict with actual values
            level = 0
            last_id = ''
            for row in rows:
                logger.debug(row)
                if last_id != row[0]:  # new id
                    points_dict[row[0]][0]["signal_str"] = row[1]
                    level = 1
                if last_id == row[0] and level == 1:  # second id
                    points_dict[row[0]][1]["signal_str"] = row[1]
                    level = 0
                last_id = row[0]
            logger.debug(points_dict)

            pri = []
            sec = []

            # convert points_dict to tupple for matlab
            for point in points_dict:
                logger.debug(point)
                pri.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point][0]["signal_str"])
                )
                sec.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point][1]["signal_str"])
                )

            return False, {"primary": pri, "secondary": sec}

        # query of wifi table failed
        except Error as e:
            logger.error(e)

    # query of points table failed
    except Error as e:
        logger.error(e)

    return True, {}


def get_wifi_datarate(db_file):
    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`, `tested_at`
        FROM `points`
        WHERE `type` LIKE 'test'
        ORDER BY `tested_at`
        ;
    """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, []

    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "datarate_tx": 0,
            "datarate_rx": 0
        }
    logger.debug(points_dict)

    # query for signal at points
    statement = f"""
        SELECT `point_id`, `datarate_tx`, `datarate_rx`
        FROM `wifi`
        WHERE `connected` LIKE '1'
        ORDER BY `point_id`;
    """

    try:
        cur = conn.cursor()
        cur.execute(statement)
        rows = cur.fetchall()
    # query of wifi table failed
    except Error as e:
        logger.error(e)
        return True, []

    # fill in points_dict with actual values
    for row in rows:
        logger.debug(row)
        points_dict[row[0]]["datarate_tx"] = float(row[1])/1000
        points_dict[row[0]]["datarate_rx"] = float(row[2])/1000
    logger.debug(points_dict)

    dp = []

    # convert points_dict to tupple for matlab
    for point in points_dict:
        logger.debug(point)
        dp.append((
            points_dict[point]["x"],
            points_dict[point]["y"],
            points_dict[point]["time"],
            points_dict[point]["datarate_tx"],
            points_dict[point]["datarate_rx"])
        )

    return False, dp


def get_wifi_channels(db_file, ssid=""):
    conn = create_connection(db_file)

    if ssid:
        # query for channels by bssid filtered by ssid
        statement = f"""
            SELECT DISTINCT `bssid`, `band`, `width`, `channel`
            FROM `wifi`
            WHERE `essid` LIKE '{ssid}';
        """
    else:
        # query for channels by bssid
        statement = f"""
            SELECT DISTINCT `bssid`, `band`, `width`, `channel`
            FROM `wifi`;
        """

    try:
        cur = conn.cursor()
        cur.execute(statement)
        rows = cur.fetchall()
        logger.debug(f'rows: {rows}')
    # query of wifi table failed
    except Error as e:
        logger.error(e)
        return True, []

    dp = []
    # fill in dp with channel data
    for row in rows:
        logger.debug(row)
        width = 20
        if row[2]:
            width = int(row[2])
        dp.append({
            "bssid": row[0],
            "band": float(row[1]),
            "width": width,
            "channel": int(row[3])
        })
    logger.debug(dp)

    return False, dp


def get_wifi_iperf(db_file, test, protocol):
    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`, `tested_at`
        FROM `points`
        WHERE `type` LIKE 'test'
        ;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()

        # build points dict with min assumed signal at all points
        zero_val = 0
        if test == 'retries': zero_val = 5000
        if test == 'rtt': zero_val = 500
        if test == 'jitter': zero_val = 200
        if test == 'lost_perc': zero_val = 100

        points_dict = {}
        for point in points:
            points_dict[point[0]] = {
                "x": point[1],
                "y": point[2],
                "time": point[3],
                "f": {
                    "l": zero_val,
                    "r": zero_val,
                    "s": zero_val,
                },
                "r": {
                    "l": zero_val,
                    "r": zero_val,
                    "s": zero_val,
                },
            }
        logger.debug(points_dict)
        cur.close()

        # query for signal at points
        statement = f"""
            SELECT `point_id`,
                CASE
	                WHEN `f_bps` <= 0 THEN {zero_val}
		            ELSE `f_{test}`
	            END as f_{test},
	            CASE
	                WHEN `r_bps` <= 0 THEN {zero_val}
		            ELSE `r_{test}`
	            END as r_{test},
	            `type`,
	            `server`
            FROM `perf`
            WHERE `protocol` LIKE '{protocol}'
            ORDER BY `point_id`
            ;
            """

        try:
            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()

            f_l = []  # tuple for local upload heatmap
            r_l = []  # tuple for local download heatmap
            f_r = []  # tuple for remote upload heatmap
            r_r = []  # tuple for remote download heatmap
            u = []  # tuple for upload chart
            d = []  # tuple for download chart

            # fill in points_dict with actual values
            for row in rows:
                logger.debug(row)
                if test == "bps":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 0
                    else:
                        val_f = float(row[1])/1000000
                    logger.debug(row[2])
                    if float(row[2]) == -1:
                        val_r = 0
                    else:
                        val_r = float(row[2])/1000000
                    if row[3] == "wifi":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"] = float(val_f)
                            points_dict[row[0]]["r"]["l"] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"] = float(val_f)
                            points_dict[row[0]]["r"]["r"] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"] = float(val_f)
                        points_dict[row[0]]["r"]["s"] = float(val_r)
                elif test == "rtt":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 500
                    else:
                        val_f = float(row[1])/1000
                    if float(row[2]) == -1:
                        val_r = 500
                    else:
                        val_r = float(row[2])/1000
                    if row[3] == "wifi":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"] = float(val_f)
                            points_dict[row[0]]["r"]["l"] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"] = float(val_f)
                            points_dict[row[0]]["r"]["r"] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"] = float(val_f)
                        points_dict[row[0]]["r"]["s"] = float(val_r)
                else:
                    if row[3] == "wifi":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"] = float(row[1])
                            points_dict[row[0]]["r"]["l"] = float(row[2])
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"] = float(row[1])
                            points_dict[row[0]]["r"]["r"] = float(row[2])
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"] = float(row[1])
                        points_dict[row[0]]["r"]["s"] = float(row[2])
            logger.debug(points_dict)

            # convert points_dict to tupple for matlab
            for point in points_dict:
                logger.debug(point)
                logger.debug(points_dict[point])
                f_l.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["f"]["l"],)
                )
                r_l.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["r"]["l"],)
                )
                f_r.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["f"]["r"],)
                )
                r_r.append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["r"]["r"],)
                )
                u.append((
                    points_dict[point]["time"],
                    points_dict[point]["f"]["l"],
                    points_dict[point]["f"]["r"],
                    points_dict[point]["f"]["s"],
                ))
                d.append((
                    points_dict[point]["time"],
                    points_dict[point]["r"]["l"],
                    points_dict[point]["r"]["r"],
                    points_dict[point]["r"]["s"],
                ))

            return False, {
                "f_l": f_l,
                "r_l": r_l,
                "f_r": f_r,
                "r_r": r_r,
                "u": u,
                "d": d}

        # query of perf table failed
        except Error as e:
            logger.error(e)

    # query of points table failed
    except Error as e:
        logger.error(e)

    return True, {}


def get_lte_carriers(db_file, tbl):
    carriers = []
    conn = create_connection(db_file)
    # query for points
    statement = f"""
        SELECT DISTINCT `carrier` FROM `{tbl}`;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        res = cur.fetchall()
        logger.debug(res)
        for row in res:
            carriers.append(row[0])
        logger.debug(carriers)
        # query of points table failed
    except Error as e:
        logger.error(e)
    return carriers


def get_lte_rsrp(db_file, carriers):
    # build filler dict for carrier rsrp values
    default_list = {}
    for carrier in carriers:
        default_list[carrier] = -120

    conn = create_connection(db_file)

    try:
        # query for points
        statement = """
            SELECT `id`, `x`, `y`, `tested_at`
            FROM `points`
            WHERE `type` LIKE 'test'
            ORDER BY `tested_at`
            ;
            """

        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, {}, []
    
    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "rsrp": default_list.copy()
        }
        logger.debug(points_dict[point[0]])
    logger.debug(points_dict)

    logger.debug(f'carriers: {carriers}')
    rsrp_list_by_carrier = {}
    rsrp_list_summary = []
    for carrier in carriers:
        # query for signal at points
        logger.debug(f'carrier: {carrier}')
        try:
            statement = f"""
                SELECT `point_id`, `rsrp`
                FROM `lte`
                WHERE `carrier` LIKE '{carrier}'
                ORDER BY `point_id`, `rsrp`;
            """

            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()
        # query of lte table failed
        except Error as e:
            logger.error(e)
            return True, {}, []

        points_dict_copy = copy.deepcopy(points_dict)

        # fill in points_dict with actual values
        logger.debug(f'sql rows: {rows}')
        for row in rows:
            logger.debug(f'row in rows: {row}')
            logger.debug(f'row id: {row[0]}')
            points_dict[row[0]]["rsrp"][carrier] = row[1]
        
        # convert points_dict to tupple for matlab for heatmaps
        # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
        for carrier in carriers:
            rsrp_list_by_carrier[carrier] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                rsrp_list_by_carrier[carrier].append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["rsrp"][carrier])
                )
        logger.debug(rsrp_list_by_carrier)

        # convert points_dict to tupple for matlab for summary chart
        # [(time1,rsrp:carrier1,rsrp:carrier2,rsrp:carrier3)...]
        for point in points_dict:
            logger.debug(points_dict[point])
            temp_tup = (points_dict[point]["time"],)
            for carrier in points_dict[point]["rsrp"]:
                logger.debug(f'{carrier}: {points_dict[point]["rsrp"][carrier]}')
                temp_tup = temp_tup + (points_dict[point]["rsrp"][carrier],)
                logger.debug(temp_tup)
            rsrp_list_summary.append(temp_tup)
        logger.debug(rsrp_list_summary)

    return False, rsrp_list_by_carrier, rsrp_list_summary


def get_lte_rsrq(db_file, carriers):
    # build filler dict for carrier rsrq values
    default_list = {}
    for carrier in carriers:
        default_list[carrier] = -120

    conn = create_connection(db_file)

    try:
        # query for points
        statement = """
            SELECT `id`, `x`, `y`, `tested_at`
            FROM `points`
            WHERE `type` LIKE 'test'
            ORDER BY `tested_at`
            ;
            """

        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, {}, []
    
    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "rsrq": default_list.copy()
        }
        logger.debug(points_dict[point[0]])
    logger.debug(points_dict)

    logger.debug(f'carriers: {carriers}')
    for carrier in carriers:
        # query for signal at points
        logger.debug(f'carrier: {carrier}')
        try:
            statement = f"""
                SELECT `point_id`, `rsrq`
                FROM `lte`
                WHERE `carrier` LIKE '{carrier}'
                ORDER BY `point_id`, `rsrq`;
            """

            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()
        # query of lte table failed
        except Error as e:
            logger.error(e)
            return True, {}, []

        points_dict_copy = copy.deepcopy(points_dict)

        # fill in points_dict with actual values
        logger.debug(f'sql rows: {rows}')
        for row in rows:
            logger.debug(f'row in rows: {row}')
            logger.debug(f'row id: {row[0]}')
            points_dict[row[0]]["rsrq"][carrier] = row[1]
        
        # convert points_dict to tupple for matlab for heatmaps
        # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
        rsrq_list_by_carrier = {}
        for carrier in carriers:
            rsrq_list_by_carrier[carrier] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                rsrq_list_by_carrier[carrier].append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["rsrq"][carrier])
                )
        logger.debug(rsrq_list_by_carrier)

        # convert points_dict to tupple for matlab for summary chart
        # [(time1,rsrq:carrier1,rsrq:carrier2,rsrq:carrier3)...]
        rsrq_list_summary = []
        for point in points_dict:
            logger.debug(points_dict[point])
            temp_tup = (points_dict[point]["time"],)
            for carrier in points_dict[point]["rsrq"]:
                logger.debug(f'{carrier}: {points_dict[point]["rsrq"][carrier]}')
                temp_tup = temp_tup + (points_dict[point]["rsrq"][carrier],)
                logger.debug(temp_tup)
            rsrq_list_summary.append(temp_tup)
        logger.debug(rsrq_list_summary)

    return False, rsrq_list_by_carrier, rsrq_list_summary


def get_lte_sinr(db_file, carriers):
    # build filler dict for carrier sinr values
    default_list = {}
    for carrier in carriers:
        default_list[carrier] = -120

    conn = create_connection(db_file)

    try:
        # query for points
        statement = """
            SELECT `id`, `x`, `y`, `tested_at`
            FROM `points`
            WHERE `type` LIKE 'test'
            ORDER BY `tested_at`
            ;
            """

        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()
    # query of points table failed
    except Error as e:
        logger.error(e)
        return True, {}, []
    
    # build points dict with min assumed signal at all points
    points_dict = {}
    for point in points:
        points_dict[point[0]] = {
            "x": point[1],
            "y": point[2],
            "time": point[3],
            "sinr": default_list.copy()
        }
        logger.debug(points_dict[point[0]])
    logger.debug(points_dict)

    logger.debug(f'carriers: {carriers}')
    for carrier in carriers:
        # query for signal at points
        logger.debug(f'carrier: {carrier}')
        try:
            statement = f"""
                SELECT `point_id`, `sinr`
                FROM `lte`
                WHERE `carrier` LIKE '{carrier}'
                ORDER BY `point_id`, `sinr`;
            """

            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()
        # query of lte table failed
        except Error as e:
            logger.error(e)
            return True, {}, []

        # fill in points_dict with actual values
        logger.debug(f'sql rows: {rows}')
        for row in rows:
            logger.debug(f'row in rows: {row}')
            logger.debug(f'row id: {row[0]}')
            points_dict[row[0]]["sinr"][carrier] = row[1]
        
        # convert points_dict to tupple for matlab for heatmaps
        # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
        sinr_list_by_carrier = {}
        for carrier in carriers:
            sinr_list_by_carrier[carrier] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                sinr_list_by_carrier[carrier].append((
                    points_dict[point]["x"],
                    points_dict[point]["y"],
                    points_dict[point]["sinr"][carrier])
                )
        logger.debug(sinr_list_by_carrier)

        # convert points_dict to tupple for matlab for summary chart
        # [(time1,sinr:carrier1,sinr:carrier2,sinr:carrier3)...]
        sinr_list_summary = []
        for point in points_dict:
            logger.debug(points_dict[point])
            temp_tup = (points_dict[point]["time"],)
            for carrier in points_dict[point]["sinr"]:
                logger.debug(f'{carrier}: {points_dict[point]["sinr"][carrier]}')
                temp_tup = temp_tup + (points_dict[point]["sinr"][carrier],)
                logger.debug(temp_tup)
            sinr_list_summary.append(temp_tup)
        logger.debug(sinr_list_summary)

    return False, sinr_list_by_carrier, sinr_list_summary


def get_lte_iperf(db_file, test, carriers, protocol):
    # build points dict with min assumed signal at all points
    zero_val = 0
    if test == 'retries': zero_val = 5000
    if test == 'rtt': zero_val = 500
    if test == 'jitter': zero_val = 200
    if test == 'lost_perc': zero_val = 100.00
    if test == 'ooo_perc': zero_val = 100.00

    # build filler dict for carrier iperf values
    default_list = {}
    logger.debug(carriers)
    for carrier in carriers:
        default_list[carrier] = zero_val
    logger.debug(default_list)

    conn = create_connection(db_file)
    # query for points
    statement = """
        SELECT `id`, `x`, `y`, `tested_at`
        FROM `points`
        WHERE `type` LIKE 'test'
        ;
        """
    try:
        cur = conn.cursor()
        cur.execute(statement)
        points = cur.fetchall()

        points_dict = {}

        for point in points:
            points_dict[point[0]] = {
                "x": point[1],
                "y": point[2],
                "time": point[3],
                "f": {
                    "l": default_list.copy(),
                    "r": default_list.copy(),
                    "s": default_list.copy(),
                },
                "r": {
                    "l": default_list.copy(),
                    "r": default_list.copy(),
                    "s": default_list.copy(),
                },
            }
        logger.debug(points_dict)
        cur.close()

        if test == 'rtt': zero_val = 500000

        # query for signal at points
        statement = f"""
            SELECT `point_id`,
                CASE
	                WHEN `f_bps` <= 0 THEN {zero_val}
		            ELSE `f_{test}`
	            END as f_{test},
	            CASE
	                WHEN `r_bps` <= 0 THEN {zero_val}
		            ELSE `r_{test}`
	            END as r_{test},
	            `type`,
	            `server`,
                `carrier`
            FROM `perf`
            WHERE `protocol` LIKE '{protocol}'
            ORDER BY `point_id`
            ;
            """

        try:
            cur = conn.cursor()
            cur.execute(statement)
            rows = cur.fetchall()

            # fill in points_dict with actual values
            for row in rows:
                logger.debug(row)
                if test == "bps":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 0
                    else:
                        val_f = float(row[1])/1000000
                    logger.debug(row[2])
                    if float(row[2]) == -1:
                        val_r = 0
                    else:
                        val_r = float(row[2])/1000000
                    if row[3] == "lte":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["l"][row[5]] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["r"][row[5]] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"][row[5]] = float(val_f)
                        points_dict[row[0]]["r"]["s"][row[5]] = float(val_r)
                elif test == "rtt":
                    logger.debug(row[1])
                    if float(row[1]) == -1:
                        val_f = 500
                    else:
                        val_f = float(row[1])/1000
                    if float(row[2]) == -1:
                        val_r = 500
                    else:
                        val_r = float(row[2])/1000
                    if row[3] == "lte":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["l"][row[5]] = float(val_r)
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"][row[5]] = float(val_f)
                            points_dict[row[0]]["r"]["r"][row[5]] = float(val_r)
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"][row[5]] = float(val_f)
                        points_dict[row[0]]["r"]["s"][row[5]] = float(val_r)
                else:
                    logger.debug(row[1])
                    if row[3] == "lte":
                        if row[4] == "local":
                            points_dict[row[0]]["f"]["l"][row[5]] = float(row[1])
                            points_dict[row[0]]["r"]["l"][row[5]] = float(row[2])
                        if row[4] == "remote":
                            points_dict[row[0]]["f"]["r"][row[5]] = float(row[1])
                            points_dict[row[0]]["r"]["r"][row[5]] = float(row[2])
                    if row[3] == "stationary":
                        points_dict[row[0]]["f"]["s"][row[5]] = float(row[1])
                        points_dict[row[0]]["r"]["s"][row[5]] = float(row[2])
            logger.debug(f'points_dict: {points_dict}')

            # convert points_dict to tupple for matlab for heatmaps
            # {carrier1: [(x1,y1,v1),(x2,y2,v2)]..., carrier2:...}
            iperf_list_by_carrier = {}
            for carrier in carriers:
                iperf_list_by_carrier[carrier] = {
                    "f_l": [],
                    "f_r": [],
                    "f_s": [],
                    "r_l": [],
                    "r_r": [],
                    "r_s": []
                }
                for point in points_dict:
                    logger.debug(points_dict[point])
                    iperf_list_by_carrier[carrier]["f_l"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["f"]["l"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["r_l"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["r"]["l"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["f_r"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["f"]["r"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["r_r"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["r"]["r"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["f_s"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["f"]["s"][carrier])
                    )
                    iperf_list_by_carrier[carrier]["r_s"].append((
                        points_dict[point]["x"],
                        points_dict[point]["y"],
                        points_dict[point]["r"]["s"][carrier])
                    )
            logger.debug(f'iperf_list_by_carrier: {iperf_list_by_carrier}')

            # convert points_dict to tupple for matlab for summary chart
            # [(time1,v1:carrier1,v1:carrier2,v1:carrier3)...]
            iperf_list_summary = {}
            iperf_list_summary['u'] = []
            iperf_list_summary['d'] = []
            for point in points_dict:
                logger.debug(points_dict[point])
                u_temp_tup = (points_dict[point]["time"],)
                d_temp_tup = (points_dict[point]["time"],)
                for carrier in carriers:
                    logger.debug(f'{carrier}: {points_dict[point]["f"]["l"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["f"]["r"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["f"]["s"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["r"]["l"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["r"]["r"][carrier]}')
                    logger.debug(f'{carrier}: {points_dict[point]["r"]["s"][carrier]}')
                    u_temp_tup = u_temp_tup + (
                        points_dict[point]["f"]["l"][carrier],
                        points_dict[point]["f"]["r"][carrier],
                        points_dict[point]["f"]["s"][carrier],
                    )
                    logger.debug(u_temp_tup)
                    d_temp_tup = d_temp_tup + (
                        points_dict[point]["r"]["l"][carrier],
                        points_dict[point]["r"]["r"][carrier],
                        points_dict[point]["r"]["s"][carrier],
                    )
                    logger.debug(d_temp_tup)
                iperf_list_summary['u'].append(u_temp_tup)
                iperf_list_summary['d'].append(d_temp_tup)
            logger.debug(f"iperf_list_summary['u']: {iperf_list_summary['u']}")
            logger.debug(f"iperf_list_summary['d']: {iperf_list_summary['d']}")

            return False, iperf_list_by_carrier, iperf_list_summary

        # query of perf table failed
        except Error as e:
            logger.error(e)

    # query of points table failed
    except Error as e:
        logger.error(e)

    return True, {}, {}
