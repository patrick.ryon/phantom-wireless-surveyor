import logging
import os
import trio

from subprocess import Popen, PIPE

logger = logging.getLogger("lib_shell")


def shell_exec(cmd, env={}):
    d = dict(os.environ)
    logger.debug(f'cmd: {cmd}')
    logger.debug(f'env: {env}')
    if len(env) > 0:
        for e in env:
            d[e]=env[e]
    try:
        process = Popen(cmd.split(), stdout=PIPE, stderr=PIPE, env=d)
    except Exception as e:
        logger.warning(e)
        return 1, None, None
    (output, err) = process.communicate()
    output = output.decode("utf-8")
    logger.debug(f'process.stdout: {output}')
    if err:
        err = err.decode("utf-8")
        logger.warning(f'process.stderr: {err}')
        logger.warning(err)
    exit_code = process.wait()
    logger.debug(f'process.returncode: {exit_code}')
    return exit_code, output, err


async def shell_exec_async(cmd, env={}):
    async with trio.open_nursery() as nursery:
        d = dict(os.environ)
        logger.debug(f'cmd: {cmd}')
        logger.debug(f'env: {env}')
        
        if len(env) > 0:
            for e in env:
                d[e] = env[e]

        try:
            process = await trio.run_process(
                cmd.split(),
                capture_stdout=True,
                capture_stderr=True,
                env=d)
        except Exception as e:
            logger.warning(e)
            return 1, None, e

        logger.debug(f'process.stdout: {process.stdout.decode("utf-8")} {type(process.stdout)}')
        logger.debug(f'process.stderr: {process.stderr.decode("utf-8")} {type(process.stderr)}')
        logger.debug(f'process.returncode: {process.returncode} {type(process.returncode)}')
        
        output = process.stdout.decode("utf-8")
        err = process.stderr.decode("utf-8")
        exit_code = process.returncode

        if err:
            logger.warning(err)

        return exit_code, output, err
