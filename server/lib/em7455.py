import logging
import re
import serial

logger = logging.getLogger("lib_em7455")


def get_status(port):

    # Setup serial connection
    baud_rate = 9600
    ser = serial.Serial(port, baud_rate, timeout=3)

    # List of AT commands to send
    commands = [b'AT!GSTATUS?\r', b'AT!LTEINFO?\r']

    status = False
    output = []
    try:
        # Iterate through the list of commands
        for command in commands:
            ser.write(command)  # Send the command
            response = ser.read(1024)  # Read the response (buffer size)
            c = command.decode('utf-8').strip()
            logger.debug(f"Command: {c}")
            r = response.decode('utf-8').strip()
            logger.debug(f"Command: {r}")
            output.append(r)
            status = True
    except Exception as e:
        logging.error(e)
        status = False

    finally:
        # Close the serial connection
        ser.close()
        return (status, output)


def parse_status(content):
    rssi = -101
    rsrp = -121
    rsrq = -31
    sinr = -1

    found_1 = False
    found_2 = False

    # for multiple items in content
    for item in content:
        lines = item.split('\n')

        # process line by line
        for line in lines:
            line = line.strip()

            logger.debug(line)

            match = re.search(r'SINR \(dB\):\s+([\d.]+)', line)
            if match is not None:
                sinr = match.group(1)
                found_1 = True
            match = re.search(
                r'\d+\s+\d+\s+\d+\s+\d+\s+\w+\s+\d+\s+\d+\s+\d+\s+[\d\-]+'
                r'\s+\d+\s+([\d\-.]+)\s+([\d\-.]+)\s+([\d\-.]+)\s+', line)
            if match is not None:
                rsrq = match.group(1)
                rsrp = match.group(2)
                rssi = match.group(3)
                found_2 = True

    return (found_1 and found_2), {
        'rssi': float(rssi),
        'rsrp': float(rsrp),
        'rsrq': float(rsrq),
        'sinr': float(sinr),
    }
