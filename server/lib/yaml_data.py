import logging
import os
import yaml

logger = logging.getLogger("lib_yaml_data")


def write_yaml(data, file, path=''):
    err = False
    try:
        if path == "":
            path = os.path.dirname(os.path.realpath(__name__))
        file = os.path.join(path, file)
        with open(file, "w") as out_file:
            yaml.dump(data, out_file)
            out_file.close()
    except Exception as e:
        err = True
        logger.warning(f'write_yaml exception {e}')

    return err


def read_yaml(file, path=''):
    err = False
    try:
        if path == "":
            path = os.path.dirname(os.path.realpath(__name__))
        file = os.path.join(path, file)
        with open(file, 'r') as in_file:
            data = yaml.safe_load(in_file)
            in_file.close()
    except Exception as e:
        err = True
        data = {}
        logger.warning(f'read_yaml exception {e}')

    return err, data
