import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import logging
import numpy as np
import os
import sys
import yaml

from datetime import datetime
from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Setup logging
logger = logging.getLogger("lib_report_lte")


def rsrp_data(proj_name, report_dir, data_file, carriers, floor_img, labels=False):

    err, dp_by_carrier, dp_summary = sqlite_data.get_lte_rsrp(
        data_file, carriers)
    
    if not err:
        create_rsrp_heatmaps(proj_name, report_dir, dp_by_carrier, carriers,
                             floor_img, labels)
        create_rsrp_chart(proj_name, report_dir, dp_summary, carriers)


def create_rsrp_chart(proj_name, report_dir, data_points, carriers):

    # Extract timestamps and data columns
    timestamps = []
    for data_point in data_points:
        logger.debug(data_point)
        ts, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)

    numeric_data = [values for _, *values in data_points]

    # Create a list of column labels
    column_labels = carriers

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Time')
    ax.set_title(f'RSRP Values over Time')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-lte-summary-rsrp.jpg")


def create_rsrp_heatmaps(proj_name, report_dir, data_points, carriers, floor_img, labels=False):
    logger.debug(f"report directory: {report_dir}")

    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    for carrier in carriers:
        # Extract x, y, and z values from data points
        x, y, z = zip(*data_points[carrier])

        # Define the interpolate grid using floorplan dimensions
        xi, yi = np.meshgrid(
            np.linspace(0, floor_width, floor_width),
            np.linspace(0, floor_height, floor_height))
        xi, yi = xi.flatten(), yi.flatten()

        logger.debug(xi)
        logger.debug(yi)

        # Create the interpolant using RBF
        rbf = Rbf(x, y, z, function='cubic')

        # Interpolate the data
        zi = rbf(xi, yi)

        # Set values in zi to -120 where they are 0
        zi[zi == 0] = -120

        # Set the minimum and maximum values for the colormap
        vmin = -120  # Set your desired minimum value here
        vmax = -60  # Set your desired maximum value here

        # Set values in zi to the specified color
        cmap_colors = [
            (0.5, 0.5, 0.5),  # Gray
            (0, 1, 0),  # Green
            (1, 1, 0),  # Yellow
            (1, 0, 0)]  # Red
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', cmap_colors, N=256)

        # Create a figure
        fig, ax = plt.subplots(figsize=(10, 8))

        # Display the floorplan image with 'origin' set to 'upper'
        ax.imshow(floor_img, origin='upper')

        # Define the custom colormap with specified color transitions
        colors = [  # primary coverage colormap
            ((-120+120)/60, 'gray'),   # 0
            ((-112+120)/60, 'gray'),   # .333...
            ((-109+120)/60, 'red'),    # .333...
            ((-104+120)/60, 'red'),    # .333...
            ((-101+120)/60, 'yellow'), # .35
            ((-86+120)/60, 'yellow'),  # .25
            ((-83+120)/60, 'green'),   # .667...
            ((-60+120)/60, 'green'),]  # 1
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', colors)

        # Create heatmap overlay using interpolated data with colormap
        heatmap = ax.imshow(
            zi.reshape(floor_height, floor_width),
            cmap=cmap,
            alpha=0.5,
            extent=[0, floor_width, floor_height, 0],
            vmin=vmin, vmax=vmax)

        # Remove axis labels and tick marks
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlabel("")
        ax.set_ylabel("")

        # Add red dots for test points
        ax.scatter(x, y, c='red', marker='o', s=50)

        if labels:  # Add labels below the dots
            for i in range(len(x)):
                ax.text(x[i], y[i] + 18,
                        f'({x[i]}, {y[i]})',
                        ha='center', va='top',
                        fontsize=8)

        # Add a colorbar
        cbar = plt.colorbar(heatmap, orientation='horizontal')
        cbar.set_label('RSRP Value (dBm)')

        # Add titles and labels
        ax.set_title(f'{carrier} RSRP')

        # Save to file
        plt.tight_layout()
        plt.savefig(f"{report_dir}/{proj_name.lower()}-lte-{carrier}-rsrp.jpg")


def rsrq_data(proj_name, report_dir, data_file, carriers, floor_img, labels=False):

    err, dp_by_carrier, dp_summary = sqlite_data.get_lte_rsrq(
        data_file, carriers)
    
    if not err:
        create_rsrq_heatmaps(proj_name, report_dir, dp_by_carrier, carriers,
                             floor_img, labels)
        create_rsrq_chart(proj_name, report_dir, dp_summary, carriers)


def create_rsrq_chart(proj_name, report_dir, data_points, carriers):
    logger.debug(f"report directory: {report_dir}")

    # Extract timestamps and data columns
    timestamps = []
    for data_point in data_points:
        ts, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)

    numeric_data = [values for _, *values in data_points]

    # Create a list of column labels
    column_labels = carriers

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Time')
    ax.set_title(f'RSRQ Values over Time')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-lte-summary-rsrq.jpg")


def create_rsrq_heatmaps(proj_name, report_dir, data_points, carriers, floor_img, labels=False):
    logger.debug(f"report directory: {report_dir}")

    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    for carrier in carriers:
        # Extract x, y, and z values from data points
        x, y, z = zip(*data_points[carrier])

        # Define the interpolate grid using floorplan dimensions
        xi, yi = np.meshgrid(
            np.linspace(0, floor_width, floor_width),
            np.linspace(0, floor_height, floor_height))
        xi, yi = xi.flatten(), yi.flatten()

        logger.debug(xi)
        logger.debug(yi)

        # Create the interpolant using RBF
        rbf = Rbf(x, y, z, function='cubic')

        # Interpolate the data
        zi = rbf(xi, yi)

        # Set values in zi to -31 where they are 0
        zi[zi == 0] = -31

        # Set the minimum and maximum values for the colormap
        vmin = -31  # Set your desired minimum value here
        vmax = 0  # Set your desired maximum value here

        # Set values in zi to the specified color
        cmap_colors = [
            (0.5, 0.5, 0.5),  # Gray
            (0, 1, 0),  # Green
            (1, 1, 0),  # Yellow
            (1, 0, 0)]  # Red
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', cmap_colors, N=256)

        # Create a figure
        fig, ax = plt.subplots(figsize=(10, 8))

        # Display the floorplan image with 'origin' set to 'upper'
        ax.imshow(floor_img, origin='upper')

        # Define the custom colormap with specified color transitions
        colors = [  # primary coverage colormap
            ((-30+30)/30, 'gray'),  # 0
            ((-21+30)/30, 'gray'),
            ((-19+30)/30, 'red'), 
            ((-17+30)/30, 'yellow'),
            ((-15+30)/30, 'yellow'),
            ((-13+30)/30, 'green'),
            ((0+30)/30, 'green'),]  # 1
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', colors)

        # Create heatmap overlay using interpolated data with colormap
        heatmap = ax.imshow(
            zi.reshape(floor_height, floor_width),
            cmap=cmap,
            alpha=0.5,
            extent=[0, floor_width, floor_height, 0],
            vmin=vmin, vmax=vmax)

        # Remove axis labels and tick marks
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlabel("")
        ax.set_ylabel("")

        # Add red dots for test points
        ax.scatter(x, y, c='red', marker='o', s=50)

        if labels:  # Add labels below the dots
            for i in range(len(x)):
                ax.text(x[i], y[i] + 18,
                        f'({x[i]}, {y[i]})',
                        ha='center', va='top',
                        fontsize=8)

        # Add a colorbar
        cbar = plt.colorbar(heatmap, orientation='horizontal')
        cbar.set_label('RSRQ Value (dB)')

        # Add titles and labels
        ax.set_title(f'{carrier} RSRQ')

        # Save to file
        plt.tight_layout()
        plt.savefig(f"{report_dir}/{proj_name.lower()}-lte-{carrier}-rsrq.jpg")


def sinr_data(proj_name, report_dir, data_file, carriers, floor_img, labels=False):

    err, dp_by_carrier, dp_summary = sqlite_data.get_lte_sinr(
        data_file, carriers)
    
    if not err:
        create_sinr_heatmaps(proj_name, report_dir, dp_by_carrier, carriers,
                             floor_img, labels)
        create_sinr_chart(proj_name, report_dir, dp_summary, carriers)


def create_sinr_chart(proj_name, report_dir, data_points, carriers):

    # Extract timestamps and data columns
    timestamps = []
    for data_point in data_points:
        ts, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)

    numeric_data = [values for _, *values in data_points]

    # Create a list of column labels
    column_labels = carriers

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Time')
    ax.set_title(f'SINR Values over Time')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-lte-summary-sinr.jpg")


def create_sinr_heatmaps(proj_name, report_dir, data_points, carriers, floor_img, labels=False):
    logger.debug(f"report directory: {report_dir}")

    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    for carrier in carriers:
        # Extract x, y, and z values from data points
        x, y, z = zip(*data_points[carrier])

        # Define the interpolate grid using floorplan dimensions
        xi, yi = np.meshgrid(
            np.linspace(0, floor_width, floor_width),
            np.linspace(0, floor_height, floor_height))
        xi, yi = xi.flatten(), yi.flatten()

        logger.debug(xi)
        logger.debug(yi)

        # Create the interpolant using RBF
        rbf = Rbf(x, y, z, function='cubic')

        # Interpolate the data
        zi = rbf(xi, yi)

        # Set values in zi to 0 where they are 0
        zi[zi == 0] = 0

        # Set the minimum and maximum values for the colormap
        vmin = 0  # Set your desired minimum value here
        vmax = 30  # Set your desired maximum value here

        # Set values in zi to the specified color
        cmap_colors = [
            (0.5, 0.5, 0.5),  # Gray
            (0, 1, 0),  # Green
            (1, 1, 0),  # Yellow
            (1, 0, 0)]  # Red
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', cmap_colors, N=256)

        # Create a figure
        fig, ax = plt.subplots(figsize=(10, 8))

        # Display the floorplan image with 'origin' set to 'upper'
        ax.imshow(floor_img, origin='upper')

        # Define the custom colormap with specified color transitions
        colors = [  # primary coverage colormap
            ((0+0)/30, 'gray'),  # 0
            ((1+0)/30, 'red'),
            ((5+0)/30, 'yellow'),
            ((8+0)/30, 'yellow'),
            ((10+0)/30, 'green'),
            ((30+0)/30, 'green'),]  # 1
        cmap = LinearSegmentedColormap.from_list(
            'custom_colormap', colors)

        # Create heatmap overlay using interpolated data with colormap
        heatmap = ax.imshow(
            zi.reshape(floor_height, floor_width),
            cmap=cmap,
            alpha=0.5,
            extent=[0, floor_width, floor_height, 0],
            vmin=vmin, vmax=vmax)

        # Remove axis labels and tick marks
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlabel("")
        ax.set_ylabel("")

        # Add red dots for test points
        ax.scatter(x, y, c='red', marker='o', s=50)

        if labels:  # Add labels below the dots
            for i in range(len(x)):
                ax.text(x[i], y[i] + 18,
                        f'({x[i]}, {y[i]})',
                        ha='center', va='top',
                        fontsize=8)

        # Add a colorbar
        cbar = plt.colorbar(heatmap, orientation='horizontal')
        cbar.set_label('SINR Value (dB)')

        # Add titles and labels
        ax.set_title(f'{carrier} SINR')

        # Save to file
        plt.tight_layout()
        plt.savefig(f"{report_dir}/{proj_name.lower()}-lte-{carrier}-sinr.jpg")
