import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import logging
import os
import sys
import yaml

from datetime import datetime
from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Setup logging
logger = logging.getLogger("lib_report_wifi")


def signal_data(proj_name, report_dir, data_file, floor_img, ssid, labels=False):
    logger.debug(f"report directory: {report_dir}")
    logger.debug(f"ssid: {ssid}")

    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    for band in ["2.4", "5", "6"]:
        logger.debug(f"processing band: {band}")

        err, data_points = sqlite_data.get_wifi_signal(
            data_file, ssid, band)

        if not err:
            for level in data_points:
                logger.debug(level)
                # Extract x, y, and z values from data points
                x, y, z = zip(*data_points[level])

                # Define the interpolate grid using floorplan dimensions
                xi, yi = np.meshgrid(
                    np.linspace(0, floor_width, floor_width),
                    np.linspace(0, floor_height, floor_height))
                xi, yi = xi.flatten(), yi.flatten()

                logger.debug(xi)
                logger.debug(yi)

                # Create the interpolant using RBF
                rbf = Rbf(x, y, z, function='cubic')

                # Interpolate the data
                zi = rbf(xi, yi)

                # Set values in zi to -110 where they are 0
                zi[zi == 0] = -110

                # Set the minimum and maximum values for the colormap
                vmin = -110  # Set your desired minimum value here
                vmax = -30  # Set your desired maximum value here

                # Set values in zi to the specified color
                cmap_colors = [
                    (0.5, 0.5, 0.5),  # Gray
                    (0, 1, 0),  # Green
                    (1, 1, 0),  # Yellow
                    (1, 0, 0)]  # Red
                cmap = LinearSegmentedColormap.from_list(
                    'custom_colormap', cmap_colors, N=256)

                # Create a figure
                fig, ax = plt.subplots(figsize=(10, 8))

                # Display the floorplan image with 'origin' set to 'upper'
                ax.imshow(floor_img, origin='upper')

                # Define the custom colormap with specified color transitions
                colors = [  # primary coverage colormap
                    ((-110+110)/80, 'gray'),
                    ((-70+110)/80, 'gray'),
                    ((-67+110)/80, 'red'),
                    ((-65+110)/80, 'yellow'),
                    ((-58+110)/80, 'green'),
                    ((-52+110)/80, 'green'),
                    ((-48+110)/80, 'green'),
                    ((-30+110)/80, 'green'),]
                if level == "secondary":
                    colors = [  # secondary coverage colormap
                        ((-110+110)/80, 'gray'),
                        ((-75+110)/80, 'gray'),
                        ((-72+110)/80, 'red'),
                        ((-68+110)/80, 'yellow'),
                        ((-62+110)/80, 'green'),
                        ((-58+110)/80, 'green'),
                        ((-52+110)/80, 'green'),
                        ((-30+110)/80, 'green'),]
                cmap = LinearSegmentedColormap.from_list(
                    'custom_colormap', colors)

                # Create heatmap overlay using interpolated data with colormap
                heatmap = ax.imshow(
                    zi.reshape(floor_height, floor_width),
                    cmap=cmap,
                    alpha=0.5,
                    extent=[0, floor_width, floor_height, 0],
                    vmin=vmin, vmax=vmax)

                # Remove axis labels and tick marks
                ax.set_xticks([])
                ax.set_yticks([])
                ax.set_xlabel("")
                ax.set_ylabel("")

                # Add red dots for test points
                ax.scatter(x, y, c='red', marker='o', s=50)

                if labels:  # Add labels below the dots
                    for i in range(len(x)):
                        ax.text(x[i], y[i] + 18,
                                f'({x[i]}, {y[i]})',
                                ha='center', va='top',
                                fontsize=8)

                # Add a colorbar
                cbar = plt.colorbar(heatmap, orientation='horizontal')
                cbar.set_label('Signal strength in dBm')

                # Add titles and labels
                ax.set_title(f'{band}GHz {level} signal strength'.title())

                # Save to file
                plt.tight_layout()
                plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{band}ghz-{level}"
                            "-signal-strength.jpg")


def datarate_data(proj_name, report_dir, data_file, floor_img, ssid,
                  labels=False):
    logger.debug(f"report directory: {report_dir}")
    logger.debug(f"ssid: {ssid}")
    
    err, data_points = sqlite_data.get_wifi_datarate(data_file)

    if not err:
        # Datarate TX Heatmap
        test_type = "datarate_tx"
        test_name = "Wi-Fi Tx Datarate"
        create_datarate_heatmap(proj_name, report_dir, floor_img,
                                data_points, test_type, test_name, labels)
        # Datarate RX Heatmap
        test_type = "datarate_rx"
        test_name = "Wi-Fi Rx Datarate"
        create_datarate_heatmap(proj_name, report_dir, floor_img,
                                data_points, test_type, test_name, labels)
        # Data summary chart
        test_type = "datarate_summary"
        test_name = "Wi-Fi Datarate Summary"
        create_datarate_chart(proj_name, report_dir, data_points, test_type,
                              test_name)
    else:
        logger.error("error getting datarate values from DB")


def create_datarate_heatmap(proj_name, report_dir, floor_img, data_points,
                            test_type, test_name, labels):
    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    logger.debug(f'{test_name}')
    logger.debug(data_points)
    # Extract x, y, and z values from data points
    x, y, ts, tx, rx = zip(*data_points)
    if test_type == "datarate_tx":
        z = tx
    else:
        z = rx

    # Define the interpolate grid using floorplan dimensions
    xi, yi = np.meshgrid(
        np.linspace(0, floor_width, floor_width),
        np.linspace(0, floor_height, floor_height))
    #xi, yi = xi.flatten(), yi.flatten()

    # Create the interpolant using RBF
    rbf = Rbf(x, y, z, function='cubic')

    # Interpolate the data
    zi = rbf(xi, yi)
    logger.info('zi:')
    logger.info(zi)

    # Set values in zi to 0 where they are 0
    zi[zi == 0] = 0

    # Set the minimum and maximum values for the colormap
    vmin = 0  # Set your desired minimum value here
    vmax = 1000  # Set your desired maximum value here

    # Set values in zi to the specified color
    cmap_colors = [
        (0.5, 0.5, 0.5),  # Gray
        (0, 1, 0),  # Green
        (1, 1, 0),  # Yellow
        (1, 0, 0)]  # Red
    cmap = LinearSegmentedColormap.from_list(
        'custom_colormap', cmap_colors, N=256)

    # Create a figure
    fig, ax = plt.subplots(figsize=(10, 8))

    # Display the floorplan image with 'origin' set to 'upper'
    ax.imshow(floor_img, origin='upper')

    # Define the custom colormap with specified color transitions
    colors = [  # retries coverage colormap
        ((0)/1000, 'gray'),
        ((20)/1000, 'gray'),
        ((25)/1000, 'red'),
        ((50)/1000, 'yellow'),
        ((100)/1000, 'green'),
        ((1000)/1000, 'green'),
    ]
    cmap = LinearSegmentedColormap.from_list('custom_colormap', colors)

    # Create heatmap overlay using interpolated data with colormap
    heatmap = ax.imshow(
        zi.reshape(floor_height, floor_width),
        cmap=cmap,
        alpha=0.5,
        extent=[0, floor_width, floor_height, 0],
        vmin=vmin, vmax=vmax)

    # Remove axis labels and tick marks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel("")
    ax.set_ylabel("")

    # Add red dots for test points
    ax.scatter(x, y, c='red', marker='o', s=50)

    if labels:  # Add labels below the dots
        for i in range(len(x)):
            ax.text(x[i], y[i] + 18,
                    f'({x[i]}, {y[i]})',
                    ha='center', va='top',
                    fontsize=8)

    # Add a colorbar
    cbar = plt.colorbar(heatmap, orientation='horizontal')
    
    cbar.set_label('Rate (Mbps)')

    # Add titles and labels
    ax.set_title(f'{test_name}')

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{test_type.replace('_','-')}"
                ".jpg")


def create_datarate_chart(proj_name, report_dir, data_points, test_type, test_name):

    # Extract timestamps and data columns
    timestamps = []
    numeric_data = []
    for data_point in data_points:
        _, _, ts, tx, rx = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)
        numeric_data.append([tx, rx])


    #numeric_data = [values for _, _, _, *values in data_points]
    logger.debug(f'numeric_data: {numeric_data}')

    # Create a list of column labels
    column_labels = [
        f'Tx',
        f'Rx'
    ]

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Times')
    ax.set_title(f'{test_name}')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{test_type.replace('_','-')}"
                ".jpg")


def channel_data(proj_name, report_dir, data_file, ssid, labels=False):
    logger.debug(f"report directory: {report_dir}")
    logger.debug(f"ssid: {ssid}")
    
    err, data_points = sqlite_data.get_wifi_channels(data_file)

    if not err:
        # Channel charts
        create_channel_charts(proj_name, report_dir, data_points, '', labels)
    else:
        logger.error("error getting all channel values from DB")

    err, data_points = sqlite_data.get_wifi_channels(data_file, ssid)

    if not err:
        # Channel charts with ssid
        create_channel_charts(proj_name, report_dir, data_points, ssid, labels)
    else:
        logger.error("error getting tested channel values from DB")


def create_channel_charts(proj_name, report_dir, data_points, ssid="", labels=False):
    bssids="all"
    if ssid:
        bssids = "tested"

    # Data for 2.4GHz and 5GHz bands
    channels_2_4ghz = np.arange(1, 13)
    utilization_2_4ghz = np.zeros(len(channels_2_4ghz))

    channels_5ghz = np.array(
        [36, 40, 44, 48,
         52, 56, 60, 64,
         100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144,
         149, 153, 157, 161, 165])
    utilization_5ghz = np.zeros(len(channels_5ghz))

    # DFS channel range in 5GHz band (example range, adjust based on your specific DFS range)
    unii1_s = 34
    unii1_e = 50
    unii2a_s = 50
    unii2a_e = 66
    unii2b_s = 66
    unii2b_e = 98
    unii2c_s = 98
    unii2c_e = 147
    unii3_s = 147
    unii3_e = 167

    # Populate 5GHz and 2.4GHz utilization data based on the provided Wi-Fi data
    for entry in data_points:
        if entry['band'] == 5.0:
            channel_index = np.searchsorted(channels_5ghz, entry['channel'])
            channel_range = 1
            if entry['width'] == 160:
                channel_range = 8
            if entry['width'] == 80:
                channel_range = 4
            if entry['width'] == 40:
                channel_range = 2
            for i in range(channel_range):
                utilization_5ghz[channel_index + i] += 1
        elif entry['band'] == 2.4:
            channel_index = entry['channel'] - 1
            utilization_2_4ghz[channel_index] += 1

    # Plotting the Wi-Fi spectrum/channel utilization chart for 2.4GHz
    plt.figure(figsize=(12, 6))

    # Plot for 2.4GHz
    plt.bar(channels_2_4ghz, utilization_2_4ghz, width=0.4, color='blue', alpha=0.7, label='2.4GHz BSSIDs')

    # Customize the chart
    plt.title('Wi-Fi Channel Utilization - 2.4GHz')
    plt.xlabel('Wi-Fi Channels')
    plt.ylabel('Count of BSSIDs')
    plt.xticks(channels_2_4ghz)
    plt.legend()

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-2.4ghz-channels-{bssids}.jpg")

    # Plotting the Wi-Fi spectrum/channel utilization chart for 5GHz
    plt.figure(figsize=(12, 6))

    # Plot for 5GHz
    plt.bar(channels_5ghz - 2, utilization_5ghz, width=4, color='green', alpha=0.7, label='5GHz BSSIDs', align='edge')

    # Highlight 5GHz channel ranges with vertical lines
    line_height = plt.ylim()[1]  # Set line height to the top of the plot
    plt.axvspan(unii1_s, unii1_e, color='lightblue', alpha=0.3, label='UNII-1 Range')
    plt.axvspan(unii2a_s, unii2a_e, color='lightgreen', alpha=0.3, label='UNII-2a Range')
    plt.axvspan(unii2b_s, unii2b_e, color='gray', alpha=0.3, label='UNII-2b Range (Not used)')
    plt.axvspan(unii2c_s, unii2c_e, color='lightcoral', alpha=0.3, label='UNII-2c Range')
    plt.axvspan(unii3_s, unii3_e, color='lemonchiffon', alpha=0.3, label='UNII-3 Range')

    # Customize the chart
    plt.title('Wi-Fi Channel Utilization - 5GHz')
    plt.xlabel('Wi-Fi Channels')
    plt.ylabel('Count of BSSIDs')
    plt.xticks(channels_5ghz)
    plt.legend(loc='upper left', bbox_to_anchor=(0.265, 1.0))

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-5ghz-channels-{bssids}.jpg")
