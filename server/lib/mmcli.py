import json
import logging
import re
import lib.shell as shell

logger = logging.getLogger('lib_mmcli')


# Run and collect connections from nmcli
def modem_list():
    cmd = 'sudo mmcli -L -J'
    logger.debug('modem list command: {}'.format(cmd))
    exit_code, output, err = shell.shell_exec(cmd)
    logger.debug(exit_code)
    if not err:
        data = json.loads(output)
        logger.debug(data)
        if len(data) > 0:
            return True, data
    logger.warning(err)
    return False, {"modem-list": []}


# Parse mmcli modem list table
def modem_list_parser(list):

    modem_list = []
    for modem in list["modem-list"]:
        logger.debug(modem)
        match = re.search(r'\/ModemManager\d+\/Modem\/(\d+)', modem)
        if match is not None:
            modem_list.append(match.group(1))

    return modem_list


# Run and collect connections from nmcli
def modem(id):
    cmd = f'sudo mmcli -m {id} -J'
    logger.debug('modem command: {}'.format(cmd))
    exit_code, output, err = shell.shell_exec(cmd)
    logger.debug(exit_code)
    if not err:
        data = json.loads(output)
        logger.debug(data)
        if len(data) > 0:
            return True, data
    logger.warning(err)
    return False, {}


# Parse mmcli modem list table
def modem_parser(details):
    logger.debug(f'modem details: {details}')

    modem_details = {
        "operator": "unknown",
        "state": "unknown",
        "registration": "",
        "port": "",
        "int": "",
        "tty": "",
        "imei": "",
        "bearers": [],
    }
    modem_details['operator'] = details['modem']['3gpp']['operator-name']
    modem_details['imei'] = details['modem']['3gpp']['imei']
    modem_details['registration'] = details['modem']['3gpp']['registration-state']
    modem_details["state"] = details['modem']['generic']['state']
    modem_details["bearers"] = details['modem']['generic']['bearers']
    modem_details["port"] = details['modem']['generic']['primary-port']
    if len(details['modem']['generic']['ports']) > 0:
        for port in details['modem']['generic']['ports']:
            logger.debug(port)

            match = re.search(r'([\w-]+) \(net\)', port)
            if match is not None:
                modem_details["int"] = match.group(1)
            match = re.search(r'([\w-]+) \(at\)', port)
            if match is not None:
                modem_details["tty"] = match.group(1)

    return modem_details


# Return list of gsm/lte modems with imei as key
def get_modems():
    mlist = modem_list()
    if mlist[0]:
        mlist = modem_list_parser(mlist[1])
    modems = {}

    for m in mlist:
        logger.debug(f'modem: {m}')
        modem_details = modem(m)
        if modem_details[0]:
            modem_details = modem_parser(modem_details[1])
            logger.debug(f'modem details: {modem_details}')

            modems[modem_details['imei']] = modem_details

    return modems
