import logging
import requests
import time
import trio
import lib.iperf as iperf
import lib.iwconfig as iwconfig
import lib.iwlink as iwlink
import lib.iwscan as iwscan

from app import stage_conf

logger = logging.getLogger("lib_tests")


# Run iperf3 tcp check against local intranet server
async def test_tcp_local(interface):
    logger.info("running tcp local iperf tests...")
    logger.debug(interface)
    tcp = {}

    forward = await iperf.run_tcp_client_f(
        interface["tests"]["local"]["iperf_host"],
        interface["tests"]["local"]["iperf_tcp_port"],
        interface["name"])
    tcp["forward"] = forward[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    reverse = await iperf.run_tcp_client_r(
        interface["tests"]["local"]["iperf_host"],
        interface["tests"]["local"]["iperf_tcp_port"],
        interface["name"])
    tcp["reverse"] = reverse[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    if (forward[0] and reverse[0]):
        return True, tcp

    return False, tcp


# Run iperf3 udp check against local intranet server
async def test_udp_local(interface):
    logger.info("running udp local iperf tests...")
    logger.debug(interface)
    udp = {}

    forward = await iperf.run_udp_client_f(
        interface["tests"]["local"]["iperf_host"],
        interface["tests"]["local"]["iperf_udp_port"],
        interface["name"])
    udp["forward"] = forward[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    reverse = await iperf.run_udp_client_r(
        interface["tests"]["local"]["iperf_host"],
        interface["tests"]["local"]["iperf_udp_port"],
        interface["name"])
    udp["reverse"] = reverse[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    if (forward[0] and reverse[0]):
        return True, udp

    return False, udp


# Run iperf3 tcp check against remote internet server
async def test_tcp_remote(interface):
    logger.info("running tcp remote iperf tests...")
    logger.debug(interface)
    tcp = {}

    forward = await iperf.run_tcp_client_f(
        interface["tests"]["remote"]["iperf_host"],
        interface["tests"]["remote"]["iperf_tcp_port"],
        interface["name"])
    tcp["forward"] = forward[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    reverse = await iperf.run_tcp_client_r(
        interface["tests"]["remote"]["iperf_host"],
        interface["tests"]["remote"]["iperf_tcp_port"],
        interface["name"])
    tcp["reverse"] = reverse[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    if (forward[0] and reverse[0]):
        return True, tcp

    return False, tcp


# Run iperf3 udp check against remote internet server
async def test_udp_remote(interface):
    logger.info("running udp remote iperf tests...")
    logger.debug(interface)
    udp = {}

    forward = await iperf.run_udp_client_f(
        interface["tests"]["remote"]["iperf_host"],
        interface["tests"]["remote"]["iperf_udp_port"],
        interface["name"])
    udp["forward"] = forward[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    reverse = await iperf.run_udp_client_r(
        interface["tests"]["remote"]["iperf_host"],
        interface["tests"]["remote"]["iperf_udp_port"],
        interface["name"])
    udp["reverse"] = reverse[1]
    logger.info('pausing for 2sec before next test')
    await trio.sleep(2)

    logger.debug(f'test_udp_remote: {udp}')

    if (forward[0] and reverse[0]):
        return True, udp

    return False, udp


# Run iperf3 tcp check on control system
def test_tcp_control(interface):
    logger.info("running tcp control iperf tests...")
    logger.debug(interface)
    tcp = { "forward": {
                "direction": "forward",
                "protocol": "tcp",
                "seconds": -1,
                "bytes": -1,
                "bps": -1,
                "retries": -1,
                "rtt": -1},
            "reverse": {
                "direction": "forward",
                "protocol": "tcp",
                "seconds": -1,
                "bytes": -1,
                "bps": -1,
                "retries": -1,
                "rtt": -1}
            }

    try:
        tcp = requests.get('http://{}:{}/tcp/{}/{}'.format(
            interface["tests"]["control"]["api_host"],
            interface["tests"]["control"]["api_port"],
            interface["tests"]["remote"]["iperf_host"],
            interface["tests"]["remote"]["iperf_tcp_port"]
        ), headers={'Accept': 'application/json'}, timeout=3)

        if tcp.status_code == 200:
            return True, tcp.json()
    
    except Exception as e:
        logger.warning(e)

    return False, tcp


# Run iperf3 udp check on control system
def test_udp_control(interface):
    logger.info("running udp control iperf tests...")
    logger.debug(interface)
    udp = { "forward": {
                "direction": "forward",
                "protocol": "udp",
                "seconds": -1,
                "bytes": -1,
                "bps": -1,
                "jitter_ms": -1,
                "lost_packets": -1,
                "packets": -1,
                "lost_percent": -1,
                "out_of_order": -1},
            "reverse": {
                "direction": "reverse",
                "protocol": "udp",
                "seconds": -1,
                "bytes": -1,
                "bps": -1,
                "jitter_ms": -1,
                "lost_packets": -1,
                "packets": -1,
                "lost_percent": -1,
                "out_of_order": -1,}
            }

    try:
        udp = requests.get('http://{}:{}/udp/{}/{}'.format(
            interface["tests"]["control"]["api_host"],
            interface["tests"]["control"]["api_port"],
            interface["tests"]["remote"]["iperf_host"],
            interface["tests"]["remote"]["iperf_udp_port"]
        ), headers={'Accept': 'application/json'}, timeout=3)

        if udp.status_code == 200:
            return True, udp.json()

    except Exception as e:
        logger.warning(e)

    return False, udp


# Collect wi-fi bssid scan stats
async def test_wifi_scan(wintf):
    scanned, data = await iwscan.scan(wintf)
    if scanned:
        return True, iwscan.parse(data)
    return False, {}


# Collect connectd wi-fi stats
async def test_wifi_connected(wintf):
    info = {
        "bssid": '',
        "ssid": '',
        "signal": '',
        "quality": '',
        "datarate_tx": 0.0,
        "datarate_rx": 0.0
    }
    conn, data = await iwconfig.info(wintf)
    if conn:
        temp = iwconfig.parse(data)
        info["bssid"] = temp["bssid"]
        info["ssid"] = temp["ssid"]
        info["signal"] = temp["signal"]
        info["quality"] = temp["quality"]
        link, datarates = await iwlink.info(wintf)
        if link:
            temp = iwlink.parse(datarates)
            info["datarate_tx"] = temp["datarate_tx"]
            info["datarate_rx"] = temp["datarate_rx"]

            return True, info
    return False, info
