import logging
import re

import lib.shell as shell

logger = logging.getLogger('lib_iwconfig')


async def info(wintf='wlan0'):
    cmd = f'sudo iwconfig {wintf}'
    logger.debug('iwconfig command: {}'.format(cmd))
    exit_code, output, err = await shell.shell_exec_async(cmd)
    logger.debug(exit_code)
    if (not err) and (exit_code == 0):
        logger.debug(output)
        return True, output
    logger.warning(err)
    return False, ""


def parse(content):
    lines = content.split('\n')

    bssid = ''
    ssid = 'hidden'
    signal = ''
    quality = ''

    for line in lines:
        line = line.strip()

        match = re.search(r'ESSID:\"(.*)\"', line)
        if match is not None:
            ssid = match.group(1)
        match = re.search((r'Access Point: '
                           r'([0-9A-Fa-f]{2})[:\-.]?'  # 1
                           r'([0-9A-Fa-f]{2})[:\-.]?'  # 2
                           r'([0-9A-Fa-f]{2})[:\-.]?'  # 3
                           r'([0-9A-Fa-f]{2})[:\-.]?'  # 4
                           r'([0-9A-Fa-f]{2})[:\-.]?'  # 5
                           r'([0-9A-Fa-f]{2})'), line)  # 6

        if match is not None:
            bssid = (f'{match.group(1).lower()}:{match.group(2).lower()}:'
                     f'{match.group(3).lower()}:{match.group(4).lower()}:'
                     f'{match.group(5).lower()}:{match.group(6).lower()}')
        match = re.search(r'Link Quality=(\d+)\/70  Signal level=(-\d+) dBm',
                          line)
        if match is not None:
            quality = match.group(1)
            signal = match.group(2)

    return {
        "bssid": bssid,
        "ssid": ssid,
        "signal": signal,
        "quality": quality,
    }
