import logging
import os
import sys

import lib.yaml_data as yaml_data

from docxtpl import DocxTemplate, InlineImage
from docx.shared import Inches

from app import base_conf, stage_conf

# Setup logging
logger = logging.getLogger("lib_report_doc")


def generate_report(proj_name, proj_dir, report_dir, floor_file):
    logger.debug(f'{base_conf["basedir"]}/samples/template.docx')
    if os.path.isfile(f'{base_conf["basedir"]}/samples/template.docx'):
        template = f'{base_conf["basedir"]}/samples/template.docx'
    else:
        logger.error('Failed to load template file')
        return False
    logger.debug(f'{base_conf["basedir"]}/samples/placeholder.png')
    if os.path.isfile(f'{base_conf["basedir"]}/samples/placeholder.png'):
        placeholder = f'{base_conf["basedir"]}/samples/placeholder.png'
    else:
        logger.error('Failed to load image placeholder file')
    err, context = yaml_data.read_yaml("project.yaml", proj_dir)
    if err:
        logger.error('Failed to read context from project.yaml')
        return False

    doc = DocxTemplate(template)

    images = load_images(doc, proj_name, report_dir, floor_file, placeholder)
    logger.debug(images)
    context["images"] = {}

    logger.debug('context before images:')
    logger.debug(context)
    for image in images:
        logger.debug(image)
        context["images"][image] = images[image]

    logger.debug('context after images:')
    logger.debug(context)

    doc.render(context)
    doc.save(f'{report_dir}/{proj_name.lower()}.docx')

    return True


def load_images(doc, title, folder, floor_file, placeholder):
    img_return = {}

    logger.debug(f"floorplan file: {floor_file}")
    if os.path.isfile(floor_file):
        img_return["floorplan"] = InlineImage(
            doc,
            image_descriptor=floor_file,
            width=Inches(6.5))
    else:
        logger.warning(f'{floor_file} not found')
        img_return["floorplan"] = InlineImage(
            doc,
            image_descriptor=placeholder,
            width=Inches(6.5))

    img_types = [
        "wifi-2.4ghz-primary-signal-strength",  # wifi signal
        "wifi-5ghz-primary-signal-strength",
        "wifi-2.4ghz-secondary-signal-strength",
        "wifi-5ghz-secondary-signal-strength",
        "wifi-datarate-summary",  # wifi datarate
        "wifi-datarate-tx",
        "wifi-datarate-rx",
        "wifi-2.4ghz-channels-all", # wifi channel
        "wifi-5ghz-channels-all",
        "wifi-2.4ghz-channels-tested",
        "wifi-5ghz-channels-tested",
        "wifi-udp-summary-upload-rate",   # wifi udp upload
        "wifi-udp-remote-upload-rate",
        "wifi-udp-local-upload-rate",
        "wifi-udp-summary-upload-jitter",
        "wifi-udp-remote-upload-jitter",
        "wifi-udp-local-upload-jitter",
        "wifi-udp-summary-upload-lost_perc",
        "wifi-udp-remote-upload-lost_perc",
        "wifi-udp-local-upload-lost_perc",
        "wifi-udp-summary-upload-ooo_perc",
        "wifi-udp-remote-upload-ooo_perc",
        "wifi-udp-local-upload-ooo_perc",
        "wifi-udp-summary-download-rate",   # wifi udp download
        "wifi-udp-remote-download-rate",
        "wifi-udp-local-download-rate",
        "wifi-udp-summary-download-jitter",
        "wifi-udp-remote-download-jitter",
        "wifi-udp-local-download-jitter",
        "wifi-udp-summary-download-lost_perc",
        "wifi-udp-remote-download-lost_perc",
        "wifi-udp-local-download-lost_perc",
        "wifi-udp-summary-download-ooo_perc",
        "wifi-udp-remote-download-ooo_perc",
        "wifi-udp-local-download-ooo_perc",
        "wifi-tcp-summary-upload-rate",  # wifi tcp upload
        "wifi-tcp-remote-upload-rate",
        "wifi-tcp-local-upload-rate",
        "wifi-tcp-summary-upload-retries",
        "wifi-tcp-remote-upload-retries",
        "wifi-tcp-local-upload-retries",
        "wifi-tcp-summary-upload-rtt",
        "wifi-tcp-remote-upload-rtt",
        "wifi-tcp-local-upload-rtt",
        "wifi-tcp-summary-download-rate",  # wifi tcp download
        "wifi-tcp-remote-download-rate",
        "wifi-tcp-local-download-rate",
        "wifi-tcp-summary-download-retries",
        "wifi-tcp-remote-download-retries",
        "wifi-tcp-local-download-retries",
        "lte-summary-rsrp",  # lte signal
        "lte-summary-rsrq",
        "lte-summary-sinr",
        "lte-AT&T-rsrp",
        "lte-T-Mobile-rsrp",
        "lte-Verizon-rsrp",
        "lte-AT&T-rsrq",
        "lte-T-Mobile-rsrq",
        "lte-Verizon-rsrq",
        "lte-AT&T-sinr",
        "lte-T-Mobile-sinr",
        "lte-Verizon-sinr",
        "lte-udp-summary-upload-rate",  # lte udp upload
        "lte-AT&T-udp-remote-upload-rate",
        "lte-T-Mobile-udp-remote-upload-rate",
        "lte-Verizon-udp-remote-upload-rate",
        "lte-udp-summary-upload-jitter",
        "lte-AT&T-udp-remote-upload-jitter",
        "lte-T-Mobile-udp-remote-upload-jitter",
        "lte-Verizon-udp-remote-upload-jitter",
        "lte-udp-summary-upload-lost_perc",
        "lte-AT&T-udp-remote-upload-lost_perc",
        "lte-T-Mobile-udp-remote-upload-lost_perc",
        "lte-Verizon-udp-remote-upload-lost_perc",
        "lte-AT&T-udp-remote-upload-ooo_perc",
        "lte-T-Mobile-udp-remote-upload-ooo_perc",
        "lte-Verizon-udp-remote-upload-ooo_perc",
        "lte-udp-summary-upload-ooo_perc",
        "lte-udp-summary-download-rate",  # lte udp download
        "lte-AT&T-udp-remote-download-rate",
        "lte-T-Mobile-udp-remote-download-rate",
        "lte-Verizon-udp-remote-download-rate",
        "lte-udp-summary-download-jitter",
        "lte-AT&T-udp-remote-download-jitter",
        "lte-T-Mobile-udp-remote-download-jitter",
        "lte-Verizon-udp-remote-download-jitter",
        "lte-udp-summary-download-lost_perc",
        "lte-AT&T-udp-remote-download-lost_perc",
        "lte-T-Mobile-udp-remote-download-lost_perc",
        "lte-Verizon-udp-remote-download-lost_perc",
        "lte-AT&T-udp-remote-download-ooo_perc",
        "lte-T-Mobile-udp-remote-download-ooo_perc",
        "lte-Verizon-udp-remote-download-ooo_perc",
        "lte-udp-summary-download-ooo_perc",
        "lte-tcp-summary-upload-rate",  # lte tcp upload
        "lte-AT&T-tcp-remote-upload-rate",
        "lte-T-Mobile-tcp-remote-upload-rate",
        "lte-Verizon-tcp-remote-upload-rate",
        "lte-tcp-summary-upload-retries",
        "lte-AT&T-tcp-remote-upload-retries",
        "lte-T-Mobile-tcp-remote-upload-retries",
        "lte-Verizon-tcp-remote-upload-retries",
        "lte-tcp-summary-upload-rtt",
        "lte-AT&T-tcp-remote-upload-rtt",
        "lte-T-Mobile-tcp-remote-upload-rtt",
        "lte-Verizon-tcp-remote-upload-rtt",
        "lte-tcp-summary-download-rate",  # lte tcp download
        "lte-AT&T-tcp-remote-download-rate",
        "lte-T-Mobile-tcp-remote-download-rate",
        "lte-Verizon-tcp-remote-download-rate",
        "lte-tcp-summary-download-retries",
        "lte-AT&T-tcp-remote-download-retries",
        "lte-T-Mobile-tcp-remote-download-retries",
        "lte-Verizon-tcp-remote-download-retries",
    ]


    for img_type in img_types:
        if os.path.isfile(f"{folder}/{title.lower()}-{img_type}.jpg"):
            # Modify the variable representation
            modified_img_type = img_type.replace("&", "_and_")
            img_return[modified_img_type] = InlineImage(
                doc,
                image_descriptor=f"{folder}/{title.lower()}-{img_type}.jpg",
                width=Inches(6.5))
        else:
            logger.warning(f'{title.lower()}-{img_type}.jpg not found')
            img_return[img_type] = InlineImage(
                doc,
                image_descriptor=placeholder,
                width=Inches(6.5))

    return img_return
