import logging
import re
import lib.shell as shell

logger = logging.getLogger('lib_nmcli')


# Run and collect connections from nmcli
async def connections():
    cmd = 'sudo nmcli -c no -m multiline connection show'
    logger.debug('connections command: {}'.format(cmd))
    exit_code, output, err = await shell.shell_exec_async(cmd)
    logger.debug(exit_code)
    if not err:
        return True, output
    logger.warning(err)
    return False, None


# Parse nmcli connection table
def connections_parser(content):
    lines = content.split('\n')

    connections_data = {}
    current_name = None

    for line in lines:
        match = re.match(r'(\w+):\s+(.+)', line)
        if match is not None:
            label, value = match.groups()
            if label == 'NAME':
                if current_name is not None:
                    connections_data[current_name] = current_entry
                current_name = value
                current_entry = {}
            else:
                if current_name is not None:
                    if label == 'DEVICE':
                        # connected if interface assigned
                        current_entry['connected'] = (value != '--')
                    current_entry[label] = value

    # Add the last connection
    if current_name is not None:
        connections_data[current_name] = current_entry

    logger.debug(f'connections: {connections_data}')
    return connections_data


# Return list of active nm connections
async def get_connections():
    clist = await connections()
    if clist[0]:
        clist = connections_parser(clist[1])

    return clist


# Connect a gsm/lte modem with nmcli
async def connect_modem(profile, intf):
    cmd = f'sudo nmcli -w 10 connection up {profile} ifname {intf}'
    logger.debug('connect modem command: {}'.format(cmd))
    exit_code, output, err = await shell.shell_exec_async(cmd)
    logger.debug(exit_code)
    if not err:
        return True, output
    logger.warning(err)
    return False, None