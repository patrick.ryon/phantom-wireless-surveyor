import json
import os


def write_json(file, data):
    try:
        dir_path = os.path.dirname(os.path.realpath(__name__))
        file = os.path.join(dir_path, file)
        # Serialize json
        json_object = json.dumps(data, indent=4)

        # Writing to sample.json
        with open(file, "w") as outfile:
            outfile.write(json_object)
            outfile.close()
        return True
    except Exception as e:
        print(e)

    return False


def read_json(file):
    try:
        dir_path = os.path.dirname(os.path.realpath(__name__))
        file = os.path.join(dir_path, file)
        with open(file, 'r') as openfile:
            # Reading from json file
            data = json.load(openfile)
            openfile.close()
    except Exception as e:
        data = {}
        print(e)

    return data
