import logging
import re

import lib.shell as shell

logger = logging.getLogger('lib_ipaddr')


async def info(wintf='wlan0'):
    cmd = f'sudo ip addr show dev {wintf}'
    logger.debug('ip addr command: {}'.format(cmd))
    exit_code, output, err = await shell.shell_exec_async(cmd)
    logger.debug(exit_code)
    if not err:
        logger.debug(output)
        return True, output
    logger.warning(err)
    return False, None


def parse(content):
    lines = content.split('\n')

    state = 'DOWN'
    ipv4 = ''
    ipv6 = ''

    for line in lines:
        line = line.strip()

        match = re.search(r'UP', line)
        if match is not None:
            state = 'UP'
        match = re.search(r'DOWN', line)
        if match is not None:
            state = 'DOWN'
        match = re.search(r'inet\s+(.+)\/', line)
        if match is not None:
            ipv4 = match.group(1)
        match = re.search(r'inet6\s+(.+)\/', line)
        if match is not None:
            ipv6 = match.group(1)

    return {
        "state": state,
        "ipv4": ipv4,
        "ipv6": ipv4
    }
