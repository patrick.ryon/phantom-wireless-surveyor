import logging
import re
import trio

import lib.shell as shell

logger = logging.getLogger('lib_iwscan')


async def scan(wintf='wlan0'):
    cmd = f'sudo iw dev {wintf} scan'
    logger.debug('iw scan command: {}'.format(cmd))
    retries = 0
    max_retries = 3  # Maximum number of connection retries
    while retries < max_retries:
        exit_code, output, err = await shell.shell_exec_async(cmd)
        if (not err) and (exit_code == 0) and ("scan aborted!" not in output):
            break
        logger.debug(exit_code)
        logger.warning(err)
        retries += 1

        # Pause for wi-fi modem scan availability
        await trio.sleep(2)
    if retries == max_retries:
        logger.warning(f'unable to connect {wintf} after {retries} tries')
        return False, {}
    else:
        logger.info(f'connected {wintf} after {retries} tries')
        return True, output


def parse(content):
    lines = content.split('\n')

    bssid = ''
    freq = ''
    band = ''
    signal = ''
    ssid = 'hidden'
    auths = []
    autht = 'Open'
    country = ''
    clients = '0'
    util = ''
    chan = ''
    width = ''
    gcipher = []
    pwcipher = []
    rates = []

    scan_data = {}
    for line in lines:
        line = line.strip()

        match = re.search(r'freq: (\d*)', line)
        if match is not None:
            freq = match.group(1)
            if int(freq) >= 2401 and int(freq) <= 2495:
                band = "2.4"
            if int(freq) >= 5150 and int(freq) <= 5895:
                band = "5"
            if int(freq) >= 5925 and int(freq) <= 7125:
                band = "6"
        match = re.search(r'signal: (-\d{2}.\d{2}) dBm', line)
        if match is not None:
            signal = match.group(1)
        match = re.search(r'SSID: (.*)$', line)
        if match is not None:
            ssid = match.group(1)
        match = re.search(r'RSN:', line)
        if match is not None:
            autht = 'WPA2'
        match = re.search(r'WPA:', line)
        if match is not None:
            autht = 'WPA1'
        match = re.search(r'WEP:', line)
        if match is not None:
            autht = 'WEP'
        match = re.search(r'Authentication suites: (.+)', line)
        if match is not None:
            match = re.search(r'Authentication suites: (.+)', line)
            authslist = match.group(1).split(' ')
            for suite in authslist:
                auths.append(suite)
            if "SAE" in auths:
                if autht == 'WPA2':
                    autht = 'WPA3'
                if autht == 'Open':
                    autht = 'WPA3'
        match = re.search(r'Country: (.{2})\s', line)
        if match is not None:
            country = match.group(1)
        match = re.search(r'station count: (.+)', line)
        if match is not None:
            clients = match.group(1)
        match = re.search(r'channel utili[s|z]ation: (\d+)\/', line)
        if match is not None:
            util = match.group(1)
        match = re.search(r'DS Parameter set: channel (\d+)', line)
        if match is not None:
            chan = match.group(1)
        match = re.search(r'primary channel: (\d+)', line)
        if match is not None:
            chan = match.group(1)
        match = re.search(r'STA channel width: (\d+) MHz', line)
        if match is not None:
            width = match.group(1)
        match = re.search(r'channel width: \d+ \((\d+) MHz\)', line)
        if match is not None:
            width = match.group(1)
        match = re.search(r'Group cipher: (.+)', line)
        if match is not None:
            cipherlist = match.group(1).split(' ')
            for cipher in cipherlist:
                gcipher.append(cipher)
        match = re.search(r'Pairwise ciphers: (.+)', line)
        if match is not None:
            cipherlist = match.group(1).split(' ')
            for cipher in cipherlist:
                pwcipher.append(cipher)
        match = re.search(r'Supported rates: (.+)', line)
        if match is not None:
            ratelist = (match.group(1).replace('*', '')).split(' ')
            for rate in ratelist:
                rates.append(rate)
        match = re.search(r'Extended supported rates: (.+)', line)
        if match is not None:
            ratelist = (match.group(1).replace('*', '')).split(' ')
            for rate in ratelist:
                rates.append(rate)
        # bssid
        match = re.search((r'BSS ([0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:'
                           r'[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2})'), line)
        if match is not None:
            if (match.group(1) != bssid) and (bssid != ''):  # next bssid
                scan_data['{}'.format(bssid)] = {}
                scan_data['{}'.format(bssid)]['freq'] = freq
                scan_data['{}'.format(bssid)]['band'] = band
                scan_data['{}'.format(bssid)]['signal'] = signal
                scan_data['{}'.format(bssid)]['ssid'] = ssid
                scan_data['{}'.format(bssid)]['autht'] = autht
                scan_data['{}'.format(bssid)]['auths'] = auths
                scan_data['{}'.format(bssid)]['clients'] = clients
                scan_data['{}'.format(bssid)]['util'] = util
                scan_data['{}'.format(bssid)]['chan'] = chan
                scan_data['{}'.format(bssid)]['width'] = width
                scan_data['{}'.format(bssid)]['country'] = country
                scan_data['{}'.format(bssid)]['gcipher'] = gcipher
                scan_data['{}'.format(bssid)]['pwcipher'] = pwcipher
                scan_data['{}'.format(bssid)]['rates'] = rates
                bssid = ''
                freq = ''
                band = ''
                signal = ''
                ssid = 'hidden'
                auths = []
                autht = 'Open'
                country = ''
                clients = '0'
                util = ''
                chan = ''
                width = ''
                gcipher = []
                pwcipher = []
                rates = []
            bssid = match.group(1)
    # last send
    scan_data['{}'.format(bssid)] = {}
    scan_data['{}'.format(bssid)]['freq'] = freq
    scan_data['{}'.format(bssid)]['band'] = band
    scan_data['{}'.format(bssid)]['signal'] = signal
    scan_data['{}'.format(bssid)]['ssid'] = ssid
    scan_data['{}'.format(bssid)]['autht'] = autht
    scan_data['{}'.format(bssid)]['auths'] = auths
    scan_data['{}'.format(bssid)]['clients'] = clients
    scan_data['{}'.format(bssid)]['util'] = util
    scan_data['{}'.format(bssid)]['chan'] = chan
    scan_data['{}'.format(bssid)]['width'] = width
    scan_data['{}'.format(bssid)]['country'] = country
    scan_data['{}'.format(bssid)]['gcipher'] = gcipher
    scan_data['{}'.format(bssid)]['pwcipher'] = pwcipher
    scan_data['{}'.format(bssid)]['rates'] = rates

    return scan_data
