import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import logging
import numpy as np
import os
import sys
import yaml

from datetime import datetime
from scipy.interpolate import Rbf
from matplotlib.colors import LinearSegmentedColormap

import lib.json_data as json_data
import lib.sqlite_data as sqlite_data

# Setup logging
logger = logging.getLogger("lib_report_wifi_iperf")


def wifi_tcp_iperf(proj_name, report_dir, data_file, floor_img, labels=False):
    # Process tests by type
    tests = [
        ["bps", "Rate (Mbps)", "rate"],
        ["retries", "Retries", "retries"],
        ["rtt", "Round Trip Time (ms)", "rtt"]
    ]
    test_types = ["f_l", "r_l", "f_r", "r_r", "u", "d"]

    # Process tests by type
    for test in tests:
        logger.debug(f"processing test: {test[0]}")

        err, data_points = sqlite_data.get_wifi_iperf(
            data_file, test[0], "tcp")

        if not err:
            for test_type in test_types:
                logger.debug(test_type)
                if test_type == "f_l":
                    test_name = f"local-upload-{test[2]}"
                    create_tcp_heatmap(proj_name, report_dir, floor_img,
                                       test, data_points, test_type, test_name,
                                       "upload", "tcp", labels)
                if test_type == "r_l":
                    test_name = f"local-download-{test[2]}"
                    create_tcp_heatmap(proj_name, report_dir, floor_img,
                                       test, data_points, test_type, test_name,
                                       "download", "tcp", labels)
                if test_type == "f_r":
                    test_name = f"remote-upload-{test[2]}"
                    create_tcp_heatmap(proj_name, report_dir, floor_img,
                                       test, data_points, test_type, test_name,
                                       "upload", "tcp", labels)
                if test_type == "r_r":
                    test_name = f"remote-download-{test[2]}"
                    create_tcp_heatmap(proj_name, report_dir, floor_img,
                                       test, data_points, test_type, test_name, 
                                       "download", "tcp", labels)
                if test_type == "u":
                    logger.info(data_points[test_type])
                    test_name = f"summary-upload-{test[2]}"
                    create_tcp_chart(proj_name, report_dir, test, data_points,
                                     test_type, test_name, "upload", "tcp")
                if test_type == "d":
                    logger.info(data_points[test_type])
                    test_name = f"summary-download-{test[2]}"
                    create_tcp_chart(proj_name, report_dir, test, data_points,
                                     test_type, test_name, "download", "tcp")


def create_tcp_heatmap(proj_name, report_dir, floor_img, test, data_points, test_type,
                   test_name, direction, protcol, labels):
    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    logger.debug(f'{test_name}')
    logger.debug(data_points[test_type])
    # Extract x, y, and z values from data points
    x, y, z = zip(*data_points[test_type])

    # Define the interpolate grid using floorplan dimensions
    xi, yi = np.meshgrid(
        np.linspace(0, floor_width, floor_width),
        np.linspace(0, floor_height, floor_height))
    #xi, yi = xi.flatten(), yi.flatten()

    # Create the interpolant using RBF
    rbf = Rbf(x, y, z, function='cubic')

    # Interpolate the data
    zi = rbf(xi, yi)

    # Set values in zi to 0 where they are 0
    zi[zi == 0] = 0
    if test[2] == "rtt":
        zi[zi == -1] = 500
        min_threshold = 0  # desired minimum threshold

        # Find indices where zi is above or equal to the minimum threshold
        below_threshold_indices = zi < min_threshold
        logger.info(zi)
        logger.info(f'below_threshold_indices: {below_threshold_indices}')

        # Apply the minimum threshold to values below the threshold
        zi[below_threshold_indices] = 0

    # Set the minimum and maximum values for the colormap
    vmin = 0  # Set your desired minimum value here
    vmax = 100  # Set your desired maximum value here
    if test[2] == "retries":
        vmin = 0  # Set your desired minimum value here
        vmax = 5000  # Set your desired maximum value here
    if test[2] == "rtt":
        vmin = 0  # Set your desired minimum value here
        vmax = 500  # Set your desired maximum value here

    # Set values in zi to the specified color
    cmap_colors = [
        (0.5, 0.5, 0.5),  # Gray
        (0, 1, 0),  # Green
        (1, 1, 0),  # Yellow
        (1, 0, 0)]  # Red
    cmap = LinearSegmentedColormap.from_list(
        'custom_colormap', cmap_colors, N=256)

    # Create a figure
    fig, ax = plt.subplots(figsize=(10, 8))

    # Display the floorplan image with 'origin' set to 'upper'
    ax.imshow(floor_img, origin='upper')

    # Define the custom colormap with specified color transitions
    colors = [  # default coverage colormap
        ((0)/100, 'green'),
        ((5)/100, 'green'),
        ((10)/100, 'yellow'),
        ((30)/100, 'yellow'),
        ((50)/100, 'red'),
        ((100)/100, 'red'),
        ]
    if test[2] == "rate":
        colors = [  # rate coverage colormap
            ((0)/100, 'gray'),
            ((3)/100, 'red'),
            ((6)/100, 'yellow'),
            ((10)/100, 'yellow'),
            ((20)/100, 'green'),
            ((100)/100, 'green'),
            ]
    if test[2] == "retries":
        colors = [  # retries coverage colormap
            ((0)/5000, 'green'),
            ((900)/5000, 'green'),
            ((1200)/5000, 'yellow'),
            ((3000)/5000, 'yellow'),
            ((4000)/5000, 'red'),
            ((5000)/5000, 'gray'),
            ]
    if test[2] == "rtt":
        colors = [  # rtt coverage colormap
            ((0)/500, 'green'),
            ((200)/500, 'green'),
            ((250)/500, 'yellow'),
            ((300)/500, 'yellow'),
            ((350)/500, 'red'),
            ((500)/500, 'gray'),
            ]
    cmap = LinearSegmentedColormap.from_list(
        'custom_colormap', colors)

    # Create heatmap overlay using interpolated data with colormap
    heatmap = ax.imshow(
        zi.reshape(floor_height, floor_width),
        cmap=cmap,
        alpha=0.5,
        extent=[0, floor_width, floor_height, 0],
        vmin=vmin, vmax=vmax)

    # Remove axis labels and tick marks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel("")
    ax.set_ylabel("")

    # Add red dots for test points
    ax.scatter(x, y, c='red', marker='o', s=50)

    if labels:  # Add labels below the dots
        for i in range(len(x)):
            ax.text(x[i], y[i] + 18,
                    f'({x[i]}, {y[i]})',
                    ha='center', va='top',
                    fontsize=8)

    # Add a colorbar
    cbar = plt.colorbar(heatmap, orientation='horizontal')
    cbar.set_label(test[1])

    # Add titles and labels
    ax.set_title(f'{protcol.upper()} {direction.title()} {test[1]}')

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{protcol}-{test_name}.jpg")
    plt.close()


def create_tcp_chart(proj_name, report_dir, test, data_points, test_type, test_name, direction,
                 protcol):

    # Extract timestamps and data columns
    timestamps = []
    numeric_data = []
    for data_point in data_points[test_type]:
        ts, l, r, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)
        numeric_data.append([l, r])

    #numeric_data = [values for _, *values in data_points[test_type]]
    logger.debug(f'numeric_data: {numeric_data}')

    # Create a list of column labels
    column_labels = [
        f'Local Server',
        f'Remote Server'
        ]

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Times')
    ax.set_title(f'{protcol.upper()} {direction.title()} {test[1]}')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{protcol}-{test_name.replace(' ', '-')}.jpg")
    plt.close()


def wifi_udp_iperf(proj_name, report_dir, data_file, floor_img, labels=False):
    # Process tests by type
    tests = [
        ["bps", "Rate (Mbps)", "rate"],
        ["jitter", "Jitter (ms)", "jitter"],
        ["lost_pack", "Lost Packets", "lost_pack"],
        ["lost_perc", "Lost Packets (%)", "lost_perc"],
        ["ooo_pack", "Out of Order Packets", "ooo_pack"],
        ["ooo_perc", "Out of Order Packets (%)", "ooo_perc"]
    ]
    test_types = ["f_l", "r_l", "f_r", "r_r", "u", "d"]

    # Process tests by type
    for test in tests:
        logger.debug(f"processing test: {test}")

        err, data_points = sqlite_data.get_wifi_iperf(
            data_file, test[0], "udp")

        if not err:
            for test_type in data_points:
                logger.debug(test_type)
                if test_type == "f_l":
                    test_name = f"local-upload-{test[2]}"
                    create_udp_heatmap(proj_name, report_dir, floor_img, test,
                                   data_points, test_type, test_name, "upload",
                                   "udp", labels)
                if test_type == "r_l":
                    test_name = f"local-download-{test[2]}"
                    create_udp_heatmap(proj_name, report_dir, floor_img, test,
                                   data_points, test_type, test_name,
                                   "download", "udp")
                if test_type == "f_r":
                    test_name = f"remote-upload-{test[2]}"
                    create_udp_heatmap(proj_name, report_dir, floor_img, test, data_points,
                                   test_type, test_name, "upload", "udp")
                if test_type == "r_r":
                    test_name = f"remote-download-{test[2]}"
                    create_udp_heatmap(proj_name, report_dir, floor_img, test, data_points,
                                   test_type, test_name, "download", "udp")
                if test_type == "u":
                    logger.info(data_points[test_type])
                    test_name = f"summary-upload-{test[2]}"
                    create_udp_chart(proj_name, report_dir, test, data_points, test_type,
                                test_name, "upload", "udp")
                if test_type == "d":
                    logger.info(data_points[test_type])
                    test_name = f"summary-download-{test[2]}"
                    create_udp_chart(proj_name, report_dir, test, data_points, test_type,
                                 test_name, "download", "udp")


def create_udp_heatmap(proj_name, report_dir, floor_img, test, data_points,
                       test_type, test_name, direction, protcol, labels=False):
    # Dimensions of the working floorplan defined by image dimensions
    floor_width = floor_img.shape[1]
    floor_height = floor_img.shape[0]

    logger.debug(f'{test_name}')
    logger.debug(data_points[test_type])
    # Extract x, y, and z values from data points
    x, y, z = zip(*data_points[test_type])

    # Define the interpolate grid using floorplan dimensions
    xi, yi = np.meshgrid(
        np.linspace(0, floor_width, floor_width),
        np.linspace(0, floor_height, floor_height))
    #xi, yi = xi.flatten(), yi.flatten()

    # Create the interpolant using RBF
    rbf = Rbf(x, y, z, function='cubic')

    # Interpolate the data
    zi = rbf(xi, yi)

    # Set values in zi to 0 where they are 0
    zi[zi == 0] = 0

    # Set the minimum and maximum values for the colormap
    vmin = 0  # Set your desired minimum value here
    vmax = 100  # Set your desired maximum value here
    if test[2] == "jitter":
        vmin = 0
        vmax = 200

    # Set values in zi to the specified color
    cmap_colors = [
        (0.5, 0.5, 0.5),  # Gray
        (0, 1, 0),  # Green
        (1, 1, 0),  # Yellow
        (1, 0, 0)]  # Red
    cmap = LinearSegmentedColormap.from_list(
        'custom_colormap', cmap_colors, N=256)

    # Create a figure
    fig, ax = plt.subplots(figsize=(10, 8))

    # Display the floorplan image with 'origin' set to 'upper'
    ax.imshow(floor_img, origin='upper')

    # Define the custom colormap with specified color transitions
    # ["bps", "jitter", "lost_pack", "lost_perc"]
    colors = [  # default coverage colormap
        (0/100, 'green'),   # 0
        (5/100, 'green'),   # 0.05
        (10/100, 'yellow'), # 0.10
        (20/100, 'yellow'), # 0.20
        (30/100, 'yellow'), # 0.30
        (50/100, 'red'),    # 0.50
        (100/100, 'red'),   # 1
        ]
    if test[2] == "rate":
        colors = [  # reate down coverage colormap
            (0/100, 'gray'),    # 0
            (3/100, 'red'),     # 0.05
            (5/100, 'yellow'), # 0.10
            (10/100, 'yellow'), # 0.20
            (20/100, 'green'),  # 0.30
            (100/100, 'green'), # 1
            ]
    if test[2] == "rate" and direction == "upload":
        colors = [  # rate up coverage colormap
            (0/100, 'gray'),    # 0
            (5/100, 'red'),     # 0.05
            (10/100, 'yellow'), # 0.10
            (15/100, 'yellow'), # 0.15
            (30/100, 'green'),  # 0.30
            (100/100, 'green'), # 1
            ]
    if test[2] == "jitter":
        colors = [  # jitter coverage colormap
            ((200-200)/200, 'green'),   # 0
            ((200-160)/200, 'green'),   # 0.05
            ((200-140)/200, 'yellow'),  # 0.10
            ((200-110)/200, 'yellow'),  # 0.20
            ((200-50)/200, 'red'),      # 0.30
            ((200-0)/200, 'gray'),      # 1
            ]

    cmap = LinearSegmentedColormap.from_list(
        'custom_colormap', colors)

    # Create heatmap overlay using interpolated data with colormap
    heatmap = ax.imshow(
        zi.reshape(floor_height, floor_width),
        cmap=cmap,
        alpha=0.5,
        extent=[0, floor_width, floor_height, 0],
        vmin=vmin, vmax=vmax)

    # Remove axis labels and tick marks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel("")
    ax.set_ylabel("")

    # Add red dots for test points
    ax.scatter(x, y, c='red', marker='o', s=50)

    if labels:  # Add labels below the dots
        for i in range(len(x)):
            ax.text(x[i], y[i] + 18,
                    f'({x[i]}, {y[i]})',
                    ha='center', va='top',
                    fontsize=8)

    # Add a colorbar
    cbar = plt.colorbar(heatmap, orientation='horizontal')
    cbar.set_label(test[1])

    # Add titles and labels
    ax.set_title(f'{protcol.upper()} {direction.title()} {test[1]}')

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{protcol}-{test_name}.jpg")
    plt.close()


def create_udp_chart(proj_name, report_dir, test, data_points, test_type,
                     test_name, direction, protcol):

    # Extract timestamps and data columns
    timestamps = []
    numeric_data = []
    for data_point in data_points[test_type]:
        ts, l, r, *_ = data_point
        timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
        timestamps.append(timestamp)
        numeric_data.append([l, r])

    #numeric_data = [values for _, *values in data_points[test_type]]
    logger.debug(f'numeric_data: {numeric_data}')

    # Create a list of column labels
    column_labels = [
        f'Local Server',
        f'Remote Server'
        ]

    # Transpose the numeric data for plotting
    numeric_data = list(zip(*numeric_data))

    # Create a figure and a single set of axes for all columns
    fig, ax = plt.subplots(figsize=(10, 6))

    # Iterate through the numeric data and create a line chart for each column
    for i in range(len(column_labels)):
        ax.plot(timestamps, numeric_data[i], label=column_labels[i])

    ax.set_xlabel('Times')
    ax.set_title(f'{protcol.upper()} {direction.title()} {test[1]}')
    ax.legend()
    ax.grid(True)

    # Save to file
    plt.tight_layout()
    plt.savefig(f"{report_dir}/{proj_name.lower()}-wifi-{protcol}-"
                f"{test_name.replace(' ', '-')}.jpg")
    plt.close()