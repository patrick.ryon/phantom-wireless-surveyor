import logging
import os


#############################
# Level       Numeric value #
# CRITICAL        50        #
# ERROR+          40        #
# WARNING+        30        #
# INFO+           20        #
# DEBUG+          10        #
# NOTSET          0         #
#############################


def setup_custom_logger(name, path=None, cl=10, fl=10, arg_override=False):

    # setup logging
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # setup console logging handler
    ch = logging.StreamHandler()
    ch.setLevel(cl)
    if arg_override:
        ch.setLevel(10)
    ch_format = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)-8s - %(message)s')
    ch.setFormatter(ch_format)
    logger.addHandler(ch)

    # setup file logging handler
    try:
        try_path = os.path.join(path, "{}.log".format(name))
        fh = logging.FileHandler(try_path, mode='a')
    except FileNotFoundError:
        fh = logging.FileHandler("{0}.log".format(name), mode='a')
    fh.setLevel(fl)
    fh_format = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)-8s - %(message)s')
    fh.setFormatter(fh_format)
    logger.addHandler(fh)

    return logger


def close_custom_logger(logger):
    for handler in logger.handlers:
        handler.close()


def shutdown_custom_logger(logger):
    for handler in logger.handlers:
        handler.shutdown()
