import json
import logging
import os
import trio
import lib.ipaddr as ipaddr
import lib.shell as shell

from app import base_conf, stage_conf

logger = logging.getLogger("lib_iperf")


async def check_iperf():
    logger.debug('checking iperf3')
    try:
        exit_code, output, err = await shell.shell_exec_async("iperf3 --version")
        if (not err and exit_code == 0):
            logger.debug('iperf3 bin found')
            logger.debug(output)
            logger.debug(f'{base_conf["basedir"]}/config/public.pem')
            if os.path.isfile(f'{base_conf["basedir"]}/config/public.pem'):
                logger.debug('iperf3 public key found')
                return True
        logger.error(err)
    except Exception as e:
        logging.error(e)
    return False


async def run_tcp_client_bidir(server_ip, server_port, intf="", downshift=False):
    forward = {
        "direction": "forward",
        "protocol": "tcp",
        "seconds": -1,
        "bytes": -1,
        "bps": -1,
        "retries": -1,
        "rtt": -1
    }
    reverse = {
        "direction": "reverse",
        "protocol": "tcp",
        "seconds": -1,
        "bytes": -1,
        "bps": -1,
        "retries": -1,
        "rtt": -1
    }

    if (await check_iperf()):
        if intf:
            stat, ip = await ipaddr.info(intf)
            if stat:
                ip = ipaddr.parse(ip)
                if ip['state'] != 'UP':
                    return False, res
            intf = f'--bind-dev {intf} '
        length = ''
        if downshift:
            length = ' -l256'
        cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
               f"{base_conf['basedir']}/config/public.pem "
               f"--username {stage_conf['iperf']['user']} -P15{length} -t3 -b0 "
               f"-p {server_port} -c {server_ip} {intf}-J "
               f"--connect-timeout 5000")
        env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

        logger.debug(f"iperf command: {cmd}")
        logger.debug(f"iperf envvars: {env}")

        exit_code, output, err = await shell.shell_exec_async(cmd, env)
        logger.debug(f'exit_code: {exit_code}')
        logger.debug(f'output: {output}')
        logger.debug(f'err: {err}')

        data = {}
        try:
            data = json.loads(output)
        except Exception as e:
            logging.error(e)
        logger.debug(f'data: {data}')

        if "error" in data:
            err = False
        if "end" in data:
            if "sum_received" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_received"])
                if "seconds" in data["end"]["sum_received"]:
                    forward["seconds"] = data["end"]["sum_received"]["seconds"]
                if "bytes" in data["end"]["sum_received"]:
                    forward["bytes"] = data["end"]["sum_received"]["bytes"]
                if "bits_per_second" in data["end"]["sum_received"]:
                    forward["bps"] = data["end"]["sum_received"]["bits_per_second"]
            if "sum_sent" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_sent"])
                if "retransmits" in data["end"]["sum_sent"]:
                    forward["retries"] = data["end"]["sum_sent"]["retransmits"]
            if "sum_received" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_received"])
                if "seconds" in data["end"]["sum_received"]:
                    reverse["seconds"] = data["end"]["sum_received"]["seconds"]
                if "bytes" in data["end"]["sum_received"]:
                    reverse["bytes"] = data["end"]["sum_received"]["bytes"]
                if "bits_per_second" in data["end"]["sum_received"]:
                    reverse["bps"] = data["end"]["sum_received"]["bits_per_second"]
            if "sum_sent" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_sent"])
                if "retransmits" in data["end"]["sum_sent"]:
                    reverse["retries"] = data["end"]["sum_sent"]["retransmits"]
            if "streams" in data["end"]:
                counter = 0
                rtt = 0.000
                for stream in data["end"]["streams"]:
                    if "mean_rtt" in stream["sender"]:
                        logger.debug(f'stream: {stream}')
                        counter += 1
                        rtt += float(stream["sender"]["mean_rtt"])
                    if counter > 0:
                        rtt = (rtt / counter)
                res["rtt"] = rtt
            logger.debug(f'ip results parsed: {res}')
        

async def run_tcp_client_f(server_ip, server_port, intf=""):
    res = {
        "direction": "forward",
        "protocol": "tcp",
        "seconds": -1,
        "bytes": -1,
        "bps": -1,
        "retries": -1,
        "rtt": -1
    }
    if (await check_iperf()):
        if intf:
            stat, ip = await ipaddr.info(intf)
            if stat:
                ip = ipaddr.parse(ip)
                if ip['state'] != 'UP':
                    return False, res
            intf = f'--bind-dev {intf} '
        cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
               f"{base_conf['basedir']}/config/public.pem "
               f"--username {stage_conf['iperf']['user']} -P15 -t3 -b0 "
               f"-p {server_port} -c {server_ip} {intf}-J "
               f"--connect-timeout 5000")
        env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

        logger.debug(f"iperf command: {cmd}")
        logger.debug(f"iperf envvars: {env}")

        exit_code, output, err = await shell.shell_exec_async(cmd, env)
        logger.debug(f'exit_code: {exit_code}')
        logger.debug(f'output: {output}')
        logger.debug(f'err: {err}')

        data = {}
        try:
            data = json.loads(output)
        except Exception as e:
            logging.error(e)
        logger.debug(f'data: {data}')
        if "end" in data:
            if "sum_received" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_received"])
                if "seconds" in data["end"]["sum_received"]:
                    res["seconds"] = data["end"]["sum_received"]["seconds"]
                if "bytes" in data["end"]["sum_received"]:
                    res["bytes"] = data["end"]["sum_received"]["bytes"]
                if "bits_per_second" in data["end"]["sum_received"]:
                    res["bps"] = data["end"]["sum_received"]["bits_per_second"]
            if "sum_sent" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_sent"])
                if "retransmits" in data["end"]["sum_sent"]:
                    res["retries"] = data["end"]["sum_sent"]["retransmits"]
            if "streams" in data["end"]:
                counter = 0
                rtt = 0.000
                for stream in data["end"]["streams"]:
                    if "mean_rtt" in stream["sender"]:
                        logger.debug(f'stream: {stream}')
                        counter += 1
                        rtt += float(stream["sender"]["mean_rtt"])
                    if counter > 0:
                        rtt = (rtt / counter)
                res["rtt"] = rtt
            logger.debug(f'ip results parsed: {res}')
        if "error" in data or exit_code > 0:
            logger.warning('error in iperf, running downshift')
            cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
               f"{base_conf['basedir']}/config/public.pem "
               f"--username {stage_conf['iperf']['user']} -P15 -t3 -b0 "
               f"-p {server_port} -c {server_ip} {intf}-J "
               f"--connect-timeout 5000 -l256")
            env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

            logger.debug(f"iperf command: {cmd}")
            logger.debug(f"iperf envvars: {env}")

            exit_code, output, err = await shell.shell_exec_async(cmd, env)
            logger.debug(f'exit_code: {exit_code}')
            logger.debug(f'output: {output}')
            logger.debug(f'err: {err}')

            try:
                data = json.loads(output)
            except Exception as e:
                logging.error(e)
            logger.debug(f'data: {data}')
            if "end" in data:
                if "sum_received" in data["end"]:
                    logger.debug('iperf result: %s', data["end"]["sum_received"])
                    if "seconds" in data["end"]["sum_received"]:
                        res["seconds"] = data["end"]["sum_received"]["seconds"]
                    if "bytes" in data["end"]["sum_received"]:
                        res["bytes"] = data["end"]["sum_received"]["bytes"]
                    if "bits_per_second" in data["end"]["sum_received"]:
                        res["bps"] = data["end"]["sum_received"]["bits_per_second"]
                if "sum_sent" in data["end"]:
                    logger.debug('iperf result: %s', data["end"]["sum_sent"])
                    if "retransmits" in data["end"]["sum_sent"]:
                        res["retries"] = data["end"]["sum_sent"]["retransmits"]
                if "streams" in data["end"]:
                    counter = 0
                    rtt = 0.000
                    for stream in data["end"]["streams"]:
                        if "mean_rtt" in stream["sender"]:
                            logger.debug(f'stream: {stream}')
                            counter += 1
                            rtt += float(stream["sender"]["mean_rtt"])
                        if counter > 0:
                            rtt = (rtt / counter)
                    res["rtt"] = rtt
                logger.debug(f'ip results parsed: {res}')
                return True, res
        else:
            return True, res

    return False, res


async def run_tcp_client_r(server_ip, server_port, intf=""):
    res = {
        "direction": "reverse",
        "protocol": "tcp",
        "seconds": -1,
        "bytes": -1,
        "bps": -1,
        "retries": -1,
        "rtt": -1
    }
    if (await check_iperf()):
        if intf:
            stat, ip = await ipaddr.info(intf)
            if stat:
                ip = ipaddr.parse(ip)
                if ip['state'] != 'UP':
                    return False, res
            intf = f'--bind-dev {intf} '
        cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
               f"{base_conf['basedir']}/config/public.pem "
               f"--username phantom -P15 -t3 -b0 -R "
               f"-p {server_port} -c {server_ip} {intf}-J "
               f"--connect-timeout 5000")
        env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

        logger.debug(f"iperf command: {cmd}")
        logger.debug(f"iperf envvars: {env}")

        exit_code, output, err = await shell.shell_exec_async(cmd, env) 
        logger.debug(f'exit_code: {exit_code}')
        logger.debug(f'output: {output}')
        logger.debug(f'err: {err}')

        data = {}
        try:
            data = json.loads(output)
        except Exception as e:
            logging.error(e)
        logger.debug(f'data: {data}')
        if "end" in data:
            if "sum_received" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_received"])
                if "seconds" in data["end"]["sum_received"]:
                    res["seconds"] = data["end"]["sum_received"]["seconds"]
                if "bytes" in data["end"]["sum_received"]:
                    res["bytes"] = data["end"]["sum_received"]["bytes"]
                if "bits_per_second" in data["end"]["sum_received"]:
                    res["bps"] = data["end"]["sum_received"]["bits_per_second"]
            if "sum_sent" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_sent"])
                if "retransmits" in data["end"]["sum_sent"]:
                    res["retries"] = data["end"]["sum_sent"]["retransmits"]
            if "streams" in data["end"]:
                counter = 0
                rtt = 0.000
                for stream in data["end"]["streams"]:
                    if "mean_rtt" in stream["sender"]:
                        logger.debug(f'stream: {stream}')
                        counter += 1
                        rtt += float(stream["sender"]["mean_rtt"])
                    if counter > 0:
                        rtt = (rtt / counter)
                res["rtt"] = rtt
            logger.debug(f'ip results parsed: {res}')
            if "error" in data or exit_code > 0:
                logger.warning('error in iperf, running downshift')
                cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
                       f"{base_conf['basedir']}/config/public.pem "
                       f"--username phantom -P15 -t3 -b0 -R "
                       f"-p {server_port} -c {server_ip} {intf}-J "
                       f"--connect-timeout 5000")
                env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

                logger.debug(f"iperf command: {cmd}")
                logger.debug(f"iperf envvars: {env}")

                exit_code, output, err = await shell.shell_exec_async(cmd, env) 
                logger.debug(f'exit_code: {exit_code}')
                logger.debug(f'output: {output}')
                logger.debug(f'err: {err}')

                try:
                    data = json.loads(output)
                except Exception as e:
                    logging.error(e)
                logger.debug(f'data: {data}')
                if "end" in data:
                    if "sum_received" in data["end"]:
                        logger.debug('iperf result: %s', data["end"]["sum_received"])
                        if "seconds" in data["end"]["sum_received"]:
                            res["seconds"] = data["end"]["sum_received"]["seconds"]
                        if "bytes" in data["end"]["sum_received"]:
                            res["bytes"] = data["end"]["sum_received"]["bytes"]
                        if "bits_per_second" in data["end"]["sum_received"]:
                            res["bps"] = data["end"]["sum_received"]["bits_per_second"]
                    if "sum_sent" in data["end"]:
                        logger.debug('iperf result: %s', data["end"]["sum_sent"])
                        if "retransmits" in data["end"]["sum_sent"]:
                            res["retries"] = data["end"]["sum_sent"]["retransmits"]
                    if "streams" in data["end"]:
                        counter = 0
                        rtt = 0.000
                        for stream in data["end"]["streams"]:
                            if "mean_rtt" in stream["sender"]:
                                logger.debug(f'stream: {stream}')
                                counter += 1
                                rtt += float(stream["sender"]["mean_rtt"])
                            if counter > 0:
                                rtt = (rtt / counter)
                        res["rtt"] = rtt
                    logger.debug(f'ip results parsed: {res}')
                    return True, res
        else:
            return True, res

    return False, res


async def run_udp_client_f(server_ip, server_port, intf=""):
    res = {
        "direction": "forward",
        "protocol": "udp",
        "seconds": -1,
        "bytes": -1,
        "bps": -1,
        "jitter_ms": -1,
        "lost_packets": -1,
        "packets": -1,
        "lost_percent": -1,
        "ooo_packets": -1,
        "ooo_percent": -1
    }
    if (await check_iperf()):
        if intf:
            stat, ip = await ipaddr.info(intf)
            if stat:
                ip = ipaddr.parse(ip)
                if ip['state'] != 'UP':
                    return False, res
            intf = f'--bind-dev {intf} '
        cmd = (f"timeout 25s iperf3 --rsa-public-key-path "
               f"{base_conf['basedir']}/config/public.pem "
               f"--username phantom -P15 -t3 -u -b0 "
               f"-p {server_port} -c {server_ip} {intf}-J "
               f"--connect-timeout 5000")
        env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

        logger.debug(f"iperf command: {cmd}")
        logger.debug(f"iperf envvars: {env}")

        exit_code, output, err = await shell.shell_exec_async(cmd, env)
        logger.debug(f'exit_code: {exit_code}')
        logger.debug(f'output: {output}')
        logger.debug(f'err: {err}')

        data = {}
        try:
            data = json.loads(output)
        except Exception as e:
            logging.error(e)
        logger.debug(f'data: {data}')
        if "end" in data:
            if "sum_received" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum"])
                if "seconds" in data["end"]["sum_received"]:
                    res["seconds"] = data["end"]["sum_received"]["seconds"]
                if "bytes" in data["end"]["sum_received"]:
                    res["bytes"] = data["end"]["sum_received"]["bytes"]
                if "bits_per_second" in data["end"]["sum_received"]:
                    res["bps"] = data["end"]["sum_received"]["bits_per_second"]
                if "jitter_ms" in data["end"]["sum_received"]:
                    res["jitter_ms"] = data["end"]["sum_received"]["jitter_ms"]
                if "lost_packets" in data["end"]["sum_received"]:
                    res["lost_packets"] = data["end"]["sum_received"]["lost_packets"]
                if "packets" in data["end"]["sum_received"]:
                    res["packets"] = data["end"]["sum_received"]["packets"]
                if "lost_percent" in data["end"]["sum_received"]:
                    res["lost_percent"] = data["end"]["sum_received"]["lost_percent"]
            if "streams" in data["end"]:
                out_of_order = 0.000
                for stream in data["end"]["streams"]:
                    if "out_of_order" in stream["udp"]:
                        logger.debug(f'stream: {stream}')
                        out_of_order += float(stream["udp"]["out_of_order"])
                res["ooo_packets"] = out_of_order
            if res["ooo_packets"] > -1 and res["packets"] > 0:
                res["ooo_percent"] = (res["ooo_packets"] / res["packets"]) * 100
            logger.debug(f'ip results parsed: {res}')
        if "error" in data or exit_code > 0:
            logger.warning('error in iperf, running downshift')
            cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
                   f"{base_conf['basedir']}/config/public.pem "
                   f"--username phantom -P15 -t3 -u -b0 "
                   f"-p {server_port} -c {server_ip} {intf}-J "
                   f"--connect-timeout 5000 -l256")
            env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

            logger.info('pausing for 2sec before next test')
            await trio.sleep(2)

            logger.debug(f"iperf command: {cmd}")
            logger.debug(f"iperf envvars: {env}")

            exit_code, output, err = await shell.shell_exec_async(cmd, env)
            logger.debug(exit_code)

            try:
                data = json.loads(output)
            except Exception as e:
                logging.error(e)
            logger.debug(f'data: {data}')
            if "end" in data:
                if "sum_received" in data["end"]:
                    logger.debug('iperf result: %s', data["end"]["sum_received"])
                    if "seconds" in data["end"]["sum_received"]:
                        res["seconds"] = data["end"]["sum_received"]["seconds"]
                    if "bytes" in data["end"]["sum_received"]:
                        res["bytes"] = data["end"]["sum_received"]["bytes"]
                    if "bits_per_second" in data["end"]["sum_received"]:
                        res["bps"] = data["end"]["sum_received"]["bits_per_second"]
                    if "jitter_ms" in data["end"]["sum_received"]:
                        res["jitter_ms"] = data["end"]["sum_received"]["jitter_ms"]
                    if "lost_packets" in data["end"]["sum_received"]:
                        res["lost_packets"] = data["end"]["sum_received"]["lost_packets"]
                    if "packets" in data["end"]["sum_received"]:
                        res["packets"] = data["end"]["sum_received"]["packets"]
                    if "lost_percent" in data["end"]["sum_received"]:
                        res["lost_percent"] = data["end"]["sum_received"]["lost_percent"]
                if "streams" in data["end"]:
                    out_of_order = 0.000
                    for stream in data["end"]["streams"]:
                        if "out_of_order" in stream["udp"]:
                            logger.debug(f'stream: {stream}')
                            out_of_order += float(stream["udp"]["out_of_order"])
                    res["ooo_packets"] = out_of_order
                if res["ooo_packets"] > -1 and res["packets"] > 0:
                    res["ooo_percent"] = (res["ooo_packets"] / res["packets"]) * 100
                logger.debug(f'ip results parsed: {res}')
                return True, res
        else:
            return True, res

    return False, res


async def run_udp_client_r(server_ip, server_port, intf=""):
    res = {
        "direction": "reverse",
        "protocol": "udp",
        "seconds": -1,
        "bytes": -1,
        "bps": -1,
        "jitter_ms": -1,
        "lost_packets": -1,
        "packets": -1,
        "lost_percent": -1,
        "ooo_packets": -1,
        "ooo_percent": -1
    }
    if (await check_iperf()):
        if intf:
            stat, ip = await ipaddr.info(intf)
            if stat:
                ip = ipaddr.parse(ip)
                if ip['state'] != 'UP':
                    return False, res
            intf = f'--bind-dev {intf} '
        cmd = (f"timeout 45s iperf3 --rsa-public-key-path "
               f"{base_conf['basedir']}/config/public.pem "
               f"--username phantom -P15 -t3 -u -b0 -R "
               f"-p {server_port} -c {server_ip} {intf}-J "
               f"--connect-timeout 5000")
        env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

        logger.debug(f"iperf command: {cmd}")
        logger.debug(f"iperf envvars: {env}")

        exit_code, output, err = await shell.shell_exec_async(cmd, env)
        logger.debug(f'exit_code: {exit_code}')
        logger.debug(f'output: {output}')
        logger.debug(f'err: {err}')

        data = {}
        try:
            data = json.loads(output)
        except Exception as e:
            logging.error(e)
        logger.debug(f'data: {data}')
        if "end" in data:
            if "sum_received" in data["end"]:
                logger.debug('iperf result: %s', data["end"]["sum_received"])
                if "seconds" in data["end"]["sum_received"]:
                    res["seconds"] = data["end"]["sum_received"]["seconds"]
                if "bytes" in data["end"]["sum_received"]:
                    res["bytes"] = data["end"]["sum_received"]["bytes"]
                if "bits_per_second" in data["end"]["sum_received"]:
                    res["bps"] = data["end"]["sum_received"]["bits_per_second"]
                if "jitter_ms" in data["end"]["sum_received"]:
                    res["jitter_ms"] = data["end"]["sum_received"]["jitter_ms"]
                if "lost_packets" in data["end"]["sum_received"]:
                    res["lost_packets"] = data["end"]["sum_received"]["lost_packets"]
                if "packets" in data["end"]["sum_received"]:
                    res["packets"] = data["end"]["sum_received"]["packets"]
                if "lost_percent" in data["end"]["sum_received"]:
                    res["lost_percent"] = data["end"]["sum_received"]["lost_percent"]
            if "streams" in data["end"]:
                out_of_order = 0.000
                for stream in data["end"]["streams"]:
                    if "out_of_order" in stream["udp"]:
                        logger.debug(f'stream: {stream}')
                        out_of_order += float(stream["udp"]["out_of_order"])
                res["ooo_packets"] = out_of_order
            if res["ooo_packets"] > -1 and res["packets"] > 0:
                res["ooo_percent"] = (res["ooo_packets"] / res["packets"]) * 100
            logger.debug(f'ip results parsed: {res}')
        if "error" in data or exit_code > 0:
            logger.warning('error in iperf, running downshift')
            cmd = (f"timeout 10s iperf3 --rsa-public-key-path "
                   f"{base_conf['basedir']}/config/public.pem "
                   f"--username phantom -P15 -t3 -u -b0 -R "
                   f"-p {server_port} -c {server_ip} {intf}-J "
                   f"--connect-timeout 5000 -l256")
            env = {"IPERF3_PASSWORD": stage_conf['iperf']['pass']}

            logger.info('pausing for 2sec before next test')
            await trio.sleep(2)

            logger.debug(f"iperf command: {cmd}")
            logger.debug(f"iperf envvars: {env}")

            exit_code, output, err = await shell.shell_exec_async(cmd, env)
            logger.debug(exit_code)

            try:
                data = json.loads(output)
            except Exception as e:
                logging.error(e)
            logger.debug(f'data: {data}')
            if "end" in data:
                if "sum_received" in data["end"]:
                    logger.debug('iperf result: %s', data["end"]["sum_received"])
                    if "seconds" in data["end"]["sum_received"]:
                        res["seconds"] = data["end"]["sum_received"]["seconds"]
                    if "bytes" in data["end"]["sum_received"]:
                        res["bytes"] = data["end"]["sum_received"]["bytes"]
                    if "bits_per_second" in data["end"]["sum_received"]:
                        res["bps"] = data["end"]["sum_received"]["bits_per_second"]
                    if "jitter_ms" in data["end"]["sum_received"]:
                        res["jitter_ms"] = data["end"]["sum_received"]["jitter_ms"]
                    if "lost_packets" in data["end"]["sum_received"]:
                        res["lost_packets"] = data["end"]["sum_received"]["lost_packets"]
                    if "packets" in data["end"]["sum_received"]:
                        res["packets"] = data["end"]["sum_received"]["packets"]
                    if "lost_percent" in data["end"]["sum_received"]:
                        res["lost_percent"] = data["end"]["sum_received"]["lost_percent"]
                if "streams" in data["end"]:
                    out_of_order = 0.000
                    for stream in data["end"]["streams"]:
                        if "out_of_order" in stream["udp"]:
                            logger.debug(f'stream: {stream}')
                            out_of_order += float(stream["udp"]["out_of_order"])
                    res["ooo_packets"] = out_of_order
                if res["ooo_packets"] > -1 and res["packets"] > 0:
                    res["ooo_percent"] = (res["ooo_packets"] / res["packets"]) * 100
                logger.debug(f'ip results parsed: {res}')
                return True, res
        else:
            return True, res

    return False, res
