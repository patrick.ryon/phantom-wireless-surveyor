import json
import logging
import lib.shell as shell

logger = logging.getLogger("lib_sperf")


def check_sperf():
    logger.debug('checking sockperf')
    try:
        exit_code, output, err = shell.shell_exec("sockperf --version")
        if (not err and exit_code == 0):
            logger.debug('sockperf bin found')
            return True
        logger.error(err)
    except Exception as e:
        logging.error(e)
    return False


def run_tcp_client(server_ip, server_port, intf=""):
    if check_sperf():
        if intf:
            intf = f'-i {intf} '
        cmd = (f"sockperf -P15 -p {server_port} -t3 -c {server_ip} {intf}-J "
               f"--connect-timeout 5000")

        res = {
            "direction": "forward",
            "protocol": "tcp",
            "seconds": -1,
            "bytes": -1,
            "bps": -1,
            "retries": -1,
        }

        logger.debug(f"sockperf command: {cmd}")

        exit_code, output, err = shell.shell_exec(cmd)
        logger.debug(exit_code)
        if not err:
            data = json.loads(output)
            logger.debug('sockperf result: %s', data["end"]["sum_received"])
            logger.debug('sockperf result: %s', data["end"]["sum_sent"])
            res["seconds"] = data["end"]["sum_received"]["seconds"]
            res["bytes"] = data["end"]["sum_received"]["bytes"]
            res["bps"] = data["end"]["sum_received"]["bits_per_second"]
            res["retries"] = data["end"]["sum_sent"]["retransmits"]
            return True, res

    return False, res


def run_udp_client(server_ip, server_port, intf=""):
    if check_sperf():
        if intf:
            intf = f'-i {intf} '
        cmd = (f"sockperf -P15 -p {server_port} -t3 -c {server_ip} -u {intf}-J "
               "--connect-timeout 5000")

        res = {
            "direction": "forward",
            "protocol": "udp",
            "seconds": -1,
            "bytes": -1,
            "bps": -1,
            "jitter_ms": -1,
            "lost_packets": -1,
            "packets": -1,
            "lost_percent": -1,
        }

        logger.debug(f"sockperf command: {cmd}")

        exit_code, output, err = shell.shell_exec(cmd)
        logger.debug(exit_code)
        if not err:
            data = json.loads(output)
            logger.debug('sockperf result: %s', data["end"]["sum"])
            res["seconds"] = data["end"]["sum"]["seconds"]
            res["bytes"] = data["end"]["sum"]["bytes"]
            res["bps"] = data["end"]["sum"]["bits_per_second"]
            res["jitter_ms"] = data["end"]["sum"]["jitter_ms"]
            res["lost_packets"] = data["end"]["sum"]["lost_packets"]
            res["packets"] = data["end"]["sum"]["packets"]
            res["lost_percent"] = data["end"]["sum"]["lost_percent"]
            return True, res

    return False, res
