import logging
import re

import lib.shell as shell

logger = logging.getLogger('lib_iwlink')


async def info(wintf='wlan0'):
    cmd = f'iw {wintf} link'
    logger.debug('iw link command: {}'.format(cmd))
    exit_code, output, err = await shell.shell_exec_async(cmd)
    logger.debug(exit_code)
    if (not err) and (exit_code == 0):
        logger.debug(output)
        return True, output
    logger.warning(err)
    return False, ""


def parse(content):
    lines = content.split('\n')

    tx = 0.0
    rx = 0.0

    for line in lines:
        line = line.strip()

        match = re.search(r'([rt]x)\s+bitrate:\s+(\d+.?\d*)\s+([GMK]?)Bit\/s',
                          line)

        if match is not None:
            if match.group(1) == 'rx':
                rx = float(match.group(2))
                if match.group(3) == 'G':
                    rx = rx * 1000000000
                if match.group(3) == 'M':
                    rx = rx * 1000000
                if match.group(3) == 'K':
                    rx = rx * 1000
            if match.group(1) == 'tx':
                tx = float(match.group(2))
                if match.group(3) == 'G':
                    tx = tx * 1000000000
                if match.group(3) == 'M':
                    tx = tx * 1000000
                if match.group(3) == 'K':
                    tx = tx * 1000

    return {
        "datarate_tx": tx,
        "datarate_rx": rx
    }
